# 虚拟化 trunk 网络配置

## 网络结构介绍

```bash
# 管理网络 172.24.99.250
# 业务网络 10.173.28.0/24, 172.24.100.0/24
```

## 服务器网络配置

```bash
nmcli con add con-name enp2s0-vlan-10 ifname enp2s0-vlan-10 type vlan id 10 dev enp2s0 ipv4.add 10.173.28.15/24 ipv4.gate 10.173.28.1 ipv4.dns 114.114.114.114 ipv4.method manual ipv6.meth disab
nmcli con add con-name enp2s0-vlan-100 ifname enp2s0-vlan-100 type vlan id 100 dev enp2s0 ipv4.add 172.24.100.2/24 ipv4.gate 172.24.100.1 ipv4.dns 114.114.114.114 ipv4.method man ipv6.meth disab
```

## 安装虚拟机

```bash
virt-install --name test-2 --memory 1024 --vcpus 4 --import --os-variant almalinux8 --disk path=/virtual/vda.qcow2,bus=virtio --cdrom /iso/AlmaLinux-8.8-x86_64-dvd.iso --network type=direct,source=enp2s0-vlan-10,source_mode=bridge,model=virtio --graphics vnc
```

