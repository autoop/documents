# Install Ansible

```bash
# 安装 python3
yum install python3.8 -y

# 升级 pip3
pip3.8 install pip==22.3.1 -i https://pypi.tuna.tsinghua.edu.cn/simple

# pip3 安装 ansible
pip3.8 install ansible -i https://pypi.tuna.tsinghua.edu.cn/simple

# ansible 版本查看
ansible --version

# 安装 sshpass
yum install sshpass -y

# 创建普通用户 greg
useradd greg
echo Fhl3y_Jsdwj | passwd --stdin greg
echo "greg ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/greg
```

