# VMware 企业级虚拟化

## 前置知识

### 1. 虚拟化解决了什么问题

```bash
举一个实际例子，假设您有 3 台物理服务器，分别用于不同的特定用途。 其中一台是邮件服务器，一台是 Web 服务器，最后一台则用于运行企业内部的传统应用。  每台服务器只使用了大约 30% 的计算容量，这仅是运行潜能的一小部分。但是，由于传统应用对内部运营非常重要，您必须将其连同所运行的第三台服务器予以保留，对吗？

过去确实如此。相对简单和可靠的做法是在单独的服务器上运行单独的任务：1 台服务器， 1 个运行操作系统，1 个处理任务。我们很难让 1 台服务器有多个大脑。但是，借助虚拟化技术，您可以将邮件服务器分为 2 个能够处理独立任务的特殊服务器，从而实现传统应用的迁移。您仍然使用相同的硬件，但可以更加高效地利用这些资源。

考虑到安全问题，您可以再次划分第一台服务器，从而可以处理另一项任务，将其使用率从 30% 提高到 60%，甚至提高到 90%。这样，现在空闲的服务器可以用于其他任务或停用，以降低散热和维护成本。
```

### 2. 虚拟化的部署模式

```bash
常规的就是你的windows装一个 vmware workstation ，或者你的 linux 装个 qemu-kvm

使用带有 hypervisor 的操作系统，然后直接在上面安装虚拟机，而不是先有操作系统再安装虚拟化软件，然后再安装虚拟机

ESXi 就是带有 hypervisor 的操作系统，我们在安装完这个操作系统之后，直接在这个操作系统上创建虚拟机，这个操作系统就是为创建虚拟机而使用的。
其实任何主流的 Linux 发行版本都是带有 hypervisor 的操作系统， 而这个 hypervisor 是集成在 Linux 内核上的

ESXi/vSphere 是商业软件
```

### 3. 安装系统前的 ISO 版本选择

```bash
ISO 有很多版本，目前主流版本为 6.5, 6.7, 7.0, 8.0
你应该选择什么版本的ISO呢？ 目前最新的硬件完全支持 ESXi 8.0， 比较旧的服务器普遍还是使用 6.5 或者 6.7， 不新不旧的服务器使用 7.0
如果你现在不清楚该用什么版本的 ISO，那么打 400, 硬件是那家的就打那家的 400 客服，如果过保了，那就只能上网搜，或者上官网查
```

### 4. 硬件不识别怎么办?

```bash
学习ESXi的时候，大多情况是使用 vmware workstation 来装个 ESXi 的虚拟机，然后学习，其实安装的时候没有碰到什么问题，为什么呢？原因是你所使用的虚拟机的虚拟化硬件都是通用硬件，VMware 在发布 vSphere ISO 的时候里面就集成了大量的通用硬件驱动，所以安装的时候没有驱动问题，所谓驱动问题就是硬件不识别，因为这个 ISO 里面没有对应的驱动。

比如说你现在在真正的服务器上安装ESXi，这个服务器上有很新的RAID阵列卡或者网卡（一般来说就是RAID卡或者网卡不识别的问题）。

一般碰到这种问题，最简单的方法就是打 400，要定制的 ISO，因为卖服务器的厂商一般为了服务器好卖，或者体现售后的价值，都会针对自己的硬件定制对应的 ESXi ISO 版本，然后当客户需要的时候，将定制的带有指定驱动的 ISO 提供给客户下载使用。

如果服务器硬件厂商也没办法给你提供集成了指定驱动的 ISO 镜像，那么你就只能自己找驱动，然后自己打上去了。这里面就要是用一款软件，名字叫做 ESXI-CUstomizer-PS。
```

## ESXi 硬件虚拟化

### ESXi 7.x 部署

#### 1. 下载 ESXi 7.x

```bash
https://cios.dhitechnical.com/VMware/VMware%20vSphere%207/vSphere%20Hypervisor%20%28ESXi%29%207.0U3n/VMware-VMvisor-Installer-7.0U3n-21930508.x86_64.iso
```

#### 2. Welcome to the VMware ESXi 7.0.3 Installation

![image-20240606154634100](http://typora.hflxhn.com/documents/2024/0607/image-20240606154634100.png)



#### 3. End User License Agreement (EULA)

![image-20240606154724949](http://typora.hflxhn.com/documents/2024/0607/image-20240606154724949.png)

#### 4. Select a Disk to Install or Upgrade

![image-20240606154825831](http://typora.hflxhn.com/documents/2024/0607/image-20240606154825831.png)

#### 5. Please select a keyboard layout

![image-20240606154859077](http://typora.hflxhn.com/documents/2024/0607/image-20240606154859077.png)

#### 6. Enter a root password

![image-20240606154939311](http://typora.hflxhn.com/documents/2024/0607/image-20240606154939311.png)

#### 7. Confirm Install

![image-20240606155133721](http://typora.hflxhn.com/documents/2024/0607/image-20240606155133721.png)

#### 8. Installation Complete

![image-20240606155527478](http://typora.hflxhn.com/documents/2024/0607/image-20240606155527478.png)

### 登陆 ESXi

#### 1. Customize System/View Logs

![image-20240606160315744](http://typora.hflxhn.com/documents/2024/0607/image-20240606160315744.png)

#### 2. Authentication Required

![image-20240606160433730](http://typora.hflxhn.com/documents/2024/0607/image-20240606160433730.png)

### 配置管理网络

#### 1. Configure Management Network

![image-20240606160815693](http://typora.hflxhn.com/documents/2024/0607/image-20240606160815693.png)

#### 2. VLAN (optional)

![image-20240606160929461](http://typora.hflxhn.com/documents/2024/0607/image-20240606160929461.png)

![image-20240606160951011](http://typora.hflxhn.com/documents/2024/0607/image-20240606160951011.png)

#### 3. IPv4 Configuration

![image-20240606161126872](http://typora.hflxhn.com/documents/2024/0607/image-20240606161126872.png)

#### 4. Test Management Network

![image-20240606161339953](http://typora.hflxhn.com/documents/2024/0607/image-20240606161339953.png)

![image-20240606161352070](http://typora.hflxhn.com/documents/2024/0607/image-20240606161352070.png)

![image-20240606161405282](http://typora.hflxhn.com/documents/2024/0607/image-20240606161405282.png)

#### 5. ping 测试

![image-20240606161504609](http://typora.hflxhn.com/documents/2024/0607/image-20240606161504609.png)

### WEB 端管理

#### 1. 登陆 ESXi

![image-20240606161651758](http://typora.hflxhn.com/documents/2024/0607/image-20240606161651758.png)

#### 2. 网络管理

```bash
关于 ESXi 的组网，涉及到了虚拟机里面的网络资源的使用。虚拟化就是三大基础设施的结合体，计算资源+网络资源+存储资源+虚拟化构建而成的虚拟化解决方案。

在 ESXi 组网这块，可以设置管理网络，用于 ESXi 的日常管理，一般来说管理网络会使用 2 个千兆网卡，千兆网络足以完成 ESXi 的日常管理。
ESXi 还可以设置数据网络（业务网络），一般业务网络会使用 2 个万兆以上的网卡做对应的链路聚合，传递多个 VLAN 的业务流量。
ESXi 还可以设置存储网络，一般存储网络也是使用 2 个万兆以上的网卡做对应的链路聚合，一般来说存储网络不配置 VLAN 并且采用直连的形式通过存储网络的交换机直接连接到存储设备。

除了上述 3 个常见的网络之外，在 ESXi 这块还会涉及到迁移网络，迁移网络指的当活动的虚拟机需要从一个 ESXi 迁移到另外一个 ESXi 的时候专门使用的网络，这样能保证虚拟机在迁移过程当中保证处于一个运行的状态，实现 0 宕机在线迁移。
```

##### Add port group

![image-20240606165952372](http://typora.hflxhn.com/documents/2024/0607/image-20240606165952372.png)

![image-20240606170106216](http://typora.hflxhn.com/documents/2024/0607/image-20240606170106216.png)

#### 3. 存储管理

##### 3.1 New datastore

![image-20240606162558197](http://typora.hflxhn.com/documents/2024/0607/image-20240606162558197.png)

##### 3.2 Mount NFS datastore

![image-20240606162702009](http://typora.hflxhn.com/documents/2024/0607/image-20240606162702009.png)

##### 3.3 Provide NFs mount details

![image-20240606162759215](http://typora.hflxhn.com/documents/2024/0607/image-20240606162759215.png)

##### 3.4 Ready to complete

![image-20240606162830091](http://typora.hflxhn.com/documents/2024/0607/image-20240606162830091.png)

##### 3.5 Datastore browser

![image-20240606162944800](http://typora.hflxhn.com/documents/2024/0607/image-20240606162944800.png)

![image-20240606163000494](http://typora.hflxhn.com/documents/2024/0607/image-20240606163000494.png)

#### 4. 虚拟机安装

##### 4.1 Create / Register VM

![image-20240606162113188](http://typora.hflxhn.com/documents/2024/0607/image-20240606162113188.png)

##### 4.2 Create a new virtual machine

![image-20240606162134895](http://typora.hflxhn.com/documents/2024/0607/image-20240606162134895.png)

##### 4.3 Select a name and huest OS

![image-20240606162237264](http://typora.hflxhn.com/documents/2024/0607/image-20240606162237264.png)

##### 4.4 Select storage

![image-20240606163228719](http://typora.hflxhn.com/documents/2024/0607/image-20240606163228719.png)

##### 4.5 Customize settings

![image-20240606162356387](http://typora.hflxhn.com/documents/2024/0607/image-20240606162356387.png)

![image-20240606163325871](http://typora.hflxhn.com/documents/2024/0607/image-20240606163325871.png)

##### 4.6 Ready to complete

![image-20240606163401578](http://typora.hflxhn.com/documents/2024/0607/image-20240606163401578.png)

##### 4.7 Power on

![image-20240606164639470](http://typora.hflxhn.com/documents/2024/0607/image-20240606164639470.png)

##### 4.8 Install CentOS 7

![image-20240606164706457](http://typora.hflxhn.com/documents/2024/0607/image-20240606164706457.png)

##### 4.9 WELCOME TO CENTOS 7

![image-20240606164934087](http://typora.hflxhn.com/documents/2024/0607/image-20240606164934087.png)

##### 4.10 DATE & TIME

![image-20240606165135703](http://typora.hflxhn.com/documents/2024/0607/image-20240606165135703.png)

![image-20240606165159130](http://typora.hflxhn.com/documents/2024/0607/image-20240606165159130.png)

##### 4.11 SOFTWARE SELECTION

![image-20240606165252873](http://typora.hflxhn.com/documents/2024/0607/image-20240606165252873.png)

##### 4.12 INSTALLATION DESTINATION

![image-20240606165422751](http://typora.hflxhn.com/documents/2024/0607/image-20240606165422751.png)

![image-20240606165442077](http://typora.hflxhn.com/documents/2024/0607/image-20240606165442077.png)

![image-20240606165459711](http://typora.hflxhn.com/documents/2024/0607/image-20240606165459711.png)

![image-20240606165514474](http://typora.hflxhn.com/documents/2024/0607/image-20240606165514474.png)

##### 4.13 NETWORK & HOST NAME

![image-20240606165609581](http://typora.hflxhn.com/documents/2024/0607/image-20240606165609581.png)

![image-20240606170840762](http://typora.hflxhn.com/documents/2024/0607/image-20240606170840762.png)

![image-20240606170904656](http://typora.hflxhn.com/documents/2024/0607/image-20240606170904656.png)

##### 4.14 Begin Installation

![image-20240606170946372](http://typora.hflxhn.com/documents/2024/0607/image-20240606170946372.png)

##### 4.15 ROOT PASSWORD

![image-20240606171057291](http://typora.hflxhn.com/documents/2024/0607/image-20240606171057291.png)

![image-20240606171042587](http://typora.hflxhn.com/documents/2024/0607/image-20240606171042587.png)

##### 2.16 Reboot

![image-20240606171619144](http://typora.hflxhn.com/documents/2024/0607/image-20240606171619144.png)

##### 2.17 Login CentOS 7

![image-20240606171732550](http://typora.hflxhn.com/documents/2024/0607/image-20240606171732550.png)

## vSphere 7.x

```bash
VCSA的全称叫做vCenter Server Appliance。
为什么要用VCSA呢？
我们之前学习的 ESXi 都是单机的玩法，管理起来只需要登录 ESXi 的 web 界面就可以了，但是一旦已有 10 个 ESXi 或者 100 个 ESXi 或者更多的 ESXi，那么你总不能一个一个 web 页面登录去管理吧。。。。能理解吗？

VCSA 是 VMware 针对 vSphere 虚拟化解决方案的最牛逼的产品，他能将 ESXi 的管理实现统一，你可以将 ESXi 加到 VCSA 的管理域中，实现统一管理。

实现统一管理只是 VCSA 最基本的功能，VCSA 还提供了很多高级功能，比如虚拟机的高可用，分布式虚拟交换机，RBAC（Role Based Access Control）等等。

VCSA 可以独立部署，也可以嵌套部署，独立部署的意思就是将 VCSA 装到物理机里面（只有超大型集群才有可能使用这种方式，超过 500 台以上的 ESXi。）

所谓嵌套部署是指将 VCSA 以虚拟机的形式安装到 ESXi 里面，这种方法是最流行的方法，由于使用了虚拟机的特性，VCSA 具备了高可用的特性。
```

### 下载 vSphere 7.x

```bash
# 官网地址
https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vcenter.install.doc/GUID-C17AFF44-22DE-41F4-B85D-19B7A995E144.html

# 互联网的镜像
https://www.tssn.services/public/vmware/vcenter/VMware-VCSA-all-7.0.3-20395099.iso
```

### 命令行安装部署

#### 1. 上传 iso 到 linux 节点

![image-20240606173545490](http://typora.hflxhn.com/documents/2024/0607/image-20240606173545490.png)

#### 2. sha512sum 哈希
```bash
sha512sum VMware-VCSA-all-7.0.3-20395099.iso
```

![image-20240606173650818](http://typora.hflxhn.com/documents/2024/0607/image-20240606173650818.png)

#### 3. 挂载 iso 到 mnt 目录

```bash
mount VMware-VCSA-all-7.0.3-20395099.iso /mnt/
```

![image-20240606173949463](http://typora.hflxhn.com/documents/2024/0607/image-20240606173949463.png)

#### 4. 拷贝安装文件并修改

```bash
cp /mnt/vcsa-cli-installer/templates/install/embedded_vCSA_on_ESXi.json ~
```

![image-20240606174029149](http://typora.hflxhn.com/documents/2024/0607/image-20240606174029149.png)

#### 5. 编辑 embedded_vCSA_on_ESXi.json

```bash
{
    "__version": "2.13.0",
    "__comments": "Sample template to deploy a vCenter Server Appliance with an embedded Platform Services Controller on an ESXi host.",
    "new_vcsa": {
        "esxi": {
            "hostname": "10.xxx.xx.xxx",
            "username": "root",
            "password": "mxxxxxx",
            "deployment_network": "VM Network",
            "datastore": "datastore1"
        },
        "appliance": {
            "__comments": [
                "You must provide the 'deployment_option' key with a value, which will affect the vCenter Server Appliance's configuration parameters, such as the vCenter Server Appliance's number of vCPUs, the memory size, the storage size, and the maximum numbers of ESXi hosts and VMs which can be managed. For a list of acceptable values, run the supported deployment sizes help, i.e. vcsa-deploy --supported-deployment-sizes"
            ],
            "thin_disk_mode": true,
            "deployment_option": "small",
            "name": "vcsa-10.xxx.xx.xxx"
        },
        "network": {
            "ip_family": "ipv4",
            "mode": "static",
            "ip": "10.1xx.xx.xxx",
            "prefix": "24",
            "gateway": "10.xxx.xx.x",
            "dns_servers": [
                "114.114.114.114"
            ]
        },
        "os": {
            "password": "Ioxxxxxx",
            "ntp_servers": "time.nist.gov",
            "ssh_enable": true
        },
        "sso": {
            "password": "Ioxxxxxx",
            "domain_name": "vsphere.local"
        }
    },
    "ceip": {
        "description": {
            "__comments": [
                "++++VMware Customer Experience Improvement Program (CEIP)++++",
                "VMware's Customer Experience Improvement Program (CEIP) ",
                "provides VMware with information that enables VMware to ",
                "improve its products and services, to fix problems, ",
                "and to advise you on how best to deploy and use our ",
                "products. As part of CEIP, VMware collects technical ",
                "information about your organization's use of VMware ",
                "products and services on a regular basis in association ",
                "with your organization's VMware license key(s). This ",
                "information does not personally identify any individual. ",
                "",
                "Additional information regarding the data collected ",
                "through CEIP and the purposes for which it is used by ",
                "VMware is set forth in the Trust & Assurance Center at ",
                "http://www.vmware.com/trustvmware/ceip.html . If you ",
                "prefer not to participate in VMware's CEIP for this ",
                "product, you should disable CEIP by setting ",
                "'ceip_enabled': false. You may join or leave VMware's ",
                "CEIP for this product at any time. Please confirm your ",
                "acknowledgement by passing in the parameter ",
                "--acknowledge-ceip in the command line.",
                "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
            ]
        },
        "settings": {
            "ceip_enabled": false
        }
    }
}
```

![image-20230815102312941](http://typora.hflxhn.com/documents/2024/0607/image-20230815102312941.png)

![image-20230815102437962](http://typora.hflxhn.com/documents/2024/0607/image-20230815102437962.png)

#### 6. 开始安装

```bash
yum install libnsl -y
/mnt/vcsa-cli-installer/lin64/vcsa-deploy install ~/embedded_vCSA_on_ESXi.json --accept-eula --no-ssl-certificate-verification -v --log-dir ~/vcsa-install-log
```

![image-20240606175819404](http://typora.hflxhn.com/documents/2024/0607/image-20240606175819404.png)

#### 7. 登陆 vSphere

![image-20240607093756861](http://typora.hflxhn.com/documents/2024/0607/image-20240607093756861.png)

### VCSA 的功能

```bash
VCSA 这个产品可以将 ESXi 纳管起来，使用 VCSA 可以管理多个 ESXi 在一个 web 页面。
除此之外 VCSA 还提供了很多 ESXi 不包含的功能，比如虚拟机的克隆，模板，分布式虚拟交换机等等。
注意：VCSA 是一个收费软件（可以使用神秘力量破解）
```

#### 1. 新建数据中心

```bash
由于 VCSA 可以将 ESXi 纳管，所以理论上只要 VCSA 到 ESXi 网络可达，那么你就可以将 ESXi 加入到 VCSA 里面，
所以一个 VCSA 可以将多个数据中心中的 ESXi 加入进来，
所以 VCSA 支持 DataCenter 的创建，这里面的 DataCenter 是一个抽象的概念，
一般我们将跨不同地域的 ESXi 节点放到不同的 DataCenter 中，比如北京的 ESXi 和上海的 ESXi
建议是放到不同的 DataCenter 中，但是一般对于中小型的 vsphere 解决方案中，ESXi和VCSA都在一个内网中，所以一般来说就创建一个DataCenter。
```

![image-20240607094745981](http://typora.hflxhn.com/documents/2024/0607/image-20240607094745981.png)

![image-20240607094855284](http://typora.hflxhn.com/documents/2024/0607/image-20240607094855284.png)

#### 2. 集群的创建

```bash
集群（Cluster）是创建在 DataCenter 中的一个抽象概念，你可以不在 DataCenter 里面创建集群，直接在 DataCenter 里面加入 ESXi 也是可以的，
但是你直接挂到 DataCenter 中的 ESXi 将不会具备集群的特有功能，比如 HA 或者 DRS。
```

![image-20240607104939814](http://typora.hflxhn.com/documents/2024/0607/image-20240607104939814.png)

![image-20240607104836343](http://typora.hflxhn.com/documents/2024/0607/image-20240607104836343.png)

![image-20240607104914627](http://typora.hflxhn.com/documents/2024/0607/image-20240607104914627.png)

```bash
你可以在一个 DataCenter 中创建多个集群，每个集群里面都可以加入 ESXi 的主机。
但是请注意，一般来说我们创建多个集群主要是用来区分不同属性的 ESXi 主机，
比如我们有一个集群 A，这个集群 A 加入的 ESXi 都是 X86 架构的 CPU，此时如果我们有 PPC 架构的 CPU 的 ESXi 主机，
我们不能将这个主机加入到集群，此时建议创建一个集群 B，将不同架构的 CPU 放到集群 B 中。
当然你也可以根据 CPU 的型号来将不同的 ESXi 主机加入到不同的集群，比如集群 A 中的 ESXi 主机是 AMD 的 cpu，集群 B 中的 ESXi 主机是 Intel 的cpu。
或者根据显卡型号，硬盘的种类，网卡的速度，详细的CPU型号等等来进行区分。
总而言之，我们强烈建议一个集群中的所有的 ESXi 主机应当保持硬件配置相同。为什么要保证硬件配置相同呢？
因为在集群有一个重要的功能 HA，如果集群开启了 HA 功能，当这个集群中的某个 ESXi Down 了，那么这个 ESXi 上运行的虚拟机将会在同集群中的另外正常的 ESXi 上开机运行，
那如果你一个集群中的 ESXi 主机规格不同，很有可能出现 VM 在原来的 ESXi 上可以运行，但是当 VM 在同集群中的另外的 ESXi 上就无法正常运行。
```

#### 3. 添加主机

```bash
我们可以直接将主机添加到 DataCenter 下，不必非得将 ESXi 加入到某个集群，根据你自己的实际情况选择将 ESXi 加入到哪里。
```

![image-20240607105837712](http://typora.hflxhn.com/documents/2024/0607/image-20240607105837712.png)

![image-20240607105919717](http://typora.hflxhn.com/documents/2024/0607/image-20240607105919717.png)

![image-20240607105933949](http://typora.hflxhn.com/documents/2024/0607/image-20240607105933949.png)

![image-20240607105951102](http://typora.hflxhn.com/documents/2024/0607/image-20240607105951102.png)

![image-20240607110008060](http://typora.hflxhn.com/documents/2024/0607/image-20240607110008060.png)

![image-20240607110143034](http://typora.hflxhn.com/documents/2024/0607/image-20240607110143034.png)

```bash
当你添加完 ESXi 主机之后，这个主机中的存储和网络资源包括原来运行在 ESXi 上的虚拟机都会被 VCSA 检测到。
```

![image-20240607110253342](http://typora.hflxhn.com/documents/2024/0607/image-20240607110253342.png)

![image-20240607110311665](http://typora.hflxhn.com/documents/2024/0607/image-20240607110311665.png)

![image-20240607110325595](http://typora.hflxhn.com/documents/2024/0607/image-20240607110325595.png)

#### 4. 创建虚拟机

![image-20240607110406843](http://typora.hflxhn.com/documents/2024/0607/image-20240607110406843.png)

![image-20240607110448704](http://typora.hflxhn.com/documents/2024/0607/image-20240607110448704.png)

![image-20240607110707611](http://typora.hflxhn.com/documents/2024/0607/image-20240607110707611.png)

![image-20240607110720074](http://typora.hflxhn.com/documents/2024/0607/image-20240607110720074.png)

![image-20240607110748935](http://typora.hflxhn.com/documents/2024/0607/image-20240607110748935.png)

![image-20240607110800637](http://typora.hflxhn.com/documents/2024/0607/image-20240607110800637.png)

![image-20240607110815157](http://typora.hflxhn.com/documents/2024/0607/image-20240607110815157.png)

![image-20240607110850340](http://typora.hflxhn.com/documents/2024/0607/image-20240607110850340.png)

![image-20240607110912306](http://typora.hflxhn.com/documents/2024/0607/image-20240607110912306.png)

![image-20240607110928166](http://typora.hflxhn.com/documents/2024/0607/image-20240607110928166.png)

![image-20240607111653439](http://typora.hflxhn.com/documents/2024/0607/image-20240607111653439.png)

#### 5. 克隆虚拟机

```bash
ESXi 是没有虚拟机克隆功能的，所以说 ESXi 使用起来是非常不方便的，当 ESXi 加入到 VCSA 中，VCSA 的 web 管理界面就有克隆虚拟机的功能。
```

![image-20240607112024141](http://typora.hflxhn.com/documents/2024/0607/image-20240607112024141.png)

![image-20240607112046718](http://typora.hflxhn.com/documents/2024/0607/image-20240607112046718.png)

![image-20240607112058088](http://typora.hflxhn.com/documents/2024/0607/image-20240607112058088.png)

![image-20240607112115141](http://typora.hflxhn.com/documents/2024/0607/image-20240607112115141.png)

```bash
选择存储的时候需要注意虚拟磁盘的格式问题，这涉及到具体分配多少磁盘，一般我们虚拟机使用的磁盘模式都是精简置备模式（thin）。
而且存储选择的时候如果克隆的虚拟机磁盘和原虚拟机磁盘在一个存储中，那就相当于一个拷贝，但是如果克隆的虚拟机选择的磁盘和原虚拟机磁盘不同，将会涉及到网络传输，因为 VCSA 管理了多个 ESXi 的，每个 ESXi 都可能有自己的本地存储，那你跨 ESXi 的本地存储之间拷贝数据肯定是要有网络流量的。
所以说我们一般在克隆的时候都会将克隆的虚拟机选择和原虚拟机一个磁盘。比如所有的 ESXi 都连接到了一个存储上（共享存储），那我们就不用担心网络传输的问题了，因为这些虚拟机都共享一个存储，克隆本质上就是在共享存储上的拷贝。
```

![image-20240607112204726](http://typora.hflxhn.com/documents/2024/0607/image-20240607112204726.png)

![image-20240607112215825](http://typora.hflxhn.com/documents/2024/0607/image-20240607112215825.png)

![image-20240607112232097](http://typora.hflxhn.com/documents/2024/0607/image-20240607112232097.png)

![image-20240607112250928](http://typora.hflxhn.com/documents/2024/0607/image-20240607112250928.png)

#### 6. 模板功能

```bash
VCSA 提供了一种更高效的部署虚拟机的方法，叫做模板，你可以选择将一个虚拟机转换成模板，然后后续创建虚拟机的时候使用模板来创建虚拟机进而实现大量虚拟机的创建。
想获得一个模板有两种方法，第一种就是直接将虚拟机转换成模板（需要虚拟机处于关机状态），第二种就是使用一个虚拟机的克隆功能，直接克隆出来一个模板。
注意：模板不是虚拟机，所以在虚拟机列表里面是看不到模板的。
```

##### 6.1 克隆出来一个模板

![image-20240607112439736](http://typora.hflxhn.com/documents/2024/0607/image-20240607112439736.png)

![image-20240607112549928](http://typora.hflxhn.com/documents/2024/0607/image-20240607112549928.png)

![image-20240607112651518](http://typora.hflxhn.com/documents/2024/0607/image-20240607112651518.png)

![image-20240607112703305](http://typora.hflxhn.com/documents/2024/0607/image-20240607112703305.png)

![image-20240607112717925](http://typora.hflxhn.com/documents/2024/0607/image-20240607112717925.png)

![image-20240607112726776](http://typora.hflxhn.com/documents/2024/0607/image-20240607112726776.png)

##### 6.2 转换为模板

![image-20240607112504009](http://typora.hflxhn.com/documents/2024/0607/image-20240607112504009.png)

![image-20240607112807320](http://typora.hflxhn.com/documents/2024/0607/image-20240607112807320.png)

##### 6.3 从模板部署虚拟机

![image-20240607112940520](http://typora.hflxhn.com/documents/2024/0607/image-20240607112940520.png)

![image-20240607113131858](http://typora.hflxhn.com/documents/2024/0607/image-20240607113131858.png)

![image-20240607113029777](http://typora.hflxhn.com/documents/2024/0607/image-20240607113029777.png)

![image-20240607113040455](http://typora.hflxhn.com/documents/2024/0607/image-20240607113040455.png)

![image-20240607113050992](http://typora.hflxhn.com/documents/2024/0607/image-20240607113050992.png)

![image-20240607113149347](http://typora.hflxhn.com/documents/2024/0607/image-20240607113149347.png)

![image-20240607113208026](http://typora.hflxhn.com/documents/2024/0607/image-20240607113208026.png)

#### 7. 加入多个 ESXi

```bash
VCSA 管理了多个 ESXi，会有什么问题呢？你思考一下。
每个 ESXi 都有自己的计算资源，网络资源，存储资源，当 VCSA 将这些 ESXi 纳管起来之后这些资源的整合其实是杂乱无章的，计算资源是最好整合的，
因为我们一般会将同型号 CPU 的 ESXi 加入到一个集群中（虽然我们现在没有将 ESXi 加到集群，我们直接将 ESXi 挂到了 DataCenter 下）。
但是网络资源和存储资源就不太好整合，因为网络资源是最抽象的，因为每个 ESXi 上都有自己的 vSwitch 和 PortGroup ，甚至每个 ESXi 上会有多个 vSwitch 和 Port Group。存储资源还好，因为存储资源是和 ESXi 直接相关的，每个ESXi有多少存储资源取决于ESXi本身有多少的 Local Storage（暂时不考虑网络存储，比如 NFS 或者 IPSAN ）。
所以说通过上面的描述来看其实网络资源在 ESXi 加入到 VCSA 中之后是最难整合的，而且我们之前无论是创建 vSwitch 还是创建 Port Group 我们都是在 ESXi 上创建的，而且注意，当 ESXi 加入 VCSA 之后，你将不能在 VCSA 上在指定的 ESXi 主机上创建 "标准的vSwitch"。
```

![image-20240607113830792](http://typora.hflxhn.com/documents/2024/0607/image-20240607113830792.png)

```bash
那怎么办？
笨方法，就是当你需要创建标准的 vSwith 和 Port Group 的时候，你就需要登录到指定的 ESXi 上进行创建。
这种方法是可行的，但是有一个最关键的问题，如果 ESXi 的主机数量太多了，我们创建起来就费事了，而且容易出现人为的失误，
比如 vSwitch 的 uplink 指错了，或者 Port Group 的 VLAN 配错了，这将导致你的 VM 无法跨 ESXi 实现访问。
（如果你加入到 VCSA 中的 ESXi 中的网络资源配置的不同步，那么就会出现这个问题。）
```

![image-20240607115528872](http://typora.hflxhn.com/documents/2024/0607/image-20240607115528872.png)

#### 8. DVS（Distributed Virtual Switch）

```bash
VCSA 给我们提供了一个 DVS 的高级功能，DVS 就是分布式虚拟交换机。
由于 ESXi 都被 VCSA 纳管了，所以 VCSA 可以逻辑的创建一个连接所有 ESXi 的分布式虚拟交换机，也就是说这个分布式虚拟交换机是对所有的 ESXi 都生效的，避免了你有 10 个ESXi主机，你就要分别在 10 个 ESXi 主机上创建 10 个虚拟交换机的行为，你只需要创建一个 DVS，那么这个 DVS 就会出现在 10 个 ESXi 主机上。
```

![image-20240607115755589](http://typora.hflxhn.com/documents/2024/0607/image-20240607115755589.png)

### 使用 PowerCLI 管理 vCenter

#### 1. ubuntu 安装 PowerCLI

```bash
# https://learn.microsoft.com/en-us/powershell/scripting/install/install-ubuntu?view=powershell-7.4

sudo apt-get install -y wget apt-transport-https software-properties-common
source /etc/os-release
wget -q https://packages.microsoft.com/config/ubuntu/$VERSION_ID/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
sudo apt-get update

# Install PowerShell
sudo apt-get install -y powershell

# Start PowerShell
pwsh
```

#### 2. Install 

```bash
Install-Module -Name VMware.PowerCLI
```

#### 3. PowerCLI 管理 vCenter

```bash
# 忽略 ssl 证书
Set-PowerCLIConfiguration -InvalidCertificateAction:Ignore

# 连接到 vCenter
Connect-VIServer -Server <vCenter 地址> -User <用户名> -Password <密码>
```

![image-20240126095523544](http://typora.hflxhn.com/documents/2024/0126/image-20240126095523544.png)

```bash
# 获取虚拟机列表
Get-VM
```

![image-20240126095636871](http://typora.hflxhn.com/documents/2024/0126/image-20240126095636871.png)

```bash
# 从虚拟机复制文件到本地
Copy-VMGuestFile -Source "/etc/hosts" -Destination "c:\temp\" -VM "cdh-data-governance-10.173.28.162-AlmaLinux-8" -GuestToLocal -GuestUser root -GuestPassword '3wsxzaq1`'

# 在虚拟机中执行命令
Invoke-VMScript -VM "cdh-data-governance-10.173.28.162-AlmaLinux-8" -ScriptText "ls" -GuestUser root -GuestPassword '3wsxzaq1`'
```

![image-20240126095711989](http://typora.hflxhn.com/documents/2024/0126/image-20240126095711989.png)

```bash
# 断开与vCenter服务器的连接
Disconnect-VIServer -Server 10.173.28.240 -Force -Confirm:$false
```

### vSphere Error

```bash
在BIOS下打开Monitor/Mwait特性，路径为：Advanced -> Intel RC Group -> Processor Configuration
```

```bash
原因：
VMware在打开EVC特性的时候，要求BIOS需要打开Monitor/Mwait特性，否则会报错
解决方案：
BIOS打开Monitor/Mwait特性
```

## vRA (VMware vRealize Automation)

### 部署要求

#### 1. 官方文档

```bash
https://docs.vmware.com/cn/vRealize-Automation/index.html
```

#### 2. 节点配置

| 用途                        | 磁盘大小                                     | CPU        | 内存           |
| :-------------------------- | -------------------------------------------- | ---------- | -------------- |
| VMware Aria Suite Lifecycle | 78G                                          | 2          | 6GB            |
| VMware Identity Manager     | 100G                                         | 8          | 16GB           |
| VMware Aria Automation      | 246G (仅适用单节点)<br />246G (仅适用单节点) | 12<br />24 | 48GB<br />96GB |

### 安装准备

#### 1. ntp 准备

```bash
yum install chrony -y

sed -i "s|^#allow.*|allow 0.0.0.0/0|" /etc/chrony.conf
grep allow /etc/chrony.conf

sed -i '/^server/d' /etc/chrony.conf
sed -i '/server/a \
ntp.aliyun.com iburst' /etc/chrony.conf

systemctl enable chronyd.service && systemctl restart chronyd.service
chronyc sources
```

#### 2. dns 准备

```bash
yum install unbound -y

sed -i 's/# interface: 0.0.0.0$/interface: 0.0.0.0/' /etc/unbound/unbound.conf
sed -i 's|# access-control: 0.0.0.0/0 refuse$|access-control: 0.0.0.0/0 allow|' /etc/unbound/unbound.conf

cat << "END" > /etc/unbound/local.d/moonpac.com.conf
local-zone: "moonpac.com." static
local-data: "moonpac.com. IN SOA ns.moonpac.com. root.moonpac.com. 1 1h 1h 1h 1h"
local-data: "moonpac.com. IN NS ns.moonpac.com."
local-data: "ns.moonpac.com. IN A 10.173.28.219"

local-data: "vcenter.moonpac.com. IN A 10.173.28.241"
local-data: "vrlcm.moonpac.com. IN A 172.24.98.192"
local-data: "vidm.moonpac.com. IN A 172.24.98.191"
local-data: "vra.moonpac.com. IN A 172.24.98.190"

local-data-ptr: "10.173.28.241 vcenter.moonpac.com"
local-data-ptr: "172.24.98.192 vrlcm.moonpac.com"
local-data-ptr: "172.24.98.191 vidm.moonpac.com"
local-data-ptr: "172.24.98.190 vra.moonpac.com"
END

cat << "END" > /etc/unbound/conf.d/forward.com.conf
server:
    domain-insecure: "com."
    domain-insecure: "net."
    domain-insecure: "org."
forward-zone:
    name: "."
    forward-addr: 114.114.114.114
END

unbound-control-setup

systemctl enable unbound.service --now
systemctl disable firewalld.service --now

ss -tunlp | grep :53
```

### 开始安装

#### 1. 挂载拷贝

```bash
sudo mount /iso/VMware-Aria-Automation-Lifecycle-Installer-21628956.iso /cdrom/
cp -r /cdrom/* ~/Templates/
chmod -R 777 ~/Templates/{ova,vrlcm,vrlcm-ui-installer}
```

![image-20240325153855928](http://typora.hflxhn.com/documents/2024/0325/image-20240325153855928.png)

#### 2. 接受协议

![image-20240325153950280](http://typora.hflxhn.com/documents/2024/0325/image-20240325153950280.png)

#### 3. 部署目标

![image-20240325154100463](http://typora.hflxhn.com/documents/2024/0325/image-20240325154100463.png)

#### 4. 安装位置

![image-20240325154148350](http://typora.hflxhn.com/documents/2024/0325/image-20240325154148350.png)

#### 5. 选择计算资源

![image-20240325154208635](http://typora.hflxhn.com/documents/2024/0325/image-20240325154208635.png)

#### 6. 选择存储位置

![image-20240325154438236](http://typora.hflxhn.com/documents/2024/0325/image-20240325154438236.png)

#### 7. 网络配置

![image-20240325154515257](http://typora.hflxhn.com/documents/2024/0325/image-20240325154515257.png)

#### 8. 密码配置

![image-20240325154607666](http://typora.hflxhn.com/documents/2024/0325/image-20240325154607666.png)

#### 9. vRLCM 配置

![image-20240325154656161](http://typora.hflxhn.com/documents/2024/0325/image-20240325154656161.png)

#### 10. vIDM 配置

![image-20240325154831314](http://typora.hflxhn.com/documents/2024/0325/image-20240325154831314.png)

#### 11. vRA 配置

![image-20240325154939400](http://typora.hflxhn.com/documents/2024/0325/image-20240325154939400.png)

#### 12. 开始安装

![image-20240325153050468](http://typora.hflxhn.com/documents/2024/0325/image-20240325153050468.png)

#### 13. 完成安装

![image-20240326100119084](http://typora.hflxhn.com/documents/2024/0325/image-20240326100119084.png)
