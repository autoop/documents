# rhel 系统安装 kvm 虚拟化

## 图形化工具

```bash
# 安装 virt-manage
yum install virt-manager -y

# 启动 libvirtd 服务
systemctl enable libvirtd --now

# virt-manage 管理工具
virt-manager
```

## 命令行工具

### 1. 安装 virt-install

```bash
yum install virt-install -y

# --name           虚拟机名称
# --memory         内存
# --vcpus          cpu
# --import         导入现有的磁盘文件
# --os-variant     虚拟机类型 centos-stream8, centos7, win7, win10, ubuntu16.04, ubuntu20.04
# --disk           磁盘位置  bus=virtio: 总线类型, 默认识别sda,sdb. virtio会被识别vda, vdb.
# --network        网桥 model=virtio, 为了增加性能也使用 virtio
# --network type=direct,source=enp2s0-vlan-10,source_mode=bridge,model=virtio
# --noautoconsole  不弹出界面
# --graphics vnc
```

### 2. 虚拟机安装

```bash
# 本地安装
virt-install \
--name test-1 \
--memory 1024 \
--vcpus 1 \
--import --os-variant centos-stream8 \
--disk path=/virtual/centos-stream-8/vda.qcow2,bus=virtio \
--cdrom /iso/CentOS-Stream-8-x86_64-20220120-dvd1.iso \
--network bridge=default,model=virtio \
--graphics vnc \
--noautoconsole

# 自动化安装
virt-install \
--name test-2 \
--memory 1024 \
--vcpus 1 \
--import --os-variant centos-stream8 \
--disk path=/virtual/vda.qcow2,bus=virtio \
--location http://172.24.0.248/web/cdrom/CentOS-Stream/8/ \
--network bridge=default,model=virtio \
--extra-args ks=http://172.24.0.248/ks8.cfg \
--graphics vnc \
--noautoconsole
```

### 3. 虚拟机管理

```bash
# 列出虚拟机
virsh list --all

# 干掉虚拟机
virsh destroy test-1
virsh undefine test-1

# 进入控制台
virsh console test-1
```

### 4. xml 文件

```bash
# 如果 virsh destory 之后再 virsh undefine 虚拟机, 这个文件就会消失
# 如果 xml 文件做过高度定制化修改, 那么一定要备份, 下次拉起虚拟机就应该使用 virsh define /path/xxx.xml, 因为 xml 的高级部分并不被 virt-install 的所有参数支持.
cat /etc/libvirt/qemu/centos-7.xml
```

## 虚拟化网络管理

### 1. 安装 libvirt-client

```bash
yum install libvirt-client -y
```

### 2. 查看虚拟网络

```bash
virsh net-list
virsh net-list --all
```

### 3. 删除虚拟网桥

```bash
# 删除 default 虚拟网桥
# 此方法清理 linux bridge 属于是虚拟化层面上的清理, 对于虚拟机来说可以使用 libvirt 提供的虚拟网桥, 也可以使用 NetworkManage 提供的网桥.
virsh net-destroy default
virsh net-undefine default
```

### 4. 创建虚拟网桥

```bash
# 创建虚拟网桥, 生成配置文件
net-add='172.16'
cat > /usr/share/libvirt/networks/net-${net-add}.0.0.xml << 'END'
<network>
    <name>net-${net-add}.0.0</name>
    <bridge name="net-${net-add}.0.0"></bridge>
    <forward></forward>
    <ip address="${net-add}.0.1" netmask="255.255.255.0">
        <dhcp>
            <range start="${net-add}.0.50" end="${net-add}.0.99"></range>
        </dhcp>
    </ip>
</network>
END

# 启用新建的 NAT 网络
virsh net-define /usr/share/libvirt/networks/net-${net-add}.0.0.xml
virsh net-start net-${net-add}.0.0
virsh net-autostart net-${net-add}.0.0
```

## nmcli 管理虚拟化网络

### 1. 不同 vlan 配置

```bash
# 创建网桥, 网桥一般不需要配置 ip 地址
nmcli con add con-name br-vlan-100 ifname br-vlan-100 type bridge ipv4.method disab ipv6.meth disab stp no
# 创建网桥, 且给网桥配置地址
nmcli con add con-name br-vlan-100 ifname br-vlan-100 type bridge ipv4.add 172.24.100.2/24 ipv4.gate 172.24.100.1 ipv4.dns 114.114.114.114 ipv4.meth manual ipv6.meth manual ipv6.meth disab stp no

# 创建 vlan100, 并将 vlan100 桥接到刚刚创建的网桥上. 
nmcli con add con-name enp2s0-vlan-100 ifname enp2s0-vlan-100 type vlan id 100 dev enp2s0 slave-type bridge master br-vlan-100

# 查看新建的网络设备
nmcli device

# 查看网络配置文件
nmcli con show

# 查看网桥地址
ip a show br-vlan-100
```

### 2. 无 vlan 配置

```bash
# 创建网桥
nmcli con add con-name vm-bridge ifname vm-bridge type bridge ipv4.addresses 172.24.100.2/24 ipv4.gateway 172.24.100.1 ipv4.dns 114.114.114.114 ipv4.method manual ipv6.method disab

# 配置桥接
nmcli con add con-name enp2s0 type ether ifname enp2s0 slave-type bridge master vm-bridge
nmcli con up enp2s0
```

## 管理 qcow2

> qcow2 文件是可以修改的, 但是需要工具.

### 1. 安装 virt-customize

```bash
yum install libguestfs libguestfs-tools-c -y
```

### 2. 修改 qcow2 root 密码

```bash
virt-customize -a CentOS-Stream-GenericCloud-8-20220125.1.x86_64.qcow2 --root-password password:123 --selinux-relabel
```

### 3. 扩容 qcow2 根分区

```bash
# 查看 qcow2 信息
qemu-img info CentOS-Stream-GenericCloud-8-20220125.1.x86_64.qcow2

# 查看 qcow2 文件的分区情况
virt-filesystems --long -h --all -a CentOS-Stream-GenericCloud-8-20220125.1.x86_64.qcow2

# 扩容 qcow2
qemu-img create -f qcow2 vda.qcow2 100G
virt-resize --expand /dev/sda1 CentOS-Stream-GenericCloud-8-20220125.1.x86_64.qcow2 vda.qcow2
```

## 串口控制台

```bash
使用 virsh console 命令，可以连接到虚拟机的串行控制台(VM)。
但虚拟机有以下情况时很有用：
● 没有提供 VNC 或 SPICE 协议，因此没有为 GUI 工具提供视频显示。
● 没有网络连接，因此无法使用 SSH 进行交互。
```

### 1. rhel 配置

```bash
# 在虚拟机上，编辑 /etc/default/grub 文件。
sed -i 's/GRUB_CMDLINE_LINUX="[^"]*/& console=tty0 console=ttyS0,115200n8/' /etc/default/grub

# 重新载入 Grub 配置
grub2-mkconfig -o /boot/grub2/grub.cfg
```

### 2. ubuntu 配置

```bash
# 在虚拟机上，编辑 /etc/default/grub 文件。
sed -i 's/GRUB_CMDLINE_LINUX="[^"]*/& console=tty0 console=ttyS0,115200n8/' /etc/default/grub

# 重新载入 Grub 配置
update-grub
```
