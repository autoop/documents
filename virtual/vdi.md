# Virtual Desktop Infrastructure

## 节点规划

| 服务       | 主机名                    | OS               | IP地址        | 用户名                      | 密码       |
| ---------- | ------------------------- | ---------------- | ------------- | --------------------------- | ---------- |
| DNS Server | dns.moonpac.com           | AlmaLinux 8      | 10.173.28.219 | root                        | Iop[]=-09* |
| NTP Server | ntp.moonpac.com           | AlmaLinux 8      | 10.173.28.219 | root                        | Iop[]=-09* |
| vCenter    | vcenter.moonpac.com       | VMware Photon OS | 172.24.98.200 | administrator@vsphere.local | Iop[]=-09* |
| ESXi       | dev1.esxi.moonpac.com     | VMware ESXi      | 172.24.98.250 | root                        | Iop[]=-09* |
| AD Server  | ad.vdi.moonpac.com        | Win Server 2019  | 10.173.21.100 | administrator               | Iop[]=-09* |
| Horizon    | horizon.vdi.moonpac.com   | Win Server 2019  | 10.173.21.101 | administrator               | Iop[]=-09* |
| NSX        | nsx.vdi.moonpac.com       |                  | 10.173.21.102 | root                        | Iop[]=-09* |
| vRops      | vrops.vdi.moonpac.com     | VMware Photon OS | 10.173.21.103 | root                        | Iop[]=-09* |
| LogInsight | vli.vdi.moonpac.com       | VMware Photon OS | 10.173.21.104 | root                        | Iop[]=-09* |
| SqlServer  | sqlserver.vdi.moonpac.com | Win Server 2019  | 10.173.21.105 | administrator               | Iop[]=-09* |
| Win10-VDI  |                           | Win 10           |               | administrator               | Iop[]=-09* |

## Win 2019 Template

### 新建虚拟机

#### 1. 选择创建类型

![image-20240828090508615](http://typora.hflxhn.com/documents/2024/0829/image-20240828090508615.png)

#### 2. 选择名称和文件夹

![image-20240828090541235](http://typora.hflxhn.com/documents/2024/0829/image-20240828090541235.png)

#### 3. 选择计算资源

![image-20240828090606040](http://typora.hflxhn.com/documents/2024/0829/image-20240828090606040.png)

#### 4. 选择存储

![image-20240828090727271](http://typora.hflxhn.com/documents/2024/0829/image-20240828090727271.png)

#### 5. 选择兼容性

![image-20240828090752112](http://typora.hflxhn.com/documents/2024/0829/image-20240828090752112.png)

#### 6. 选择客户机操作系统

![image-20240828090808589](http://typora.hflxhn.com/documents/2024/0829/image-20240828090808589.png)

#### 7. 自定义硬件

##### 7.1 CPU

![image-20240828090909169](http://typora.hflxhn.com/documents/2024/0829/image-20240828090909169.png)

##### 7.2 内存

![image-20240828091015416](http://typora.hflxhn.com/documents/2024/0829/image-20240828091015416.png)

##### 7.3 硬盘

![image-20240828091044817](http://typora.hflxhn.com/documents/2024/0829/image-20240828091044817.png)

##### 7.4 网络

![image-20240828091124743](http://typora.hflxhn.com/documents/2024/0829/image-20240828091124743.png)

##### 7.5 CD/DVD 驱动器

![image-20240828091203264](http://typora.hflxhn.com/documents/2024/0829/image-20240828091203264.png)

#### 8. 即将完成

![image-20240828091233375](http://typora.hflxhn.com/documents/2024/0829/image-20240828091233375.png)

### 系统安装

#### 1. 选择创建的虚拟机

![image-20240828092155134](http://typora.hflxhn.com/documents/2024/0829/image-20240828092155134.png)

#### 2. 打开 WEB 控制台

>   在如下界面需要按 “ESC” 键进入安装界面

![image-20240828092335396](http://typora.hflxhn.com/documents/2024/0829/image-20240828092335396.png)

#### 3. 选择语言

![image-20240828092429509](http://typora.hflxhn.com/documents/2024/0829/image-20240828092429509.png)

#### 4. 现在安装

![image-20240828092516983](http://typora.hflxhn.com/documents/2024/0829/image-20240828092516983.png)

#### 5. 没有产品密钥

![image-20240828092551503](http://typora.hflxhn.com/documents/2024/0829/image-20240828092551503.png)

#### 6. 选择桌面体验

![image-20240828092613579](http://typora.hflxhn.com/documents/2024/0829/image-20240828092613579.png)

#### 7. 接受许可

![image-20240828092640624](http://typora.hflxhn.com/documents/2024/0829/image-20240828092640624.png)

#### 8. 自定义

![image-20240828092709053](http://typora.hflxhn.com/documents/2024/0829/image-20240828092709053.png)

#### 9. 选择硬盘

![image-20240828092725406](http://typora.hflxhn.com/documents/2024/0829/image-20240828092725406.png)

#### 10. 开始安装

![image-20240828092754784](http://typora.hflxhn.com/documents/2024/0829/image-20240828092754784.png)

#### 11. 设置管理员密码

![image-20240828093233732](http://typora.hflxhn.com/documents/2024/0829/image-20240828093233732.png)

#### 12. 登录系统

![image-20240828093347207](http://typora.hflxhn.com/documents/2024/0829/image-20240828093347207.png)

#### 13. 安装 VM Tools

![image-20240828093523371](http://typora.hflxhn.com/documents/2024/0829/image-20240828093523371.png)

![image-20240828093540781](http://typora.hflxhn.com/documents/2024/0829/image-20240828093540781.png)

![image-20240828093735068](http://typora.hflxhn.com/documents/2024/0829/image-20240828093735068.png)

### 系统配置

#### 1. 设置分辨率

![image-20240828093942668](http://typora.hflxhn.com/documents/2024/0829/image-20240828093942668.png)

#### 2. 设置远程登录

![image-20240828094113079](http://typora.hflxhn.com/documents/2024/0829/image-20240828094113079.png)

#### 3. 激活系统

```bash
slmgr /ipk N69G4-B89J2-4G8F4-WWYCC-J464C
slmgr /skms kms.03k.org
slmgr /ato
```

### 转换为模板

![image-20240828110044621](http://typora.hflxhn.com/documents/2024/0829/image-20240828110044621.png)

## Win 10 Template

### 新建虚拟机

#### 1. CPU

![image-20240828100235754](http://typora.hflxhn.com/documents/2024/0829/image-20240828100235754.png)

#### 2. 内存

![image-20240828100257170](http://typora.hflxhn.com/documents/2024/0829/image-20240828100257170.png)

#### 3. 硬盘

![image-20240828100321970](http://typora.hflxhn.com/documents/2024/0829/image-20240828100321970.png)

#### 4. 网络

![image-20240828100347165](http://typora.hflxhn.com/documents/2024/0829/image-20240828100347165.png)

#### 5. CD/DVD

![image-20240828100412365](http://typora.hflxhn.com/documents/2024/0829/image-20240828100412365.png)

#### 6. 即将完成

![image-20240828100427973](http://typora.hflxhn.com/documents/2024/0829/image-20240828100427973.png)

### 系统安装

#### 1. 选择磁盘

![image-20240828101701349](http://typora.hflxhn.com/documents/2024/0829/image-20240828101701349.png)

#### 2. 选择区域

![image-20240828104202549](http://typora.hflxhn.com/documents/2024/0829/image-20240828104202549.png)

#### 3. 创建用户

![image-20240828104311085](http://typora.hflxhn.com/documents/2024/0829/image-20240828104311085.png)

#### 4. 设置密码

![image-20240828104327330](http://typora.hflxhn.com/documents/2024/0829/image-20240828104327330.png)

#### 5. 设置隐私

![image-20240828104417473](http://typora.hflxhn.com/documents/2024/0829/image-20240828104417473.png)

#### 6. 安装 VM Tools

![image-20240828104630368](http://typora.hflxhn.com/documents/2024/0829/image-20240828104630368.png)

### 系统配置

#### 1. 设置分辨率

![image-20240828105518243](http://typora.hflxhn.com/documents/2024/0829/image-20240828105518243.png)

#### 2. 设置远程登录

![image-20240828105629302](http://typora.hflxhn.com/documents/2024/0829/image-20240828105629302.png)

## 虚拟机自定义规范

### Win 2019

![image-20240828142816674](http://typora.hflxhn.com/documents/2024/0829/image-20240828142816674.png)

![image-20240828142905802](http://typora.hflxhn.com/documents/2024/0829/image-20240828142905802.png)

![image-20240828142918766](http://typora.hflxhn.com/documents/2024/0829/image-20240828142918766.png)

![image-20240828142946904](http://typora.hflxhn.com/documents/2024/0829/image-20240828142946904.png)

![image-20240828143027787](http://typora.hflxhn.com/documents/2024/0829/image-20240828143027787.png)

![image-20240828143144383](http://typora.hflxhn.com/documents/2024/0829/image-20240828143144383.png)

![image-20240828143206379](http://typora.hflxhn.com/documents/2024/0829/image-20240828143206379.png)

![image-20240828143242112](http://typora.hflxhn.com/documents/2024/0829/image-20240828143242112.png)

![image-20240828143353639](http://typora.hflxhn.com/documents/2024/0829/image-20240828143353639.png)

![image-20240828143422047](http://typora.hflxhn.com/documents/2024/0829/image-20240828143422047.png)

![image-20240828143443141](http://typora.hflxhn.com/documents/2024/0829/image-20240828143443141.png)

### Win 10

![image-20240829115436366](http://typora.hflxhn.com/documents/2024/0829/image-20240829115436366.png)

![image-20240829115455862](http://typora.hflxhn.com/documents/2024/0829/image-20240829115455862.png)

![image-20240829115520749](http://typora.hflxhn.com/documents/2024/0829/image-20240829115520749.png)

![image-20240829115606280](http://typora.hflxhn.com/documents/2024/0829/image-20240829115606280.png)

![image-20240829115631236](http://typora.hflxhn.com/documents/2024/0829/image-20240829115631236.png)

![image-20240829115645849](http://typora.hflxhn.com/documents/2024/0829/image-20240829115645849.png)

![image-20240829115655662](http://typora.hflxhn.com/documents/2024/0829/image-20240829115655662.png)

![image-20240829115922251](http://typora.hflxhn.com/documents/2024/0829/image-20240829115922251.png)

![image-20240829115818984](http://typora.hflxhn.com/documents/2024/0829/image-20240829115818984.png)

![image-20240829115842159](http://typora.hflxhn.com/documents/2024/0829/image-20240829115842159.png)

![image-20240829134259275](http://typora.hflxhn.com/documents/2024/0829/image-20240829134259275.png)

![image-20240829134309476](http://typora.hflxhn.com/documents/2024/0829/image-20240829134309476.png)

## AD 域配置

### 创建 AD

![image-20240828105909691](http://typora.hflxhn.com/documents/2024/0829/image-20240828105909691.png)

![image-20240828110127714](http://typora.hflxhn.com/documents/2024/0829/image-20240828110127714.png)

![image-20240828110140719](http://typora.hflxhn.com/documents/2024/0829/image-20240828110140719.png)

![image-20240828110153691](http://typora.hflxhn.com/documents/2024/0829/image-20240828110153691.png)

![image-20240828110203818](http://typora.hflxhn.com/documents/2024/0829/image-20240828110203818.png)

### 设置 IP

![image-20240828111519236](http://typora.hflxhn.com/documents/2024/0829/image-20240828111519236.png)

### 修改计算机名

![image-20240828111955480](http://typora.hflxhn.com/documents/2024/0829/image-20240828111955480.png)

### 安装 AD 域

![image-20240828112951953](http://typora.hflxhn.com/documents/2024/0829/image-20240828112951953.png)

![image-20240828113718646](http://typora.hflxhn.com/documents/2024/0829/image-20240828113718646.png)

### 提升域控制器

![image-20240828113941137](http://typora.hflxhn.com/documents/2024/0829/image-20240828113941137.png)

![image-20240828114038814](http://typora.hflxhn.com/documents/2024/0829/image-20240828114038814.png)

![image-20240828114220220](http://typora.hflxhn.com/documents/2024/0829/image-20240828114220220.png)

![image-20240828114338888](http://typora.hflxhn.com/documents/2024/0829/image-20240828114338888.png)

![image-20240828114438746](http://typora.hflxhn.com/documents/2024/0829/image-20240828114438746.png)

### 创建用户

![image-20240828134416880](http://typora.hflxhn.com/documents/2024/0829/image-20240828134416880.png)

![image-20240828134551081](http://typora.hflxhn.com/documents/2024/0829/image-20240828134551081.png)

![image-20240828134715255](http://typora.hflxhn.com/documents/2024/0829/image-20240828134715255.png)

![image-20240828134748686](http://typora.hflxhn.com/documents/2024/0829/image-20240828134748686.png)

![image-20240828134822240](http://typora.hflxhn.com/documents/2024/0829/image-20240828134822240.png)

![image-20240828135246679](http://typora.hflxhn.com/documents/2024/0829/image-20240828135246679.png)

![image-20240828135656047](http://typora.hflxhn.com/documents/2024/0829/image-20240828135656047.png)

## SqlServer 部署

### 系统安装

![image-20240829092438125](http://typora.hflxhn.com/documents/2024/0829/image-20240829092438125.png)



![image-20240829092458389](http://typora.hflxhn.com/documents/2024/0829/image-20240829092458389.png)



![image-20240829092511607](http://typora.hflxhn.com/documents/2024/0829/image-20240829092511607.png)



![image-20240829092528333](http://typora.hflxhn.com/documents/2024/0829/image-20240829092528333.png)



![image-20240829092545055](http://typora.hflxhn.com/documents/2024/0829/image-20240829092545055.png)



![image-20240829092609880](http://typora.hflxhn.com/documents/2024/0829/image-20240829092609880.png)



![image-20240829092622161](http://typora.hflxhn.com/documents/2024/0829/image-20240829092622161.png)

### SQLServer 安装

#### 1. 获取安装包

![image-20240829093637197](http://typora.hflxhn.com/documents/2024/0829/image-20240829093637197.png)

#### 2. 挂载 ISO

![image-20240829093734019](http://typora.hflxhn.com/documents/2024/0829/image-20240829093734019.png)

#### 3. 运行 setup

![image-20240829093830155](http://typora.hflxhn.com/documents/2024/0829/image-20240829093830155.png)

#### 4. 安装全新的 SQL Server

![image-20240829094039586](http://typora.hflxhn.com/documents/2024/0829/image-20240829094039586.png)

#### 5. 产品密钥

![image-20240829094216783](http://typora.hflxhn.com/documents/2024/0829/image-20240829094216783.png)

#### 6. 许可条款

![image-20240829094246467](http://typora.hflxhn.com/documents/2024/0829/image-20240829094246467.png)

#### 7. Microsoft 更新

![image-20240829094453592](http://typora.hflxhn.com/documents/2024/0829/image-20240829094453592.png)

#### 8. 产品更新

>   取掉对钩

![image-20240829095254007](http://typora.hflxhn.com/documents/2024/0829/image-20240829095254007.png)

#### 9. 安装规则

![image-20240829095412467](http://typora.hflxhn.com/documents/2024/0829/image-20240829095412467.png)

#### 10. 关闭防火墙

![image-20240829095551673](http://typora.hflxhn.com/documents/2024/0829/image-20240829095551673.png)

#### 11. 功能选择

![image-20240829095817446](http://typora.hflxhn.com/documents/2024/0829/image-20240829095817446.png)

#### 12. 实例配置

![image-20240829095940769](http://typora.hflxhn.com/documents/2024/0829/image-20240829095940769.png)

#### 13. 服务器配置

![image-20240829100156412](http://typora.hflxhn.com/documents/2024/0829/image-20240829100156412.png)

#### 14. 数据库引擎配置

![image-20240829103955059](http://typora.hflxhn.com/documents/2024/0829/image-20240829103955059.png)

#### 15. 准备安装

![image-20240829104028939](http://typora.hflxhn.com/documents/2024/0829/image-20240829104028939.png)

#### 16. 完成安装

![image-20240829104944364](http://typora.hflxhn.com/documents/2024/0829/image-20240829104944364.png)

### SSMS 安装

![image-20240829113256340](http://typora.hflxhn.com/documents/2024/0829/image-20240829113256340.png)

![image-20240829113652393](http://typora.hflxhn.com/documents/2024/0829/image-20240829113652393.png)

### 创建数据库

![image-20240829114246523](http://typora.hflxhn.com/documents/2024/0829/image-20240829114246523.png)

![image-20240829114354341](http://typora.hflxhn.com/documents/2024/0829/image-20240829114354341.png)

![image-20240829114455400](http://typora.hflxhn.com/documents/2024/0829/image-20240829114455400.png)

## Horizon 安装

###  虚拟机创建

![image-20240828143655130](http://typora.hflxhn.com/documents/2024/0829/image-20240828143655130.png)

![image-20240828143724498](http://typora.hflxhn.com/documents/2024/0829/image-20240828143724498.png)

![image-20240828143736480](http://typora.hflxhn.com/documents/2024/0829/image-20240828143736480.png)

![image-20240828143749538](http://typora.hflxhn.com/documents/2024/0829/image-20240828143749538.png)

![image-20240828143800010](http://typora.hflxhn.com/documents/2024/0829/image-20240828143800010.png)

![image-20240828143928383](http://typora.hflxhn.com/documents/2024/0829/image-20240828143928383.png)

![image-20240828143936902](http://typora.hflxhn.com/documents/2024/0829/image-20240828143936902.png)

### 加入域

![image-20240828145146499](http://typora.hflxhn.com/documents/2024/0829/image-20240828145146499.png)

![image-20240828145238196](http://typora.hflxhn.com/documents/2024/0829/image-20240828145238196.png)

![image-20240828145303012](http://typora.hflxhn.com/documents/2024/0829/image-20240828145303012.png)



### 安装 Horizon

#### 准备安装包

![image-20240828150426398](http://typora.hflxhn.com/documents/2024/0829/image-20240828150426398.png)

#### 获取 Keys

![image-20240828151517028](http://typora.hflxhn.com/documents/2024/0829/image-20240828151517028.png)

#### 运行安装程序

![image-20240828151701114](http://typora.hflxhn.com/documents/2024/0829/image-20240828151701114.png)

![image-20240828151721053](http://typora.hflxhn.com/documents/2024/0829/image-20240828151721053.png)

![image-20240828152247509](http://typora.hflxhn.com/documents/2024/0829/image-20240828152247509.png)

![image-20240828152303918](http://typora.hflxhn.com/documents/2024/0829/image-20240828152303918.png)

![image-20240828152332797](http://typora.hflxhn.com/documents/2024/0829/image-20240828152332797.png)

![image-20240828152408974](http://typora.hflxhn.com/documents/2024/0829/image-20240828152408974.png)

![image-20240828152649545](http://typora.hflxhn.com/documents/2024/0829/image-20240828152649545.png)

![image-20240828152701324](http://typora.hflxhn.com/documents/2024/0829/image-20240828152701324.png)

![image-20240828153426982](http://typora.hflxhn.com/documents/2024/0829/image-20240828153426982.png)

### Horizon 配置

#### 登录 Horizon

[https://horizon.vdi.moonpac.com/admin/#/login](https://horizon.vdi.moonpac.com/admin/#/login)

![image-20240828154809352](http://typora.hflxhn.com/documents/2024/0829/image-20240828154809352.png)

#### 配置许可

![image-20240828154851450](http://typora.hflxhn.com/documents/2024/0829/image-20240828154851450.png)

#### 添加 vCenter

![image-20240828155650053](http://typora.hflxhn.com/documents/2024/0829/image-20240828155650053.png)

![image-20240828155113638](http://typora.hflxhn.com/documents/2024/0829/image-20240828155113638.png)

![image-20240828155310586](http://typora.hflxhn.com/documents/2024/0829/image-20240828155310586.png)

![image-20240828155336206](http://typora.hflxhn.com/documents/2024/0829/image-20240828155336206.png)

![image-20240828155428630](http://typora.hflxhn.com/documents/2024/0829/image-20240828155428630.png)

![image-20240828155439178](http://typora.hflxhn.com/documents/2024/0829/image-20240828155439178.png)

#### 添加事件数据库

![image-20240829115013099](http://typora.hflxhn.com/documents/2024/0829/image-20240829115013099.png)

![image-20240829115156375](http://typora.hflxhn.com/documents/2024/0829/image-20240829115156375.png)

## vRops 部署

### 系统安装

#### 1. OVF 导入

![image-20240828172900920](http://typora.hflxhn.com/documents/2024/0829/image-20240828172900920.png)

#### 2. 虚拟机名称

![image-20240828173034091](http://typora.hflxhn.com/documents/2024/0829/image-20240828173034091.png)

#### 3. 选择计算资源

![image-20240828173104314](http://typora.hflxhn.com/documents/2024/0829/image-20240828173104314.png)

#### 4. 查看详细信息

![image-20240828173148436](http://typora.hflxhn.com/documents/2024/0829/image-20240828173148436.png)

#### 5. 许可协议

![image-20240828173243816](http://typora.hflxhn.com/documents/2024/0829/image-20240828173243816.png)

#### 6. 配置

![image-20240828173325662](http://typora.hflxhn.com/documents/2024/0829/image-20240828173325662.png)

#### 7. 选择存储

![image-20240828173342157](http://typora.hflxhn.com/documents/2024/0829/image-20240828173342157.png)

#### 8. 选择网络

![image-20240828173357287](http://typora.hflxhn.com/documents/2024/0829/image-20240828173357287.png)

#### 9. 自定义模板

![image-20240828173659709](http://typora.hflxhn.com/documents/2024/0829/image-20240828173659709.png)

#### 10. 即将完成

![image-20240828173711081](http://typora.hflxhn.com/documents/2024/0829/image-20240828173711081.png)

### 初始服务配置

[https://vrops.vdi.moonpac.com/](https://vrops.vdi.moonpac.com/)

#### 1. 开始

![image-20240828180750327](http://typora.hflxhn.com/documents/2024/0829/image-20240828180750327.png)

#### 2. 新建集群

![image-20240828180803352](http://typora.hflxhn.com/documents/2024/0829/image-20240828180803352.png)

#### 3. 设置管理员凭据

![image-20240828180821646](http://typora.hflxhn.com/documents/2024/0829/image-20240828180821646.png)

#### 4. 选择证书

![image-20240828180916286](http://typora.hflxhn.com/documents/2024/0829/image-20240828180916286.png)

#### 5. 部署设置

![image-20240828180949181](http://typora.hflxhn.com/documents/2024/0829/image-20240828180949181.png)

#### 6. 配置可用性

![image-20240828181114813](http://typora.hflxhn.com/documents/2024/0829/image-20240828181114813.png)

#### 7. 节点

![image-20240828181125126](http://typora.hflxhn.com/documents/2024/0829/image-20240828181125126.png)

#### 8. 后续部署

![image-20240828181133679](http://typora.hflxhn.com/documents/2024/0829/image-20240828181133679.png)

#### 9. 启动集群

![image-20240829110137384](http://typora.hflxhn.com/documents/2024/0829/image-20240829110137384.png)

### vRops 使用

[https://vrops.vdi.moonpac.com/ui/login.action](https://vrops.vdi.moonpac.com/ui/login.action)

#### 1. 登录

![image-20240829110327314](http://typora.hflxhn.com/documents/2024/0829/image-20240829110327314.png)

#### 2. 欢迎使用

![image-20240829110414987](http://typora.hflxhn.com/documents/2024/0829/image-20240829110414987.png)

#### 3. 许可协议

![image-20240829110425646](http://typora.hflxhn.com/documents/2024/0829/image-20240829110425646.png)

#### 4. 许可密钥

>    JF69H-4DJ1K-480H8-N90QP-2AK64

![image-20240829110954966](http://typora.hflxhn.com/documents/2024/0829/image-20240829110954966.png)

#### 5. 改善计划

![image-20240829111009048](http://typora.hflxhn.com/documents/2024/0829/image-20240829111009048.png)

#### 6. 即将完成

![image-20240829111024234](http://typora.hflxhn.com/documents/2024/0829/image-20240829111024234.png)

#### 7. 集成新的 vCenter

![image-20240829111537771](http://typora.hflxhn.com/documents/2024/0829/image-20240829111537771.png)

![image-20240829111904431](http://typora.hflxhn.com/documents/2024/0829/image-20240829111904431.png)

## LogInsight 部署

### 系统安装

#### 1. OVF 导入

![image-20240828174414138](http://typora.hflxhn.com/documents/2024/0829/image-20240828174414138.png)

#### 2. 选择名称和文件夹

![image-20240828174517243](http://typora.hflxhn.com/documents/2024/0829/image-20240828174517243.png)

#### 3. 选择计算资源

![image-20240828174538722](http://typora.hflxhn.com/documents/2024/0829/image-20240828174538722.png)

#### 4. 查看详细信息

![image-20240828174616458](http://typora.hflxhn.com/documents/2024/0829/image-20240828174616458.png)

#### 5. 许可协议

![image-20240828174628526](http://typora.hflxhn.com/documents/2024/0829/image-20240828174628526.png)

#### 6. 配置

![image-20240828174638342](http://typora.hflxhn.com/documents/2024/0829/image-20240828174638342.png)

#### 7. 选择存储

![image-20240828174652206](http://typora.hflxhn.com/documents/2024/0829/image-20240828174652206.png)

#### 8. 选择网络

![image-20240828174703174](http://typora.hflxhn.com/documents/2024/0829/image-20240828174703174.png)

#### 9. 自定义模板

![image-20240828174858878](http://typora.hflxhn.com/documents/2024/0829/image-20240828174858878.png)

![image-20240828175019123](http://typora.hflxhn.com/documents/2024/0829/image-20240828175019123.png)

#### 10. 即将完成

![image-20240828175033161](http://typora.hflxhn.com/documents/2024/0829/image-20240828175033161.png)

### 服务配置

[https://vli.vdi.moonpac.com/](https://vli.vdi.moonpac.com/)

#### 1. 安装程序

![image-20240828181252172](http://typora.hflxhn.com/documents/2024/0829/image-20240828181252172.png)

#### 2. 选择部署类型

![image-20240828181306729](http://typora.hflxhn.com/documents/2024/0829/image-20240828181306729.png)

#### 3. 管理员凭证

![image-20240829104733354](http://typora.hflxhn.com/documents/2024/0829/image-20240829104733354.png)

#### 4. 许可证

>   JC002-0F110-48960-CC3Q6-1CAK4

![image-20240829105535353](http://typora.hflxhn.com/documents/2024/0829/image-20240829105535353.png)

#### 5. 常规配置

![image-20240829105547002](http://typora.hflxhn.com/documents/2024/0829/image-20240829105547002.png)

#### 6. 时间配置

![image-20240829105919812](http://typora.hflxhn.com/documents/2024/0829/image-20240829105919812.png)

#### 7. SMTP 配置

![image-20240829105950983](http://typora.hflxhn.com/documents/2024/0829/image-20240829105950983.png)

#### 8. SSL 配置

![image-20240829110010203](http://typora.hflxhn.com/documents/2024/0829/image-20240829110010203.png)

#### 9. 设置完成

![image-20240829110019792](http://typora.hflxhn.com/documents/2024/0829/image-20240829110019792.png)



#### 10. 添加 vCenter

![image-20240829112409185](http://typora.hflxhn.com/documents/2024/0829/image-20240829112409185.png)
