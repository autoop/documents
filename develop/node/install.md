# node js 安装

## node 下载

```bash
wget -c https://nodejs.org/dist/v20.11.0/node-v20.11.0-linux-x64.tar.xz
```

## 解压安装

```bash
tar xf node-v20.11.0-linux-x64.tar.xz -C /opt/
```

## 设置环境变量

```bash
grep node /etc/profile || echo 'export PATH=$PATH:/opt/node-v20.11.0-linux-x64/bin' >> /etc/profile
```