# 搭建 pip 内网仓库

## 下载 pip 包

```bash
pip3 download PyMySQL -d ./pip-repo -i https://pypi.tuna.tsinghua.edu.cn/simple
```

## 安装索引工具

```bash
pip3 download pip2pi -d ./pip-repo -i https://pypi.tuna.tsinghua.edu.cn/simple
pip3 install pip2pi -i https://pypi.tuna.tsinghua.edu.cn/simple
```

## 创建索引

```bash
cd /web/pip/
dir2pi .
```

## 配置 http 仓库

```bash
cat > /opt/nginx-1.22.0/conf/conf.d/pip.conf << 'END'
server {
    listen 80;
    root /web/pip;

    location / {
        autoindex on;
        autoindex_exact_size off;
        autoindex_localtime on;
    }
}
END

systemctl restart nginx-1.22.0.service
```

![image-20231024161625290](http://typora.hflxhn.com/documents/2023/1024/image-20231024161625290.png)

## 配置 pip 源

```bash
cat > ~/.pip/pip.conf << 'END'
[global]
trusted-host = 172.24.0.248
index-url = http://172.24.0.248:89/simple/
END
```

## 安装 PyMySQL

```bash
pip3 install PyMySQL
```

![image-20231024161750468](http://typora.hflxhn.com/documents/2023/1024/image-20231024161750468.png)