# Flatpak 沙盒程序

## 安装及配置

### 1. 安装 Flatpak

```bash
yum install flatpak -y
```

### 2. 配置上海交大源站

```bash
# 上海交大源站
https://sjtug.org/post/mirror-help/flathub/
# Flathub 官方站
https://flathub.org/

# 更换国内源
flatpak remote-add --if-not-exists flathub https://mirror.sjtu.edu.cn/flathub/flathub.flatpakrepo
flatpak remote-modify flathub --url=https://mirror.sjtu.edu.cn/flathub
```

### 3. 查看源

```bash
# 查看相关源
flatpak remotes

# 查看所有源
flatpak remotes -d

# 查看系统级源
flatpak remotes --system -d

# 查看当前级源
flatpak remotes --user -d
```

## Flatpak 安装 OBS

### 1. 安装

```bash
flatpak install flathub com.obsproject.Studio -y
```

![image-20230614114536530](http://typora.hflxhn.com/documents/2023/0614/image-20230614114536530.png)

### 2. 运行

```bash
flatpak run com.obsproject.Studio -y
```

![image-20230614114706216](http://typora.hflxhn.com/documents/2023/0614/image-20230614114706216.png)

## 安装其他软件

```bash
# 腾讯会议
flatpak install flathub com.tencent.wemeet -y
sed -i '/^Exec=/s/.*/& --no-sandbox/' /var/lib/flatpak/exports/share/applications/com.tencent.wemeet.desktop
chattr +i /var/lib/flatpak/exports/share/applications/com.tencent.wemeet.desktop
flatpak run com.tencent.wemeet --no-sandbox

# 百度网盘
flatpak install flathub com.baidu.NetDisk -y
sed -i '/^Exec=/s/.*/& --no-sandbox/' /var/lib/flatpak/exports/share/applications/com.baidu.NetDisk.desktop
chattr +i /var/lib/flatpak/exports/share/applications/com.baidu.NetDisk.desktop

# qqmusic
flatpak install flathub com.qq.QQmusic -y

# wps
flatpak install flathub com.wps.Office -y

# 迅雷
flatpak install flathub com.xunlei.Thunder -y

# putty
flatpak install flathub uk.org.greenend.chiark.sgtatham.putty -y

# 微信
flatpak install flathub com.tencent.WeChat -y

# qq
flatpak install flathub com.qq.QQ -y

# vnc
flatpak install flathub org.videolan.VLC -y

# Edge
flatpak install flathub com.microsoft.Edge -y
sed -i '/^Exec=/s/.*/& --no-sandbox/' /var/lib/flatpak/exports/share/applications/com.microsoft.Edge.desktop
chattr +i /var/lib/flatpak/exports/share/applications/com.microsoft.Edge.desktop

flatpak install flathub io.typora.Typora -y
sed -i '/^Exec=/s/.*/& --no-sandbox/' /var/lib/flatpak/exports/share/applications/io.typora.Typora.desktop
chattr +i /var/lib/flatpak/exports/share/applications/io.typora.Typora.desktop

flatpak install flathub org.remmina.Remmina -y

flatpak install flathub com.google.Chrome -y
sed -i '/^Exec=/s/.*/& --no-sandbox/' /var/lib/flatpak/exports/share/applications/com.google.Chrome.desktop
chattr +i /var/lib/flatpak/exports/share/applications/com.google.Chrome.desktop

flatpak install flathub net.xmind.XMind -y
sed -i '/^Exec=/s/.*/& --no-sandbox/' /var/lib/flatpak/exports/share/applications/net.xmind.XMind.desktop
chattr +i /var/lib/flatpak/exports/share/applications/net.xmind.XMind.desktop
```

