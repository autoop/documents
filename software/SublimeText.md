# Sublime Test 4

## Linux 安装 Sublime Test 4

### 1. 官网下载

[下载 sublime test 4 ](https://download.sublimetext.com/sublime_text_build_4126_x64.tar.xz)

```bash
https://download.sublimetext.com/sublime_text_build_4126_x64.tar.xz
```

### 2. 解压安装

```bash
tar xf sublime_text_build_4126_x64.tar.xz -C /opt
```

### 3. 创建快捷键

```bash
cat > /usr/share/applications/sublime_text.desktop << 'END'
[Desktop Entry]
Version=1.0
Type=Application
Name=Sublime Text
GenericName=Text Editor
Comment=Sophisticated text editor for code, markup and prose
Exec=/opt/sublime_text/sublime_text %F
Terminal=false
MimeType=text/plain;
Icon=sublime-text
Categories=TextEditor;Development;
StartupNotify=true
Actions=new-window;new-file;

[Desktop Action new-window]
Name=New Window
Exec=/opt/sublime_text/sublime_text --launch-or-new-window
OnlyShowIn=Unity;

[Desktop Action new-file]
Name=New File
Exec=/opt/sublime_text/sublime_text --command new_file
OnlyShowIn=Unity;
END
```

### 4. 设置图标

```bash
sudo cp -r /opt/sublime_text/Icon/16x16/apps/sublime-text.png   /usr/share/icons/hicolor/16x16/apps/sublime-text.png
sudo cp -r /opt/sublime_text/Icon/32x32/apps/sublime-text.png   /usr/share/icons/hicolor/32x32/apps/sublime-text.png
sudo cp -r /opt/sublime_text/Icon/48x48/apps/sublime-text.png   /usr/share/icons/hicolor/48x48/apps/sublime-text.png
sudo cp -r /opt/sublime_text/Icon/128x128/apps/sublime-text.png /usr/share/icons/hicolor/128x128/apps/sublime-text.png
sudo cp -r /opt/sublime_text/Icon/256x256/apps/sublime-text.png /usr/share/icons/hicolor/256x256/apps/sublime-text.png
sudo rm -rf /usr/share/icons/hicolor/icon-theme.cache
```

### 5. 设置缓存目录

```shell
# 安装好后不要着急打开, 进入到 sublime_text 目录
# 在该目录下创建 Data 目录. 和 Packages 目录同级
# 再打开 sublime 将默认装在该Data目录下.
mkdir -p /opt/sublime_text/Data/
```

## package control (插件管理)

```bash
# 依次打开 Perferences >> Browse Packages... >> (../Installed Packages)
# 将 "Package Control.sublime-package" 考入 "Installed Packages"
# 修改 "package control" 配置文件 (Preferences >> Package Settings >> Package Control >> Settings - User)
"remove_orphaned": false
```

## 常用插件安装

> 以下插件均使用快捷键 Ctrl+Shift+p 调出命令窗口 输入 "install package"

1. Emmet

2. SublimeLinter (语法检测插件)

3. sftp

4. SublimeREPL ( less, less2css )

   ```bash
   npm install -g less less-plugin-clean-css
   ```

5. DocBlockr

6. DocBlockr (注释快捷键)

   ```bash
   {
       "jsdocs_extra_tags":[
           "This is a cool function",
           "@Author username",
           "@DateTime {{datetime}}"
       ],
       "jsdocs_function_description": false
   }
   ```

7. JavaScript & NodeJS Snippets

8. Advanced New File (快速新建文件 win + alt + n)

9. git

10. GitGutter (标注文件每一行git状态)

11. Placeholders (快速生成占位符图像, 表单, 列表和表格)

12. Minify

13. WordCount 可以实时显示当前文件的字数

14. BracketHighlighter：显示我在哪个括号内

15. TrailingSpaces：强迫症患者必备 高亮显示尾部多余的空格

16. SublimeCodeIntel (代码提示插件)

17. jQuery

18. pretty json (格式化json alt + ctrl + j)

19. phpfmt php代码格式化

    ```python
    {
        "enable_auto_align": true,
        "format_on_save": false,
        "indent_with_space": true,
        "php_bin": "/opt/lnmp/php7.4/bin/php",
        "psr1": true,
        "psr1_naming": false,
        "psr2": true,
        "version": 1
    }

20. Vue Syntax Hightlight

## Sublime Text 配置

```python
{
    "auto_complete": true,                       # 关闭命令提示
    "auto_match_enabled": true,                  # 关闭自动补全 (单引号,双引号,圆括号,大括号)
    "font_size": 18,                             # 设置字体大小
    "highlight_line": true,                      # 打开当前操作行的背景线
    "ignored_packages":
    [
        ""
    ],                                           # 开启 vi 模式
    "line_numbers": true,                        # 关闭行号
    "save_on_focus_lost": true,                  # 开启自动保存
    "theme": "Adaptive.sublime-theme",           # 主题
    "translate_tabs_to_spaces": true,            # 将 tab 转换为 空格
    "update_check": false,                       # 关闭自动更新
    "vintage_start_in_command_mode": true,       # 以命令模式启动
    "color_scheme": "Packages/Color Scheme - Default/Mariana.sublime-color-scheme",
    "ensure_newline_at_eof_on_save": true,       # 保存时自动增加文件末尾换行
    "font_face": "黑体",
    "show_definitions": false,                   # 禁止显示 definitions
    "draw_white_space": "all",                   # 显示空白字符
    "trim_trailing_white_space_on_save": true,   # 保存时自动去除行末空白
    "rulers": [80, 100],                         # 添加行宽标尺
    "default_line_ending": "unix",               # 文件末行以 unix 格式保存
}
```

## 新增编译系统

```python
# php
{
    "cmd": ["php", "$file"],
    "file_regex": "php$",
    "selector": "source.php"
}

# node
{
    "cmd": ["node", "$file"],
    "selector": "source.js"
}

# go
{
    "shell_cmd": "/opt/go/bin/go run $file",
    "encoding": "utf-8"
}

# sass
# SASS - SASS.sublime-build
{
    "cmd": [
        "sass",
        "--update",
        "$file:${file_path}/../css/${file_base_name}.css",
        "--stop-on-error"
    ],
    "selector": "source.sass, source.scss",
    "line_regex": "Line ([0-9]+):",

    "osx":
    {
        "path": "/usr/local/bin:$PATH"
    },
    "windows":
    {
        "shell": "true"
    }
}
# SASS - Nomap - Compressed.sublime-build
{

    "cmd": [
        "sass",
        "--update",
        "$file:${file_path}/../css/${file_base_name}.css",
        "--stop-on-error",
        "--style",
        "compressed",
        "--no-source-map"
    ],
    "selector": "source.sass, source.scss",
    "line_regex": "Line ([0-9]+):",

    "osx":
    {
        "path": "/usr/local/bin:$PATH"
    },

    "windows":
    {
        "shell": "true"
    }
}
```
