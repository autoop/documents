# Typora Markdown 编辑器

## Typora 配置图片上传

### 1. 安装 picgo

```bash
# 通过 nodejs 安装 picgo
yum install nodejs -y
npm install picgo -g
picgo install super-prefix
mkdir -p /root/.config/Typora/picgo/linux
ln -s /usr/local/bin/picgo /root/.config/Typora/picgo/linux/picgo
```

### 2. 注册七牛云存储

#### 2.1 七牛云存储区域表

| 存储区域 | 区域代码 | 客户端上传地址                  | 服务端上传地址              |
| :------- | :------- | :------------------------------ | :-------------------------- |
| 华东     | z0       | http(s)://upload.qiniup.com     | http(s)://up.qiniup.com     |
| 华北     | z1       | http(s)://upload-z1.qiniup.com  | http(s)://up-z1.qiniup.com  |
| 华南     | z2       | http(s)://upload-z2.qiniup.com  | http(s)://up-z2.qiniup.com  |
| 北美     | na0      | http(s)://upload-na0.qiniup.com | http(s)://up-na0.qiniup.com |
| 东南亚   | as0      | http(s)://upload-as0.qiniup.com | http(s)://up-as0.qiniup.com |

#### 2.2 七牛云注册

[七牛云 - 注册](https://portal.qiniu.com/signup?ref=www.qiniu.com)

#### 2.3 新建空间

![image-20230611214109454](http://typora.hflxhn.com/documents/2023/0613/image-20230611214109454.png)

### 3. 配合七牛云存储

```json
# 依次打开 文件 > 偏好设置 > 图像
{
	"picBed": {
		"current": "qiniu",
		"qiniu": {
			"accessKey": "你的AccessKey",
			"secretKey": "你的SecretKey",
			"bucket": "存储空间的名称",
			"url": "http://绑定的域名地址.com",
			"area": "区域, 上文有描述",
			"options": "不用填",
			"path": "存储路径(建议为:img/)"
		}
	},
	"picgoPlugins": {
        "picgo-plugin-super-prefix": true
    },
    "picgo-plugin-super-prefix": {
        "prefixFormat": "YYYY/MMDD/",
        "fileFormat": ""
    }
}
```

![image-20230611210227567](http://typora.hflxhn.com/documents/2023/0613/image-20230611210227567.png)