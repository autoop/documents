# OpenWrt 软路由

## 1. 源码编译 OpenWrt

### 1.1 编译环境

```bash
# 务必准备一个ubuntu 20.04 + 的环境
# 当前编译 openwrt v22.03.5
```

### 1.2 依赖安装

```bash
sudo apt-get -y install build-essential asciidoc binutils bzip2 gawk gettext git libncurses5-dev libz-dev patch python3 python2.7 unzip zlib1g-dev lib32gcc1 libc6-dev-i386 subversion flex uglifyjs git-core gcc-multilib p7zip p7zip-full msmtp libssl-dev texinfo libglib2.0-dev xmlto qemu-utils upx libelf-dev autoconf automake libtool autopoint device-tree-compiler g++-multilib antlr3 gperf wget curl swig rsync
```

### 1.3 clone openwrt

```bash
git clone https://github.com/openwrt/openwrt.git
git checkout v22.03.5
```

### 1.4 修改默认 ip

```bash
sed -i 's/192.168.1.1/192.168.0.250/g' package/base-files/files/bin/config_generate
```

### 1.5 添加组件 (可选项)

#### 1.5.1 添加主题

```bash
git clone https://github.com/jerrykuku/luci-theme-argon.git package/luci-theme-argon
```

#### 1.5.2 添加 passwall (可选项)

```bash
cat >> feeds.conf.default << END
src-git passwall_packages https://github.com/xiaorouji/openwrt-passwall-packages.git;main
src-git passwall https://github.com/xiaorouji/openwrt-passwall.git;main
END
```

#### 1.5.3 添加 vlmcsd

```bash
git clone https://github.com/cokebar/openwrt-vlmcsd.git package/openwrt-vlmcsd
git clone https://github.com/cokebar/luci-app-vlmcsd.git package/luci-app-vlmcsd
```

#### 1.5.4 添加 AdGuardHome

```bash
git clone https://github.com/rufengsuixing/luci-app-adguardhome.git package/luci-app-adguardhome
```

#### 1.5.5 添加  ddns-go

```bash
git clone https://github.com/sirpdboy/luci-app-ddns-go.git package/ddns-go
```

### 1.6 更新并安装

```bash
./scripts/feeds clean
./scripts/feeds update -a
./scripts/feeds install -a
```

### 1.7 生成配置文件

```bash
make menuconfig
```

```bash
# > LuCI > 1. Collections
luci

# > LuCI > 2. Modules
luci-compat
luci-mod-dashboard

# > LuCI > 3. Applications
luci-app-adguardhome
luci-app-aria2
luci-app-ddns-go
luci-app-dockerman
luci-app-frpc
luci-app-openvpn
luci-app-passwall
luci-app-privoxy
luci-app-samba4
luci-app-statistics
luci-app-ttyd
luci-app-vlmcsd
luci-app-wol

# > LuCI > 5. Protocols
luci-proto-ipv6
luci-proto-ppp

# > Utilities > Editors
vim-full

# > Utilities > Shells
bash

#  > Utilities
shadow-utils
docker
docker-compose
dockerd
```

### 1.8 配置环境变量

```bash
export GO111MODULE=on
export GOPROXY=https://goproxy.io
```

### 1.9 忽略证书验证

```bash
export GIT_SSL_NO_VERIFY=1
git config --global http.sslVerify false
```

### 1.10 编译 openwrt

```bash
make -j16 V=s
```

### 1.11 磁盘转换

```bash
# vdi 转 qcow2
scp ops-test-ubuntu-server-20.04:/home/fanyang/openwrt/bin/targets/x86/64/openwrt-x86-64-generic-squashfs-combined.vdi .
qemu-img convert -f vdi -O qcow2 openwrt-x86-64-generic-squashfs-combined.vdi openwrt-x86-64-generic-squashfs-combined.vdi.qcow2

# img 转 qcow2
qemu-img convert -f raw -O qcow2 openwrt-x86-64-generic-ext4-combined-efi.img openwrt-x86-64-generic-ext4-combined-efi.img.qcow2
```

### 1.12 将 img 文件写入磁盘

```bash
# 尽可能的使用固态硬盘作为PE进行引导，使用U盘的速度将慢的可怜
gunzip -dc openwrt-x86-64-generic-ext4-combined-efi.img.gz | dd of=/dev/sdc
dd if=openwrt-x86-64-generic-ext4-combined-efi.img of=/dev/sdc
```

## 2. Passwall 配置

### 2.1 v2ray

```bash
tail -n 15 /etc/config/passwall
config nodes 'bNH8SKr4'
	option remarks 'v2ray'
	option type 'V2ray'
	option protocol 'vmess'
	option address 'vps.xxxxxx.com'
	option port '443'
	option security 'aes-128-gcm'
	option uuid 'xxx-xxx-xxx-xxx-xxx'
	option transport 'ws'
	option ws_host 'vps.xxxxxx.com'
	option ws_path '/xxxx'
	option tls '1'
	option alpn 'default'
	option tls_allowInsecure '1'
```

![image-20230725170140507](http://typora.hflxhn.com/documents/2023/0725/image-20230725170140507.png)

### 2.2 privoxy

```bash
# 访问控制 > Listen addresses > 192.168.0.250:8118
```

![image-20230725182245504](http://typora.hflxhn.com/documents/2023/0725/image-20230725182245504.png)

```bash
# 转发 > Forward SOCKS 5 > / 192.168.0.250:10808 .
```

![image-20230725182335977](http://typora.hflxhn.com/documents/2023/0725/image-20230725182335977.png)

### 2.3 install

```bash
# 确认系统 CPU 结构
root@GL-MT6000:/tmp/passwall# grep OPENWRT_ARCH /etc/os-release 
OPENWRT_ARCH="aarch64_cortex-a53"

# 下载 passwall
https://github.com/xiaorouji/openwrt-passwall/releases
# 1. passwall_packages_ipk_aarch64_cortex-a53.zip
# 2. luci-23.05_luci-i18n-passwall-zh-cn_git-24.199.60095-4dbb164_all.ipk
# 3. luci-23.05_luci-app-passwall_4.78-1_all.ipk

# 下载 passwall 依赖包
# https://downloads.openwrt.org/snapshots/packages/aarch64_cortex-a53/
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/luci-compat_git-22.046.85744-f08a0f6_all.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/kmod-ipt-iprange_5.4.238-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/iptables-mod-iprange_1.8.7-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/kmod-ipt-tproxy_5.4.238-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/iptables-mod-tproxy_1.8.7-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/libltdl7_2.4.6-2_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/haproxy_2.2.24-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/libmbedtls12_2.16.12-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/shadowsocks-libev-config_3.3.5-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/shadowsocks-libev-ss-local_3.3.5-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/shadowsocks-libev-ss-redir_3.3.5-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/libcares_1.17.2-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.com/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/shadowsocks-libev-ss-server_3.3.5-1_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/luci/luci-app-passwall_4.68-1_all.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/v2ray-core_5.7.0-1_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/v2ray-plugin_5.7.0-1_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/libsodium_1.0.18-2022-02-27-2e9b4c47-1_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/coreutils-base64_8.32-6_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/coreutils-nohup_8.32-6_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/chinadns-ng_2023.06.01-1_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/microsocks_1.0.3-3_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/tcping_0.3-2_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/ipt2socks_1.1.3-3_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/shadowsocks-rust-sslocal_1.16.2-1_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/shadowsocksr-libev-ssr-local_2.5.6-9_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/shadowsocksr-libev-ssr-redir_2.5.6-9_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/simple-obfs-client_0.0.5-2_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/simple-obfs-server_0.0.5-2_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/trojan-plus_10.0.3-2_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/packages/xray-core_1.8.3-1_aarch64_cortex-a53.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/luci/luci-compat_git-22.046.85744-f08a0f6_all.ipk
wget -c https://downloads.immortalwrt.org/releases/21.02-SNAPSHOT/packages/aarch64_cortex-a53/base/libltdl7_2.4.6-2_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/haproxy_2.2.24-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/libltdl7_2.4.6-2_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/libudns_0.4-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/boost_1.75.0-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/boost-system_1.75.0-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/boost-program_options_1.75.0-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/kmod-ipt-iprange_5.4.238-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/iptables-mod-iprange_1.8.7-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/kmod-ipt-tproxy_5.4.238-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/kmod-4.5.7/aarch64_cortex-a53/mediatek/mt7986/iptables-mod-tproxy_1.8.7-1_aarch64_cortex-a53.ipk
wget -c https://fw.gl-inet.cn/releases/mt798x/packages-4.5/aarch64_cortex-a53/packages/haproxy_2.2.24-1_aarch64_cortex-a53.ipk

# 安装
opkg install *
```

## 3. samba 4.14.12 的使用

### 3.1 配置 samba

![image-20230728152732469](http://typora.hflxhn.com/documents/2023/0728/image-20230728152732469.png)

### 3.2 共享目录

![image-20230728152901244](http://typora.hflxhn.com/documents/2023/0728/image-20230728152901244.png)

### 3.3 samba 用户管理

```bash
# 有时会需用启用 samba 的用户管理功能，以将共享目录限制为特定用户才能访问
# samba 不能直接使用操作系统的用户名和密码，需要为操作系统内已有的用户配置专用的 samba 访问密码
# samba 配置文件默认禁止 root 用户登录，一般也不建议启用 root 用户访问共享目录

# 安装全部支持组件，以支持各类用户管理命令。(与 Linux 系统相同)
opkg update
opkg install shadow

groupadd samba
chgrp samba /share/
chmod 2775 /share/
ls -ld /share
tail -n9 /etc/samba/smb.conf

useradd -g samba -s /sbin/nologin share
smbpasswd -a share

pdbedit -L
```

### 3.4 Linux 客户端安装

#### 3.4.1 客户端软件安装

```bash
yum install cifs-utils -y
```

####  3.4.2 临时挂载 

```bash
mkdir -p /iso
mount -t cifs -ousername=share,password=123 //172.24.0.250/share/iso /iso
df -Th
```

#### 3.4.3 永久挂载

```bash
cat >> /etc/fstab << END
//172.24.0.250/share/iso /iso/ cifs defaults,user=share,password=123 0 0
END
```

## 4. store

```bash
cat >> /etc/opkg/customfeeds.conf << 'END'
src/gz store https://istore.linkease.com/repo/all/store
END

opkg install taskd luci-lib-xterm luci-lib-taskd luci-app-store
```

