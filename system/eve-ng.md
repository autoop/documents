# eve 安装

## 配置清华源

```bash
cp /etc/apt/sources.list /etc/apt/sources.list.bak

cat > /etc/apt/sources.list << END
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
# deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
# deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
# deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse
# deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse

# 预发布软件源，不建议启用
# deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-proposed main restricted universe multiverse
# deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-proposed main restricted universe multiverse
END

apt update
```

## 运行官网安装脚本

```bash
cat > eve-setup.sh << 'END'
#!/bin/sh
#Modify /etc/ssh/sshd_config with: PermitRootLogin yes
sed -i -e "s/.*PermitRootLogin .*/PermitRootLogin yes/" /etc/ssh/sshd_config
sed -i -e 's/.*DefaultTimeoutStopSec=.*/DefaultTimeoutStopSec=5s/' /etc/systemd/system.conf
systemctl restart ssh
apt-get update
apt-get -y install software-properties-common
wget -O - http://www.eve-ng.net/focal/eczema@ecze.com.gpg.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64]  http://www.eve-ng.net/focal focal main"
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y install eve-ng
/etc/init.d/mysql restart
DEBIAN_FRONTEND=noninteractive apt-get -y install eve-ng
echo root:eve | chpasswd 
ROOTLV=$(mount | grep ' / ' | awk '{print $1}')
echo $ROOTLV
lvextend -l +100%FREE $ROOTLV
echo Resizing ROOT FS
resize2fs $ROOTLV
reboot
END

bash eve-setup.sh
```

```bash
# 脚本介绍
# 使root用户可以ssh登陆
#Modify /etc/ssh/sshd_config with: PermitRootLogin yes
sed -i -e "s/.*PermitRootLogin .*/PermitRootLogin yes/" /etc/ssh/sshd_config
sed -i -e 's/.*DefaultTimeoutStopSec=.*/DefaultTimeoutStopSec=5s/' /etc/systemd/system.conf
systemctl restart ssh

# 前期准备
apt-get -y install software-properties-common

# 添加 eve-ng 官方源
wget -O - http://www.eve-ng.net/focal/eczema@ecze.com.gpg.key | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64]  http://www.eve-ng.net/focal focal main"
apt-get update

# 安装 eve-ng
DEBIAN_FRONTEND=noninteractive apt-get -y install eve-ng
/etc/init.d/mysql restart
DEBIAN_FRONTEND=noninteractive apt-get -y install eve-ng

# 设置 root 密码
echo root:eve | chpasswd

# 磁盘扩容
ROOTLV=$(mount | grep ' / ' | awk '{print $1}')
echo $ROOTLV
lvextend -l +100%FREE $ROOTLV
echo Resizing ROOT FS
resize2fs $ROOTLV

# 重启
reboot
```

## 将网卡服务改为 network-manage

```bash
# 安装network-manage
apt install network-manager -y

# 让 network-manage 接管网卡
echo '
[main]
plugins=ifupdown,keyfile,ofono
dns=dnsmasq

[ifupdown]
managed=true

[keyfile]
unmanaged-devices=*,except:type:wifi,except:type:gsm,except:type:cdma,except:type:wwan,except:type:ethernet,except:type:vlan,except:type:bridge
' > /etc/NetworkManager/NetworkManager.conf

# 清空旧的网卡管理配置文件
cp -r /etc/network/interfaces /etc/network/interfaces.bak
echo > /etc/network/interfaces

# 停用旧的网卡服务
systemctl disable networkd-dispatcher.service
systemctl stop networkd-dispatcher.service

# 启用network-manage
systemctl enable NetworkManager
systemctl restart NetworkManager

# 通过nmcli配置ip
nmcli con add con-name pnet0 ifname pnet0 type bridge ipv4.method manual ipv4.add 172.24.0.211/24 ipv4.gateway 172.24.0.250 ipv4.dns 114.114.114.114 ipv6.method dhcp stp no 802-3-ethernet.wake-on-lan magic
nmcli con add con-name eth0 type ethernet ifname eth0 slave-type bridge master pnet0
nmcli con up pnet0
nmcli con up eth0

# 添加网桥
nmcli con add con-name pnet1 type bridge ifname pnet1 bridge.stp no ipv4.addresses 10.0.0.1/24 ipv4.method manual ipv6.method disabled

# 配置iptables转发
snat=`ip a show pnet0 | grep -m 1 inet | awk '{print $2}' | awk -F"/" '{print $1}'`
iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -o eth0 -j SNAT --to $snat

sed -i "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/" /etc/sysctl.conf
echo 1 > /proc/sys/net/ipv4/ip_forward
apt install iptables-persistent -y
iptables-save > /etc/iptables/rules.v4

# 配置dhcp服务器
apt install isc-dhcp-server -y

cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bak
cat > /etc/dhcp/dhcpd.conf << END
subnet 10.0.0.0 netmask 255.255.255.0 {
    range 10.0.0.100 10.0.0.199;
    option domain-name-servers 114.114.114.114;
    option subnet-mask 255.255.255.0;
    option routers 10.0.0.1;
    default-lease-time 600;
    max-lease-time 7200;
}
END
systemctl restart isc-dhcp-server
```

## 访问物理机的 eve 模拟器

```bash
# linux 端
ip route add 192.168.116.0/24 via 10.173.26.5

# win 端
route add 10.0.0.0 mask 255.255.255.0 172.24.0.211
route print -4
```

## 定制 linux 系统到 eve-ng

```bash
# 图标定制
wget -c http://172.24.0.248/web/mirror/ubuntu/20.04/eve-ng/icons.tar.gz
# 或者
wget -c http://typora.hflxhn.com/software/eve-ng/icons.tar.gz
tar xf icons.tar.gz
mv icons/*.png /opt/unetlab/html/images/icons/

# 取消不可用模板
cp -r /opt/unetlab/html/includes/config.php.distribution /opt/unetlab/html/includes/config.php

# 模板定制
# custom_templates.yml
sys_list=( \
	centos-7 centos-stream-8 centos-stream-8 \
	rocky-8 rocky-9 \
	almalinux-8 almalinux-9 \
	ubuntu-server-16.04 ubuntu-server-20.04 \
	win-10 \
	h3cvsr-1k h3cvsr-2k \
)

for val in ${sys_list[@]}; do
    # 创建系统存放目录
    mkdir -p /opt/unetlab/addons/qemu/${val}
done



cat > /opt/unetlab/html/includes/custom_templates.yml << END
---
custom_templates:
  - name: centos
    listname: CentOS
  - name: rocky
    listname: Rocky
  - name: almaLinux
    listname: AlmaLinux
  - name: ubuntu
    listname: Ubuntu
  - name: win
    listname: win
  - name: h3cvsr
    listname: h3cvsr
...
END

# centos.yml
cat > /opt/unetlab/html/templates/intel/centos.yml << END
---
type: qemu
name: centos
cpulimit: 1
icon: centos.png
cpu: 4
ram: 4096
ethernet: 2
eth_format: eth{0}
console: vnc
shutdown: 1
qemu_arch: x86_64
qemu_nic: virtio-net-pci
qemu_options: -machine type=pc,accel=kvm -vga std -usbdevice tablet -boot order=dc -cpu host
...
END

# rocky.yml
cat > /opt/unetlab/html/templates/intel/rocky.yml << END
---
type: qemu
name: rocky
cpulimit: 1
icon: rocky.png
cpu: 4
ram: 4096
ethernet: 2
eth_format: eth{0}
console: vnc
shutdown: 1
qemu_arch: x86_64
qemu_nic: virtio-net-pci
qemu_options: -machine type=pc,accel=kvm -vga std -usbdevice tablet -boot order=dc -cpu host
...
END

# almalinux.yml
cat > /opt/unetlab/html/templates/intel/almalinux.yml << END
---
type: qemu
name: almalinux
cpulimit: 1
icon: almalinux.png
cpu: 4
ram: 4096
ethernet: 2
eth_format: eth{0}
console: vnc
shutdown: 1
qemu_arch: x86_64
qemu_nic: virtio-net-pci
qemu_options: -machine type=pc,accel=kvm -vga std -usbdevice tablet -boot order=dc -cpu host
...
END

# ubuntu.yml
cat > /opt/unetlab/html/templates/intel/ubuntu.yml << END
---
type: qemu
name: ubuntu
cpulimit: 1
icon: ubuntu.png
cpu: 4
ram: 4096
ethernet: 2
eth_format: eth{0}
console: vnc
shutdown: 1
qemu_arch: x86_64
qemu_nic: virtio-net-pci
qemu_options: -machine type=pc,accel=kvm -vga std -usbdevice tablet -boot order=dc -cpu host
...
END

# win.yml
cat > /opt/unetlab/html/templates/intel/win.yml << END
type: qemu
description: Windows
name: Win
cpulimit: 1
icon: win10.png
cpu: 4
ram: 4196
ethernet: 4
console: vnc
shutdown: 1
qemu_arch: x86_64
qemu_version: 4.1.0
qemu_options: -machine type=pc,accel=kvm -cpu host,+pcid,+kvm_pv_unhalt,+kvm_pv_eoi,hv_spinlocks=0x1fff,hv_vapic,hv_time,hv_reset,hv_vpindex,hv_runtime,hv_relaxed,hv_synic,hv_stimer
   -vga std -usbdevice tablet -boot order=cd -drive file=/opt/qemu/share/qemu/virtio-win-drivers.img,index=1,if=floppy,readonly
...
END

# h3cvsr.yml
cat > /opt/unetlab/html/templates/intel/h3cvsr.yml << END
type: qemu
name: h3cvsr
cpulimit: 1
icon: Router.png
cpu: 1
ram: 1024
ethernet: 16
console: vnc
eth_format: GE{0}/0
shutdown: 1
qemu_arch: x86_64
qemu_nic: e1000
qemu_options: -machine type=pc,accel=kvm -vga std -usbdevice tablet -boot order=dc -cpu host
...
END
```

## 定制系统到 eve-ng

### 1. 下载 iso 镜像到 eve-ng

```bash
cd /opt/unetlab/addons/qemu/ubuntu-server-16.04/
wget http://172.24.0.249/web/iso/Ubuntu/ubuntu-16.04.7-server-amd64.iso
mv ubuntu-16.04.7-server-amd64.iso cdrom.iso
```

### 2. 创建一块新硬盘

```bash
/opt/qemu/bin/qemu-img create -f qcow2 virtioa.qcow2 100G
```

### 3. 刷新权限

```bash
/opt/unetlab/wrappers/unl_wrapper -a fixpermissions
```

### 4. 配置 node

```bash
# 1. 试验台中新建节点选定linux-myiso
# 2. 启动后通过VNC等待cdrom.iso加载完毕
# 3. 按照定制化需求配置好node

# 必须打开cpu虚拟化, 否则无法启动vnc.
```

### 5. 删除或重命名镜像

```bash
rm -rf cdrom.iso

# 先关闭node再执行, 此步骤为避免下次仍然从cdrom启动
```

### 6. 提交修改至镜像 (PNET支持界面直接commit)

```bash
cd /opt/unetlab/tmp/0/eaff72ac-f27e-4e68-9cf8-e4d623ebb3f0/1/

# 执行时需关闭所有该镜像 node
/opt/qemu/bin/qemu-img commit virtioa.qcow2
```

### 7. 压缩镜像 (选做)

```bash
virt-sparsify --compress virtioa.qcow2  newvirtioa.qcow2

mv virtioa.qcow2 oldvirtioa.qcow2

mv newvirtioa.qcow2 virtioa.qcow2
```

### 8.镜像测试

```bash
# 开机前需要wipe一次node, 否则可能无法启动.
```

## eve-ng-integration

```bash
# ubuntu 安装 eve-ng-integration
sudo add-apt-repository ppa:smartfinn/eve-ng-integration
sudo apt-get update
sudo apt-get install eve-ng-integration -y
```

