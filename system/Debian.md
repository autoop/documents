# Debian Desktop 美化

## 配置 SSH 服务

```bash
sed -i "s/^[[:space:]]*GSSAPIAuthentication.*/    GSSAPIAuthentication no/" /etc/ssh/ssh_config

cat > ~/.ssh/config << 'END'
Host openwrt
	HostName frp.hflxhn.com
	Port 65501

Host home-data
	HostName 172.24.0.248
	ProxyJump openwrt

Host *
	User root
END
```

## 设置 root 无需密码提权

```bash
su - root

echo 'fanyang ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/fanyang
chown fanyang:fanyang -R /opt
```

## 更换国内源

```bash
sudo cp /etc/apt/sources.list{,.bak}

# Debian 12
sudo bash -c "cat > /etc/apt/sources.list" << 'END'
deb https://mirrors.ustc.edu.cn/debian/ bookworm main non-free non-free-firmware contrib
deb-src https://mirrors.ustc.edu.cn/debian/ bookworm main non-free non-free-firmware contrib
deb https://mirrors.ustc.edu.cn/debian-security/ bookworm-security main
deb-src https://mirrors.ustc.edu.cn/debian-security/ bookworm-security main
deb https://mirrors.ustc.edu.cn/debian/ bookworm-updates main non-free non-free-firmware contrib
deb-src https://mirrors.ustc.edu.cn/debian/ bookworm-updates main non-free non-free-firmware contrib
deb https://mirrors.ustc.edu.cn/debian/ bookworm-backports main non-free non-free-firmware contrib
deb-src https://mirrors.ustc.edu.cn/debian/ bookworm-backports main non-free non-free-firmware contrib
END

sudo apt clean all
sudo apt update
```

## 卸载常用软件

```bash
sudo apt remove -y gnome-2048 gnome-contacts gnome-weather gnome-clocks gnome-maps gnome-chess aisleriot file-roller gnome-characters yelp five-or-more four-in-a-row hitori  im-config gnome-klotski lightsoff gnome-mahjongg gnome-mines gnome-music libreoffice-* gnome-nibbles quadrapassel iagno gnome-robots rhythmbox gnome-sound-recorder gnome-sudoku swell-foop synaptic tali gnome-taquin gnome-tetravex transmission-gtk

sudo apt install -y bash-completion git gnome-tweaks vim remmina flameshot
```

## 系统美化

```bash
git clone https://gitee.com/tbhflxhn/Mojave-gtk-theme.git
sudo apt install libglib2.0-dev -y
cd Mojave-gtk-theme
./install.sh

sudo apt install gnome-shell-extension-dashtodock gnome-shell-extension-appindicator -y

# gnome 启用扩展
gnome-extensions enable dash-to-dock@micxgx.gmail.com
gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
gnome-extensions enable ubuntu-appindicators@ubuntu.com
# 设置标题栏按钮
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'
# 设置 Dock 居中显示
gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
# 设置 Dock 自动隐藏
gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
# 设置 Dock 居左
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position LEFT
# 隐藏 Dock 上的磁盘挂载图标
gsettings set org.gnome.shell.extensions.dash-to-dock show-mounts false
# 多显示器
gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true
# 设置主题
gsettings set org.gnome.desktop.interface gtk-theme 'Mojave-Dark'
# 设置 Top Bar
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.interface clock-show-weekday true

# PS1 设置
PS1='\[\e[34;1m\][\[\e[33;1m\]\u@\[\e[32;1m\]\h \[\e[36;1m\]\w\[\e[34;1m\]] \[\e[34;3m\]\$ \[\e[0m\]'
grep ^PS1 ~/.bashrc &>/dev/null || echo "PS1='${PS1}'" >> ~/.bashrc
grep ^PS1 ~/.bashrc &>/dev/null && sed -i "s/^PS1=.*%/PS1='${PS1}'/g" ~/.bashrc
grep ^PS1 ~/.bashrc
```

## 中文输入

```bash
sudo apt install ibus ibus-pinyin ibus-libpinyin ibus-sunpinyin -y
```

## kvm 虚拟化

```bash
sudo apt install virt-manager libguestfs-tools -y
```

## 常用软件安装

```bash
# sublime
wget -c http://172.24.0.248/web/software/Linux/SublimeText/sublime_text.tar.gz
tar xf sublime_text.tar.gz -C /opt/
sudo bash -c 'cat /opt/sublime_text/sublime_text.desktop > /usr/share/applications/sublime_text.desktop'
sudo bash -c 'cat /usr/share/applications/sublime_text.desktop'
sudo bash -c 'cp -r /opt/sublime_text/Icon/* /usr/share/icons/hicolor/'
sudo bash -c 'rm -rf /usr/share/icons/hicolor/icon-theme.cache'

# typora
wget -c http://172.24.0.248/web/software/Linux/Typora/typora.tar.gz
tar xf typora.tar.gz -C /opt/
sudo bash -c 'cat /opt/typora/typora.desktop > /usr/share/applications/typora.desktop'
cd ~/.config/Typora/themes
git clone git@gitee.com:hflxhn/typora-theme.git
mv typora-theme/* . && rm -rf typora-theme

# balena
wget -c http://172.24.0.248/web/software/Linux/balenaEtcher/balenaEtcher-1.18.11-x64.tar.gz
mkdir /opt/balena
tar xf balenaEtcher-1.18.11-x64.tar.gz -C /opt/balena/

# Chrome
wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb -y

# Dingtalk
wget -c https://dtapp-pub.dingtalk.com/dingtalk-desktop/xc_dingtalk_update/linux_deb/Release/com.alibabainc.dingtalk_7.6.0.4091801_amd64.deb
sudo apt install ./com.alibabainc.dingtalk_7.6.0.4091801_amd64.deb

# TencentMeeting
wget -c https://updatecdn.meeting.qq.com/cos/fb7464ffb18b94a06868265bed984007/TencentMeeting_0300000000_3.19.2.400_x86_64_default.publish.officialwebsite.deb
sudo apt install ./TencentMeeting_0300000000_3.19.2.400_x86_64_default.publish.officialwebsite.deb -y

# Edge
wget -c https://packages.microsoft.com/repos/edge/pool/main/m/microsoft-edge-stable/microsoft-edge-stable_129.0.2792.52-1_amd64.deb
sudo apt install ./microsoft-edge-stable_129.0.2792.52-1_amd64.deb -y

# WPS
wget -c https://wdl1.pcfg.cache.wpscdn.com/wpsdl/wpsoffice/download/linux/11723/wps-office_11.1.0.11723.XA_amd64.deb
sudo apt install ./wps-office_11.1.0.11723.XA_amd64.deb
sudo vi /usr/bin/wps
LANGUAGE=zh_CN

# ToDesk
wget -c https://dl.todesk.com/linux/todesk-v4.7.2.0-amd64.deb
sudo apt install ./todesk-v4.7.2.0-amd64.deb
```

## vi 配置

```bash
sudo sed -i 's/mouse=a/mouse-=a/' /usr/share/vim/vim90/defaults.vim
```

## flatpak 安装

```bash
# 安装 flatpak
sudo apt install flatpak gnome-software-plugin-flatpak -y

# 更换国内源
sudo flatpak remote-add --if-not-exists flathub https://mirror.sjtu.edu.cn/flathub/flathub.flatpakrepo
sudo flatpak remote-modify flathub --url=https://mirror.sjtu.edu.cn/flathub
```
