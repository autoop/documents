# Win 10 系统管理

## PowerShell

```bash
# 检查防火墙规则
Get-NetFirewallRule | Where-Object { $_.LocalPort -eq 445 }

# 添加防火墙规则
New-NetFirewallRule -DisplayName "Allow SMB 445" -Direction Inbound -Protocol TCP -LocalPort 445 -Action Allow

# 检查端口是否打开
Test-NetConnection -ComputerName localhost -Port 445
```

## 破解 Windows 密码

### ESXi 操作

```bash
# 编辑配置
```

![image-20231106100418578](http://typora.hflxhn.com/documents/2023/1106/image-20231106100418578.png)

```bash
# 挂载光盘
```

![image-20231106100522652](http://typora.hflxhn.com/documents/2023/1106/image-20231106100522652.png)

```bash
# 找到虚拟机选项,引导选择BIOS,并设置下次强制进入BIOS设置,然后保存
```

![image-20231106100621332](http://typora.hflxhn.com/documents/2023/1106/image-20231106100621332.png)

### Windows虚机操作

```bash
# 启动虚机,找到boot选项,将CD-ROM Drive设置为高优先引导后保存退出.
```

![image-20231106100812155](http://typora.hflxhn.com/documents/2023/1106/image-20231106100812155.png)

```bash
# 重启后进入安装界面,点击下一步
```

![image-20231106100851523](http://typora.hflxhn.com/documents/2023/1106/image-20231106100851523.png)

```bash
# 找到修复计算机(位于页面左下角)
```

![image-20231106100921464](http://typora.hflxhn.com/documents/2023/1106/image-20231106100921464.png)

```bash
# 点击疑难解答
```

![image-20231106100946147](http://typora.hflxhn.com/documents/2023/1106/image-20231106100946147.png)

```bash
# 点击命令提示符
```

![image-20231106101008803](http://typora.hflxhn.com/documents/2023/1106/image-20231106101008803.png)

```bash
# 输入以下命令后关闭cmd界面,然后关闭电脑
c:
# 切换到c盘(系统盘)
cd Windows\System32
copy osk.exe c:\
# 备份文件
copy cmd.exe osk.exe
# 复制cmd.exe为osk.exe
```

![image-20231106101052489](http://typora.hflxhn.com/documents/2023/1106/image-20231106101052489.png)

### ESXi 操作

```bash
# 修改为efi后启动虚机
```

![image-20231106101138508](http://typora.hflxhn.com/documents/2023/1106/image-20231106101138508.png)

```bash
# 点击右下角三个图标中间的轻松使用
# 点击屏幕键盘(此时屏幕键盘已被替换为cmd.exe)
```

![image-20231106101323347](http://typora.hflxhn.com/documents/2023/1106/image-20231106101323347.png)

```bash
# 跳出cmd窗口,输入以下命令修改用户密码
net user
# 查看当前有哪些用户
net user li 2WsxzaQ1`
# 修改用户li的密码为2WsxzaQ1`
```

![image-20231106101546470](http://typora.hflxhn.com/documents/2023/1106/image-20231106101546470.png)

```bash
# 输入密码 2WsxzaQ1`
```

![image-20231106101626116](http://typora.hflxhn.com/documents/2023/1106/image-20231106101626116.png)