# rhel system manage

## 1.  yum repo

### 1.1 开启 yum 缓存

```bash
cat > /etc/yum.conf << 'END'
[main]
gpgcheck=1
installonly_limit=3
clean_requirements_on_remove=False
best=True
skip_if_unavailable=False
keepcache=1
END

find /var/cache/dnf/ -name *.rpm
```

### 1.2 rhel 9 配置 repo 仓库

```bash
# backup old repo
mkdir -p /etc/yum.repos.d/bak
mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak

# AlmaLinux 9
os_name=AlmaLinux
baseurl_BaseOS=https://mirrors.aliyun.com/almalinux/9.2/BaseOS/x86_64/os/
baseurl_AppStream=https://mirrors.aliyun.com/almalinux/9.2/AppStream/x86_64/os/

# CentOS Stream 9
os_name=CentOS-Stream
baseurl_BaseOS=https://mirrors.aliyun.com/centos-stream/9-stream/BaseOS/x86_64/os/
baseurl_AppStream=https://mirrors.aliyun.com/centos-stream/9-stream/AppStream/x86_64/os/

# RockyLinux 9
os_name=RockyLinux
baseurl_BaseOS=https://mirrors.aliyun.com/rockylinux/9.2/BaseOS/x86_64/os/
baseurl_AppStream=https://mirrors.aliyun.com/rockylinux/9.2/AppStream/x86_64/os/

# repo
cat > /etc/yum.repos.d/${os_name}.repo << END
[BaseOS]
baseurl = ${baseurl_BaseOS}
enabled = 1
gpgcheck = 0
name = BaseOS

[AppStream]
baseurl = ${baseurl_AppStream}
enabled = 1
gpgcheck = 0
name = AppStream
END

# update cache
yum makecache
```

### 1.3 rhel 8 配置 repo 仓库

```bash
# backup old repo
mkdir -p /etc/yum.repos.d/bak
mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak

# AlmaLinux 8
os_name=AlmaLinux
baseurl_BaseOS=https://mirrors.aliyun.com/almalinux/8.8/BaseOS/x86_64/os/
baseurl_AppStream=https://mirrors.aliyun.com/almalinux/8.8/AppStream/x86_64/os/

# RockyLinux 8
os_name=RockyLinux
baseurl_BaseOS=https://mirrors.aliyun.com/rockylinux/8.8/BaseOS/x86_64/os/
baseurl_AppStream=https://mirrors.aliyun.com/rockylinux/8.8/AppStream/x86_64/os/

# repo
cat > /etc/yum.repos.d/${os_name}.repo << END
[BaseOS]
baseurl = ${baseurl_BaseOS}
enabled = 1
gpgcheck = 0
name = BaseOS

[AppStream]
baseurl = ${baseurl_AppStream}
enabled = 1
gpgcheck = 0
name = AppStream
END

# update cache
yum makecache
```

### 1.4 CentOS 7 配置 repo 仓库

```bash
rm -rf /etc/yum.repos.d/*

# 163
cat > /etc/yum.repos.d/os.repo << 'END'
[os]
baseurl = https://mirrors.163.com/centos/7/os/x86_64/
enabled = 1
gpgcheck = 0
name = os
END

cat > /etc/yum.repos.d/updates.repo << 'END'
[updates]
baseurl = https://mirrors.163.com/centos/7/updates/x86_64/
enabled = 1
gpgcheck = 0
name = updates
END

cat > /etc/yum.repos.d/extras.repo << 'END'
[extras]
baseurl = https://mirrors.163.com/centos/7/extras/x86_64/
enabled = 1
gpgcheck = 0
name = extras
END

cat > /etc/yum.repos.d/virt.repo << 'END'
[virt]
baseurl = https://mirrors.163.com/centos/7/virt/x86_64/kvm-common/
enabled = 1
gpgcheck = 0
name = virt
END

cat > /etc/yum.repos.d/cloud.repo << 'END'
[cloud]
baseurl = https://mirrors.163.com/centos/7/cloud/x86_64/openstack-rocky/
enabled = 1
gpgcheck = 0
name = cloud
END

# ali
cat > /etc/yum.repos.d/epel.repo << 'END'
[epel]
baseurl = https://mirrors.aliyun.com/epel/7/x86_64
enabled = 1
gpgcheck = 0
name = epel
END

yum makecache
```

## 2. 安装常用软件

```bash
# 服务器
yum install gcc gcc-c++ make cmake \
git wget \
tar zip unzip bzip2 \
cairo pango gtk3 \
bash-completion \
tmux tree \
chrony

# 桌面环境
yum install -y gcc gcc-c++ make cmake \
gnome-tweak-tool gnome-shell-extension-dash-to-dock gnome-extensions-app \
git wget tmux wireshark remmina \
tar zip unzip bzip2 \
langpacks-zh_CN
```

## 3. 配置 ssh 服务

```bash
# 配置 ssh 禁止密码登陆
sed -i 's/^PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config

# 重启 ssh 服务
systemctl restart sshd.service
```

## 4. 设置普通用户提权无需密码

```bash
username=$(whoami)
sudo bash -c "echo '${username} ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/${username}"
sudo chown ${username}:${username} -R /opt
```

## 5. 设置 PS1 变量

```bash
PS1='\[\e[34;1m\][\[\e[33;1m\]\u@\[\e[32;1m\]\h \[\e[36;1m\]\w\[\e[34;1m\]] \[\e[34;3m\]\$ \[\e[0m\]'
grep ^PS1 ~/.bashrc &>/dev/null || echo "PS1='${PS1}'" >> ~/.bashrc
grep ^PS1 ~/.bashrc &>/dev/null && sed -i "s/^PS1=.*%/PS1='${PS1}'/g" ~/.bashrc
grep ^PS1 ~/.bashrc
```

## 6. 配置 root 使用声卡设备

```bash
sed -i 's/^ConditionUser=/#&/' /usr/lib/systemd/user/pipewire-pulse.service
systemctl --user daemon-reload
systemctl --user enable pipewire-pulse.service
systemctl --user restart pipewire-pulse.service
```

## 7. 设置主题

```bash
git clone https://gitee.com/tbhflxhn/Mojave-gtk-theme.git
yum install glib2-devel -y
cd Mojave-gtk-theme
./install.sh

# gnome 启用扩展
gnome-extensions enable dash-to-dock@gnome-shell-extensions.gcampax.github.com
gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
# 设置标题栏按钮
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'
# 设置 Dock 居中显示
gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
# 设置 Dock 自动隐藏
gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
# 设置 Dock 居左
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position LEFT
# 隐藏 Dock 上的磁盘挂载图标
gsettings set org.gnome.shell.extensions.dash-to-dock show-mounts false
# 多显示器
gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true
# 设置主题
gsettings set org.gnome.desktop.interface gtk-theme 'Mojave-Dark'
# 设置 Top Bar
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.interface clock-show-weekday true
```

## 8. google root open

```bash
sed -i '/^Exec=/s/.*/& --no-sandbox/' /usr/share/applications/google-chrome.desktop
chattr +i /usr/share/applications/google-chrome.desktop
```

## 9. 内网软件

```bash
# sublime
wget -c http://172.24.0.248/web/software/Linux/SublimeText/sublime_text.tar.gz
tar xf sublime_text.tar.gz -C /opt/
cat /opt/sublime_text/sublime_text.desktop > /usr/share/applications/sublime_text.desktop
cat /usr/share/applications/sublime_text.desktop
cp -r /opt/sublime_text/Icon/* /usr/share/icons/hicolor/
rm -rf /usr/share/icons/hicolor/icon-theme.cache

# typora
wget -c http://172.24.0.248/web/software/Linux/Typora/typora.tar.gz
tar xf typora.tar.gz -C /opt/
cat /opt/typora/typora.desktop > /usr/share/applications/typora.desktop

# balena
wget -c http://172.24.0.248/web/software/Linux/balenaEtcher/balenaEtcher-1.18.11-x64.tar.gz
mkdir /opt/balena
tar xf balenaEtcher-1.18.11-x64.tar.gz -C /opt/balena/

# flameshot
yum install http://172.24.0.249/mirror/centos-stream/8/extend/soft/flameshot-0.6.0-4.el8.x86_64.rpm -y
```

## 10. 功能键F1-F12映射修复

```bash
# 在Linux上，Keychon K2不会将任何F1-F12功能键映射为实际的F键，而是默认将它们视为多媒体键
# Fn+F1-12的组合也不会起作用
echo 0 | sudo tee /sys/module/hid_apple/parameters/fnmode
echo "options hid_apple fnmode=0" | sudo tee -a /etc/modprobe.d/hid_apple.conf
```

## 11. 同步两个目录的文件

```bash
rsync -av --delete /path/source/ /path/destination/

# -av 选项表示以递归方式同步，并保留文件属性
# --delete 表示删除目标目录中不存在于源目录中的文件
```

## 12. 卸载旧的内核

```bash
# 查看当前使用的内核版本
uname -a

# 查询当前已安装的所有内核
rpm -qa | grep kernel

# 删除未使用的内核
dnf remove --oldinstallonly --setopt installonly_limit=2 kernel
```

## 13. 分离 ssh 成功和失败的日志

```bash
# 修改配置文件 /etc/rsyslog.conf
# The authpriv file has restricted access.
authpriv.*                                             /var/log/success-secure
authpriv.err                                           /var/log/secure

# 重启服务
systemctl restart rsyslog.service
```

## 14. 挂载磁盘

```bash
pvcreate /dev/sdb
vgextend almalinux /dev/sdb
lvcreate -n ops -L 99G almalinux
mkfs.xfs /dev/mapper/almalinux-ops
grep almalinux-ops /etc/fstab || echo "/dev/mapper/almalinux-ops /opt xfs defaults 0 0" >> /etc/fstab
mount -a
```

## 15. rhel 系统播放 h264 视频

### 15.1 问题描述

```bash
h.264 (high profile) decoder is required to play the file, but is not installed
```

![image-20230613221640936](http://typora.hflxhn.com/documents/2023/0613/image-20230613221640936.png)

### 15.2 错误解决

```bash
# 安装 GStreamer Multimedia Codecs（libav）及其插件

# 配置本地软件仓库
cat > /etc/yum.repos.d/rpmfusion-free-updates.repo << END
[rpmfusion-free-updates]
name = rpmfusion-free-updates
enabled = 1
gpgcheck = 0
baseurl = http://172.24.0.249/mirror/almalinux/9/rpmfusion-free-updates/
END

# 或者添加 RPM Fusion 软件源
dnf install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-9.noarch.rpm
dnf install https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-9.noarch.rpm

dnf install gstreamer1-libav -y
```

![image-20230613222258220](http://typora.hflxhn.com/documents/2023/0613/image-20230613222258220.png)

## 16. rhel 运行 dingtalk

```bash
# 下载打包代码
git clone https://gitee.com/tbhflxhn/dingtalk-for-fedora.git

# 修改 Dingtalk 版本号
sed -i 's/1.6.0.230113/7.6.0.40718/' SPECS/dingtalk-bin.spec
grep 7.6.0.40718 SPECS/dingtalk-bin.spec

# 安装依赖包
yum install epel-release -y
yum install rpm-build dpkg rpmdevtools -y

# rpmbuild 打包
dnf builddep SPECS/dingtalk-bin.spec
spectool -gR SPECS/dingtalk-bin.spec
cp SOURCES/* ~/rpmbuild/SOURCES
rpmbuild -bb --define="_binary_payload w9T12.xzdio" SPECS/dingtalk-bin.spec

# rpm 包位置
ls ~/rpmbuild/SRPMS
```
