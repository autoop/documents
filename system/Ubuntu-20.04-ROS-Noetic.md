# Ubuntu Server 20.04 安装 ROS-Noetic

## set root ssh login

```bash
cp -r /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
sed -i '/^#PermitRootLogin/a PermitRootLogin yes' /etc/ssh/sshd_config
systemctl restart sshd
```

## apt repo

```bash
cp -r /etc/apt/sources.list{,.bak}

# ali
cat > /etc/apt/sources.list << 'END'
deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
END

# intranet
cat > /etc/apt/sources.list << 'END'
deb [trusted=yes] http://10.173.28.219/web/mirror/Ubuntu/20.04/ main/
END

# update repo
apt clean all
apt update
```

## network-manage

```bash
apt install network-manager -y

echo '
[main]
plugins=ifupdown,keyfile,ofono
dns=dnsmasq

[ifupdown]
managed=true

[keyfile]
unmanaged-devices=*,except:type:wifi,except:type:gsm,except:type:cdma,except:type:wwan,except:type:ethernet,except:type:vlan,except:type:bridge
' > /etc/NetworkManager/NetworkManager.conf

cp -r /etc/network/interfaces /etc/network/interfaces.bak
echo > /etc/network/interfaces

systemctl disable networkd-dispatcher.service
systemctl stop networkd-dispatcher.service

systemctl enable NetworkManager --now
```

## sys init

```bash
apt install -y zip lrzsz gcc g++ make zlib1g-dev libnss3-dev
apt install -y bluetooth lm-sensors nmon iftop iotop monit
```

## ros-noetic

```bash
# sh -c 'echo "deb http://mirrors.tuna.tsinghua.edu.cn/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
# apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
# apt update
apt install -y ros-noetic-desktop
systemctl set-default multi-user.target

apt install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential

# edit intranet host
git_url='http:\/\/10.173.28.219\/web\/mirror\/Ubuntu\/20.04'
sed -i "s/DEFAULT_SOURCES_LIST_URL = .*/DEFAULT_SOURCES_LIST_URL = '${git_url}\/git\/rosdistro\/rosdep\/sources.list.d\/20-default.list'/" /usr/lib/python3/dist-packages/rosdep2/sources_list.py
sed -i "s/DEFAULT_INDEX_URL = .*/DEFAULT_INDEX_URL = '${git_url}\/git\/rosdistro\/index-v4.yaml'/" /usr/lib/python3/dist-packages/rosdistro/__init__.py
sed -i "s/REP3_TARGETS_URL = .*/REP3_TARGETS_URL = '${git_url}\/git\/rosdistro\/releases\/targets.yaml'/" /usr/lib/python3/dist-packages/rosdep2/rep3.py

mkdir -p /etc/ros/rosdep/sources.list.d/
cat > /etc/ros/rosdep/sources.list.d/20-default.list << 'END'
# os-specific listings first
yaml http://10.173.28.219/web/mirror/Ubuntu/20.04/git/rosdistro/rosdep/osx-homebrew.yaml osx

# generic
yaml http://10.173.28.219/web/mirror/Ubuntu/20.04/git/rosdistro/rosdep/base.yaml
yaml http://10.173.28.219/web/mirror/Ubuntu/20.04/git/rosdistro/rosdep/python.yaml
yaml http://10.173.28.219/web/mirror/Ubuntu/20.04/git/rosdistro/rosdep/ruby.yaml
gbpdistro http://10.173.28.219/web/mirror/Ubuntu/20.04/git/rosdistro/releases/fuerte.yaml fuerte

# newer distributions (Groovy, Hydro, ...) must not be listed anymore, they are being fetched from the rosdistro index.yaml instead
END

rosdep update
```

## alsa

```bash
apt install -y libsndfile1-dev libespeak-dev libncurses5-dev libmp3lame-dev pulseaudio alsa-utils
cat >> ~/.profile << END
pulseaudio --start --log-target=syslog
END
```