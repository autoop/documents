# ubuntu-server install

## Ubuntu server 20.04 (kvm)

### 创建虚拟机

####  1. 创建一个新的虚拟机

![image-20230625102316786](http://typora.hflxhn.com/documents/2023/0625/image-20230625102316786.png)

#### 2. 选择 iso 文件

![image-20230625102400756](http://typora.hflxhn.com/documents/2023/0625/image-20230625102400756.png)

#### 3. 选择内存和cpu核数

![image-20230625102438206](http://typora.hflxhn.com/documents/2023/0625/image-20230625102438206.png)

#### 4. 创建磁盘

![image-20230625102521145](http://typora.hflxhn.com/documents/2023/0625/image-20230625102521145.png)

#### 5. 完成创建

![image-20230625102605671](http://typora.hflxhn.com/documents/2023/0625/image-20230625102605671.png)

### 安装虚拟机

#### 1. 选择语言

![image-20230625104001643](http://typora.hflxhn.com/documents/2023/0625/image-20230625104001643.png)

#### 2. 继续不更新

![image-20230625104233154](http://typora.hflxhn.com/documents/2023/0625/image-20230625104233154.png)

#### 3. 选择键盘

![image-20230625104021005](http://typora.hflxhn.com/documents/2023/0625/image-20230625104021005.png)

#### 4. 配置网络

![image-20230625104416164](http://typora.hflxhn.com/documents/2023/0625/image-20230625104416164.png)

#### 5. 不设置代理

![image-20230625104445715](http://typora.hflxhn.com/documents/2023/0625/image-20230625104445715.png)

#### 6. 配置软件源

![image-20230625104505144](http://typora.hflxhn.com/documents/2023/0625/image-20230625104505144.png)

#### 7. 配置磁盘

![image-20230625104525616](http://typora.hflxhn.com/documents/2023/0625/image-20230625104525616.png)

![image-20230625104713235](http://typora.hflxhn.com/documents/2023/0625/image-20230625104713235.png)

![image-20230625104736427](http://typora.hflxhn.com/documents/2023/0625/image-20230625104736427.png)

#### 8. 配置系统信息

![image-20230625104755656](http://typora.hflxhn.com/documents/2023/0625/image-20230625104755656.png)

#### 9. 安装 openssh (空格选中)

![image-20230625104810550](http://typora.hflxhn.com/documents/2023/0625/image-20230625104810550.png)

#### 10. 安装完成

![image-20230625111528148](http://typora.hflxhn.com/documents/2023/0625/image-20230625111528148.png)

### 系统管理

#### 1. 登陆系统

![image-20230625112258484](http://typora.hflxhn.com/documents/2023/0625/image-20230625112258484.png)

#### 2. 允许 root 登陆

```shell
sudo sed -i 's/#PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config
sudo systemctl restart sshd.service
```

![image-20230625112611509](http://typora.hflxhn.com/documents/2023/0625/image-20230625112611509.png)

#### 3. ubuntu server 20.04 启动慢

```bash
# 问题描述:

A start job is running for wait for network to be Configured
比较少见的情况，碰到的话，一般是因为不具备DHCP自动分配IP造成设备尝试获取IP等待过久。

# 解决方法
grep '^TimeoutStartSec=2sec' /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service || sed -i '/^RemainAfterExit=.*/a TimeoutStartSec=2sec' /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service
```

