# ubuntu desktop 美化

## 安装 openssh-server

```bash
sudo apt install openssh-server -y
sed -i "s/^[[:space:]]*GSSAPIAuthentication.*/    GSSAPIAuthentication no/" /etc/ssh/ssh_config
```

## 设置 root 无需密码提权

```bash
sudo bash -c "echo '$(whoami) ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/$(whoami)"
sudo chown $(whoami):$(whoami) -R /opt
```

## 更换国内源

```bash
sudo cp /etc/apt/sources.list{,.bak}

# ubuntu 24.04
sudo bash -c "cat > /etc/apt/sources.list" << 'END'
deb https://mirrors.aliyun.com/ubuntu-ports/ noble main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu-ports/ noble main restricted universe multiverse

deb https://mirrors.aliyun.com/ubuntu-ports/ noble-security main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu-ports/ noble-security main restricted universe multiverse

deb https://mirrors.aliyun.com/ubuntu-ports/ noble-updates main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu-ports/ noble-updates main restricted universe multiverse

# deb https://mirrors.aliyun.com/ubuntu-ports/ noble-proposed main restricted universe multiverse
# deb-src https://mirrors.aliyun.com/ubuntu-ports/ noble-proposed main restricted universe multiverse

deb https://mirrors.aliyun.com/ubuntu-ports/ noble-backports main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu-ports/ noble-backports main restricted universe multiverse
END

# ubuntu 22.04
sudo bash -c "cat > /etc/apt/sources.list" << 'END'
deb https://mirrors.aliyun.com/ubuntu/ jammy main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu/ jammy main restricted universe multiverse

deb https://mirrors.aliyun.com/ubuntu/ jammy-security main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu/ jammy-security main restricted universe multiverse

deb https://mirrors.aliyun.com/ubuntu/ jammy-updates main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu/ jammy-updates main restricted universe multiverse

# deb https://mirrors.aliyun.com/ubuntu/ jammy-proposed main restricted universe multiverse
# deb-src https://mirrors.aliyun.com/ubuntu/ jammy-proposed main restricted universe multiverse

deb https://mirrors.aliyun.com/ubuntu/ jammy-backports main restricted universe multiverse
deb-src https://mirrors.aliyun.com/ubuntu/ jammy-backports main restricted universe multiverse
END

sudo apt clean all
sudo apt update
```

## gnome 设置

```bash
# 设置标题栏按钮
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'
# 设置 Dock 居中显示
gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
# 设置 Dock 自动隐藏
gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
# 设置 Dock 居左
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position LEFT
# 隐藏Ubuntu Dock上的磁盘挂载图标
gsettings set org.gnome.shell.extensions.dash-to-dock show-mounts false
# 多显示器
gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true
```

## 卸载无用的软件包

```bash
sudo apt remove fcitx5* snapd -y
```

## 安装常用软件

```bash
sudo apt install git gnome-tweaks vim remmina flameshot gnome-shell-extensions -y
```

## 主题美化

```bash
sudo apt install libglib2.0-dev -y
git clone https://gitee.com/tbhflxhn/Mojave-gtk-theme.git
cd Mojave-gtk-theme/
bash install.sh
```

## 中文输入

```bash
sudo apt install ibus ibus-pinyin ibus-libpinyin ibus-sunpinyin -y
```

## kvm 虚拟化

```bash
sudo apt install virt-manager libguestfs-tools -y
```

## 禁用更新

```bash
sudo sed -i 's/^Prompt=.*/Prompt=never/' /etc/update-manager/release-upgrades
sudo bash -c 'cat > /etc/apt/apt.conf.d/10periodic' << 'END'
APT::Periodic::Update-Package-Lists "14";
APT::Periodic::Download-Upgradeable-Packages "14";
APT::Periodic::AutocleanInterval "0";
APT::Periodic::Unattended-Upgrade "0";
END
```

## 常用软件安装

```bash
# sublime
wget -c http://172.24.0.248/web/software/Linux/SublimeText/sublime_text.tar.gz
tar xf sublime_text.tar.gz -C /opt/
sudo bash -c 'cat /opt/sublime_text/sublime_text.desktop > /usr/share/applications/sublime_text.desktop'
sudo bash -c 'cat /usr/share/applications/sublime_text.desktop'
sudo bash -c 'cp -r /opt/sublime_text/Icon/* /usr/share/icons/hicolor/'
sudo bash -c 'rm -rf /usr/share/icons/hicolor/icon-theme.cache'

# typora
wget -c http://172.24.0.248/web/software/Linux/Typora/typora.tar.gz
tar xf typora.tar.gz -C /opt/
sudo bash -c 'cat /opt/typora/typora.desktop > /usr/share/applications/typora.desktop'
cd ~/.config/Typora/themes
git clone git@gitee.com:hflxhn/typora-theme.git
mv typora-theme/* . && rm -rf typora-theme

# balena
wget -c http://172.24.0.248/web/software/Linux/balenaEtcher/balenaEtcher-1.18.11-x64.tar.gz
mkdir /opt/balena
tar xf balenaEtcher-1.18.11-x64.tar.gz -C /opt/balena/
```

## flatpak 安装

```bash
# 卸载 snap 安装的软件
sudo snap remove snap-store firefox snapd-desktop-integration gtk-common-themes

# 安装 flatpak
sudo apt install flatpak gnome-software-plugin-flatpak -y

# 更换国内源
flatpak remote-add --if-not-exists flathub https://mirror.sjtu.edu.cn/flathub/flathub.flatpakrepo
flatpak remote-modify flathub --url=https://mirror.sjtu.edu.cn/flathub

# 安装 obs
flatpak install flathub com.obsproject.Studio -y

# 百度网盘
flatpak install flathub com.baidu.NetDisk -y

# firefox
flatpak install flathub org.mozilla.firefox -y

# qqmusic
flatpak install flathub com.qq.QQmusic -y

# wps
flatpak install flathub com.wps.Office -y

# 迅雷
flatpak install flathub com.xunlei.Thunder -y

# putty
flatpak install flathub uk.org.greenend.chiark.sgtatham.putty -y

# 微信
flatpak install flathub com.tencent.WeChat -y

# qq
flatpak install flathub com.qq.QQ -y

# vnc
flatpak install flathub org.videolan.VLC -y
```

