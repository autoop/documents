# OpenVPN Server

## 1. OpenVPN 安装

```bash
# 获取 openvpn
wget https://swupdate.openvpn.org/community/releases/openvpn-2.6.6.tar.gz

# 解压并切换到 openvpn 源码目录
tar xf openvpn-2.6.6.tar.gz && cd openvpn-2.6.6

# 安装 openvpn 依赖包
yum install libnl3-devel libcap-ng-devel lz4-devel openssl-devel lzo-devel pam-devel -y

# ubuntu 22.04
sudo apt install gcc make libnl-genl-3-dev libcap-ng-dev pkg-config openssl libssl-dev liblz4-dev liblzo2-dev libpam0g-dev -y

# 生成编译配置文件
./configure --prefix=/opt/openvpn

# 编译并安装
make -j12 && make install
```

## 2. CA 证书签发

### 2.1 安装 EasyRSA

```bash
wget https://github.com/OpenVPN/easy-rsa/releases/download/v3.1.7/EasyRSA-3.1.7.tgz
tar xf EasyRSA-3.1.7.tgz -C /opt/
```

### 2.2 # 创建 CA 证书

```bash
## 为 openvpn 服务器签发 CA 证书
mkdir -p /opt/openvpn/keys/ca && cd /opt/openvpn/keys/ca
cat > /opt/openvpn/keys/ca/vars << 'END'
set_var EASYRSA_ALGO        "ec"
set_var EASYRSA_DIGEST      "sha512"
set_var EASYRSA_CERT_EXPIRE 3650
END

# 初始化 PKI
/opt/EasyRSA-3.1.7/easyrsa init-pki

# 创建 CA
echo "Easy-RSA CA" | /opt/EasyRSA-3.1.7/easyrsa build-ca nopass
openssl x509 -in /opt/openvpn/keys/ca/pki/ca.crt -text
```

### 2.3 为 openvpn server 签发证书

```bash
mkdir -p /opt/openvpn/keys/openvpn-server && cd /opt/openvpn/keys/openvpn-server
cat > /opt/openvpn/keys/openvpn-server/vars << 'END'
set_var EASYRSA_ALGO        "ec"
set_var EASYRSA_DIGEST      "sha512"
END

# 初始化 PKI
/opt/EasyRSA-3.1.7/easyrsa init-pki

# 生成 openvpn server 密钥对
echo "openvpn-server" | /opt/EasyRSA-3.1.7/easyrsa gen-req openvpn-server nopass

# 为 openvpn server 签发证书
cd /opt/openvpn/keys/ca/

# 导入 openvpn 密钥
/opt/EasyRSA-3.1.7/easyrsa import-req /opt/openvpn/keys/openvpn-server/pki/reqs/openvpn-server.req openvpn-server

# 开始为 openvpn 签发证书
echo yes | /opt/EasyRSA-3.1.7/easyrsa sign-req server openvpn-server
openssl x509 -in /opt/openvpn/keys/ca/pki/issued/openvpn-server.crt -text

# 为 openvpn 服务器安装签发的证书
cd /opt/openvpn/keys/openvpn-server/
cp /opt/openvpn/keys/ca/pki/issued/openvpn-server.crt .
cp /opt/openvpn/keys/ca/pki/ca.crt .

# 生成 dh (Diffie-Hellman) 参数文件
/opt/EasyRSA-3.1.7/easyrsa gen-dh

# 创建 tls 验证
/opt/openvpn/sbin/openvpn --genkey secret /opt/openvpn/keys/openvpn-server/ta.key
```

## 3. 配置 openvpn server

```bash
# 创建日志目录
mkdir -p /opt/openvpn/logs

# 配置 OpenVPN 服务器端
mkdir -p /opt/openvpn/conf/openvpn-server
cat > /opt/openvpn/conf/openvpn-server/openvpn-server.conf << 'END'
port 1194
proto udp
dev tun

server 10.100.0.0 255.255.255.0

ca keys/openvpn-server/ca.crt
cert keys/openvpn-server/openvpn-server.crt
key keys/openvpn-server/pki/private/openvpn-server.key
dh keys/openvpn-server/pki/dh.pem

cipher AES-128-GCM
tls-server
tls-version-min 1.2
tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256
tls-crypt keys/openvpn-server/ta.key

user nobody
group nobody

persist-key
persist-tun
keepalive 10 60

verb 3
status logs/openvpn-status.log
log logs/openvpn.log

topology subnet
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 114.114.114.114"
END
```

## 4. 启动 openvpn server

```bash
# 手动启动 openvpn
/opt/openvpn/sbin/openvpn --config /opt/openvpn/conf/openvpn-server/openvpn-server.conf

# 配置 systemd 启动
cat > /etc/systemd/system/openvpn-server.service << 'END'
[Unit]
Description=OpenVPN Robust And Highly Flexible Tunneling Application On
After=network.target

[Service]
Type=simple
WorkingDirectory=/opt/openvpn
ExecStart=/opt/openvpn/sbin/openvpn --config /opt/openvpn/conf/openvpn-server/openvpn-server.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
END

# 启动 openvpn
systemctl enable openvpn-server.service --now
```

## 5. 开启路由转发功能

```bash
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
sysctl -w net.ipv4.ip_forward=1
cat /proc/sys/net/ipv4/ip_forward
```

## 6. 为 client 签发证书 - 脚本

```bash
#!/bin/bash

tag=${1}

function check() {
    if [[ $? == 0 ]]; then
        echo -e "\033[32m${1} --------> success\033[0m";
    else
        echo -e "\033[31m${1} --------> fail\033[0m"; exit 1;
    fi
}

# 删除重名的证书
pem=$(awk "/CN=${tag}/ {print \$3}" /opt/openvpn/keys/ca/pki/index.txt)
sed -i "/${tag}/d" /opt/openvpn/keys/ca/pki/{index.txt,index.txt.old}
rm -rf /opt/openvpn/keys/${tag}
rm -rf /opt/openvpn/keys/ca/pki/reqs/${tag}.req /opt/openvpn/keys/ca/pki/issued/${tag}.crt
rm -rf /opt/openvpn/keys/ca/pki/certs_by_serial/${pem}.pem

mkdir -p /opt/openvpn/keys/${tag} && cd /opt/openvpn/keys/${tag}
cat > /opt/openvpn/keys/${tag}/vars << 'END'
set_var EASYRSA_ALGO        "ec"
set_var EASYRSA_DIGEST      "sha512"
END

# 1. 初始化 PKI
/opt/EasyRSA-3.1.7/easyrsa init-pki &>/dev/null
check "/opt/EasyRSA-3.1.7/easyrsa init-pki"

# 2. 生成密钥对
echo "${tag}" | /opt/EasyRSA-3.1.7/easyrsa gen-req ${tag} nopass &>/dev/null
check "echo ${tag} | /opt/EasyRSA-3.1.7/easyrsa gen-req ${tag} nopass"

# 3. 签发证书
#   3.1 导入密钥
cd /opt/openvpn/keys/ca/
/opt/EasyRSA-3.1.7/easyrsa import-req /opt/openvpn/keys/${tag}/pki/reqs/${tag}.req ${tag} &>/dev/null
check "/opt/EasyRSA-3.1.7/easyrsa import-req /opt/openvpn/keys/${tag}/pki/reqs/${tag}.req ${tag}"

#   3.2 # 开始签发证书
echo yes | /opt/EasyRSA-3.1.7/easyrsa sign-req client ${tag} &>/dev/null
check "echo yes | /opt/EasyRSA-3.1.7/easyrsa sign-req client ${tag}"
#openssl x509 -in /opt/openvpn/keys/ca/pki/issued/${tag}.crt -text

# 安装签发的证书
cd /opt/openvpn/keys/${tag}/
cp /opt/openvpn/keys/ca/pki/issued/${tag}.crt .
cp /opt/openvpn/keys/ca/pki/ca.crt .

# 生成 ovpn 配置文件
cat > /opt/openvpn/keys/${tag}/${tag}.ovpn << END
client
proto udp
remote openvpn.hflxhn.com 1194
dev tun
remote-cert-tls server

cipher AES-128-GCM
tls-client
tls-version-min 1.2
tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256

verb 3
status logs/openvpn-status.log
log logs/openvpn.log

<ca>
`cat /opt/openvpn/keys/${tag}/ca.crt`
</ca>

<cert>
`sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/p' /opt/openvpn/keys/${tag}/${tag}.crt`
</cert>

<key>
`cat /opt/openvpn/keys/${tag}/pki/private/${tag}.key`
</key>

<tls-crypt>
`cat /opt/openvpn/keys/openvpn-server/ta.key`
</tls-crypt>
END
```

## 7. 客户端拨入服务端

```bash
# 配置 OpenVPN 客户端
mkdir -p /opt/openvpn/conf/client /opt/openvpn/keys/home/pki/private
cat > /opt/openvpn/conf/client/home.conf << 'END'
client
proto udp
remote openvpn.hflxhn.com 1194
dev tun

ca keys/home/ca.crt
cert keys/home/home.crt
key keys/home/pki/private/home.key
END

# 上传客户端证书到指定位置
scp ca.crt 192.168.0.23:/opt/openvpn/keys/home
scp home.crt 192.168.0.23:/opt/openvpn/keys/home
scp pki/private/home.key 192.168.0.23:/opt/openvpn/keys/home/pki/private

# 启动 openvpn 客户端
/opt/openvpn/sbin/openvpn --config /opt/openvpn/conf/client/home.conf
```



```bash
cat > /opt/openvpn/client/config/client.conf << 'END'
client
proto udp
explicit-exit-notify
remote 192.168.0.23 1194
dev tun
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
verify-x509-name server_kJgMUzgogcHOjKsp name
auth SHA256
auth-nocache
cipher AES-128-GCM
tls-client
tls-version-min 1.2
tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256
#setenv opt block-outside-dns # Prevent Windows 10 DNS leak
verb 3
<ca>
~/easy-rsa/pki/ca.crt
</ca>
<cert>
~/easy-rsa/pki/issued/openvpn-client-01.crt
</cert>
<key>
~/easy-rsa/pki/private/openvpn-client-01.key
</key>
<tls-crypt>
/opt/openvpn/keys/ta.key
</tls-crypt>
END
```

