# NDS Unbound 服务器

## 搭建缓存 NDS 服务器

### 安装配置 unbound

```bash
# 安装 unbound
yum install unbound -y

# 放行 53 端口
firewall-cmd --add-port=53/tcp
firewall-cmd --add-port=53/tcp --per

# 编辑配置文件
# 1. interface
interface: 0.0.0.0

# 2. access-control
access-control: 0.0.0.0/0 allow

# 3. forward-zone
forward-zone:
    name: "net."
    forward-addr: 223.223.223.223
forward-zone:
    name: "."
    forward-addr: 114.114.114.114
    
# 4. domain-insecure
domain-insecure: "com."
    
# 加载密钥
unbound-control-setup

# 启动服务
systemctl enable unbound.service --now

# 检查端口
ss -tunlp | grep :53
```

### 解析测试 unbound

```bash
# 修改 dns 为 unbound 服务器地址
nmcli con mod ens192 ipv4.dns 172.24.0.248
nmcli con up ens192

# 在客户端请求 www.baidu.com
yum install bind-utils -y
host www.baidu.com

# 抓取 baidu.com 的缓存
unbound-control dump_cache | grep www.baidu.com

# 清除单个缓存
unbound-control flush www.baidu.com
```

## 搭建 "权威" NDS 服务器

```bash
# 编辑配置文件
vi /etc/unbound/unbound.conf
# 1. interface
interface: 0.0.0.0

# 2. access-control
access-control: 0.0.0.0/0 allow

# 3. fanyang.com.conf
cat << "END" > /etc/unbound/local.d/fanyang.com.conf
local-zone: "fanyang.com." static
local-data: "fanyang.com. IN SOA ns.fanyang.com. root.fanyang.com. 1 1h 1h 1h 1h"
local-data: "fanyang.com. IN NS ns.fanyang.com."
local-data: "ns.fanyang.com. IN A 172.24.0.248"

local-data: "www.fanyang.com. IN A 1.1.1.1"
local-data: "ftp.fanyang.com. IN A 2.2.2.2"

local-data: "fanyang.com. IN MX 0 mail.fanyang.com."
local-data: "mail.fanyang.com. IN A 3.3.3.3"

local-data-ptr: "1.1.1.1 www.fanyang.com"
local-data-ptr: "2.2.2.2 ftp.fanyang.com"
local-data-ptr: "3.3.3.3 mail.fanyang.com"
END

# 4. forward
cat << "END" > /etc/unbound/conf.d/forward.com.conf
server:
    domain-insecure: "com."
    domain-insecure: "net."
    domain-insecure: "org."
    domain-insecure: "edu."
    domain-insecure: "top."
    domain-insecure: "cn."
forward-zone:
    name: "."
    forward-addr: 114.114.114.114
END

# 加载密钥
unbound-control-setup

# 重启服务
unbound-checkconf
systemctl restart unbound.service
```

## 查询 DNS 其他信息

```bash
# 查看 MX 记录
dig -t MX fanyang.com

# 查看 NS 记录
dig -t NS fanyang.com

# 查看 CNAME 记录
dig -t CNAME fanyang.com
```

