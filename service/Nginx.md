# Nginx Server

## 1. Nginx 安装

```bash
# 系统常用命令安装
yum install gcc gcc-c++ make cmake wget tar zip unzip bzip2 bash-completion -y

# 下载
wget https://www.openssl.org/source/old/1.1.1/openssl-1.1.1a.tar.gz
wget https://nginx.org/download/nginx-1.22.0.tar.gz

# Nginx 依赖安装
yum install -y pcre-devel zlib-devel openssl-devel

# 创建 nginx 的进程守护者
useradd -M -s /sbin/nologin nginx

# 解压并安装
tar xf openssl-1.1.1a.tar.gz
tar xf nginx-1.22.0.tar.gz && cd nginx-1.22.0
./configure --prefix=/opt/nginx-1.22.0 \
--user=nginx --group=nginx \
--with-http_stub_status_module \
--with-http_ssl_module \
--with-http_random_index_module \
--with-http_sub_module \
--with-http_gzip_static_module \
--with-pcre \
--with-openssl=../openssl-1.1.1a \
--with-openssl-opt='enable-tls1_3' \
--with-http_v2_module \
--with-stream \
--with-stream_ssl_module

make -j12 && make install
cd .. && rm -rf nginx-1.22.0

# 修改nginx目录权限
chown nginx:nginx -R /opt/nginx-1.22.0

# 放行防火墙
firewall-cmd --add-port=80/tcp --per
firewall-cmd --add-port=80/tcp

# 注册系统服务
cat > /etc/systemd/system/nginx.service << END
[Unit]
Description=nginx service
After=network.target network-online.target syslog.target
Wants=network.target network-online.target

[Service]
Type=forking
ExecStart=/opt/nginx-1.22.0/sbin/nginx
ExecReload=/opt/nginx-1.22.0/sbin/nginx -s reload
ExecStop=/opt/nginx-1.22.0/sbin/nginx -s stop

[Install]
WantedBy=multi-user.target
END

# 开机自启动并立即启动
systemctl enable nginx.service --now

# Nginx 新增模块 (编译后无需安装，将 nginx 拷贝到 /opt/nginx-1.22.0/sbin/, 重启 Nginx 即可)
cp objs/nginx /opt/nginx-1.22.0/sbin/
```

## 2. Nginx 负载均衡

```bash
cat > /opt/nginx-1.22.0/conf/conf.d/web.conf << 'END'
upstream backend {
    least_conn;
    server server1.com;
    server server2.com;
    server server3.com backup;
}

server {
    listen       80;
    server_name  localhost;

    location / {
        proxy_pass http://backend;
    }
}
END
```
