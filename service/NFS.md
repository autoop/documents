# NFS (Network File System)

## 1. NFS Server

```bash
# install
yum install nfs-utils -y

# 创建挂载点
mkdir /nfs-share

# 编辑 exports
echo '/nfs-share 172.16.110.0/24(rw)' >> /etc/exports

# 重启服务
systemctl enable --now nfs-server.service

# 查看共享
exportfs

# 防火墙放行
firewall-cmd --add-service=nfs
firewall-cmd --add-service=nfs --per
firewall-cmd --add-service=rpc-bind
firewall-cmd --add-service=rpc-bind --prt
```

## 2. NFS Client

```bash
# 创建挂载点
mkdir /nfs-share

# 挂载网络文件系统
mount -t nfs 172.16.110.3:/nfs-share /nfs-share

# 持续挂载
echo '172.16.110.3:/nfs-share /nfs-share nfs defaults 0 0' >> /etc/fstab
```

