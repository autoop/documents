# Ceph 分布式存储

## Ceph 存储架构

### 1. 介绍云存储 Personas

```bash
红帽 Ceph 存储管理员
这里的 Personas 指的是相对于 ceph 集群而言你的人物定位是什么? 你是存储的使用者? 还是管理者?
需要关注存储管理员 Persona 的 role 和职责, 并介绍存储管理员 Persona.
```

### 2. Ceph 存储的架构

```bash
Red Hat Ceph Storage 是一个分布式数据对象存储.
它是一种企业级,软件定义的存储解决方案,可扩展到数千个客户端,这些客户端可以访问 EB 以上的数据. 
Ceph 提供出色的性能、可靠性和可伸缩性.

Ceph 采用模块化和分布式架构, 包含了以下组件:
1. 对象存储后端, 称为 RADOS (Reliable Autonomic Distributed Object Store 可靠自主分布式对象存储)
2. 多种和RADOS交互方式
RADOS 是一个自修复(self-healing)和自管理(self-managing)的基于软件的对行存储.
```

### 3. Ceph 存储后端组件

![ceph-diagramm](http://typora.hflxhn.com/documents/2024/1007/ceph-diagramm.png)

```bash
红帽 Ceph 存储集群包含以下守护进程:
1. Monitors (MONs) 维护集群状态的映射 (map). MONs也帮助其它的守护进程互相协调
2. Object Storage Devices (OSDs) 存储数据和处理数据的复制, 恢复和 relalancing (重新平衡)
3. Manages (MGRS) 跟踪运行时指标, 并通过浏览器仪表盘和 REST API 公开集群信息.
4. Metadata Servers (MDSes) 存储 CephFS 使用的元数据(非对象存储和块存储)以便于客户端可以执行 POSIX 命令
这些守护进程都可以扩展到足以满足部署存储集群的需求.
```

#### 3.1 Monitors (MON)

```bash
Ceph Monitors (MONS) 是维护集群映射的守护进程. 集群映射是五个映射的集合,其中包含关于集群状态及其他配置信息.
Ceph 必须处理每个集群事件, 更新适当的映射, 并将更新的映射复制到每个 MON 守护进程.
要更新, MONs 必须就集群的状态建立共识. 大多数已配置的 Monitors 必须可用, 并同意映射更新.
用奇数个 Monitors 配置 Ceph 集群, 以确保 Monitors 在对集群配置的状态进行投票时能建立仲裁.
要是 Ceph 存储集群具有可操作性和可访问性, 所配置的 Monitor 的一半以上必须是可用的.
```

#### 3.2 Object Storage Devices (OSD)

![Ceph_Strategies-Guide_459708_1017_02](http://typora.hflxhn.com/documents/2024/1007/Ceph_Strategies-Guide_459708_1017_02.png)

```bash
Ceph Object Storage Devices (OSD) 是 Ceph 存储集群的底层块设备.
OSD 将存储设备 (如硬盘和其他块设备) 连接到 Ceph 存储集群. 单个存储服务器可以运行多个 OSD 守护进程, 为集群提供多个 OSD.
Red Hat Ceph Storage 5 支持一个将数据存储在 RADOS 中名为 BlueStore 的特性. 
BlueStore 采用本地存储设备的 raw 模式, 是为高性能场景 (SSD) 设计的.

OSD 操作的一个设计目标是使计算能力尽可能接近物理数据, 以便集群能够以最高效率执行.
Ceph 客户端和 OSD 守护进程都使用了 CRUSH (Controlled Replication Under Scalable Hashing - 可伸缩哈希下的受控的复制) 
算法来高效的计算对象位置信息, 而不是依赖于中央查找表.

# CRUSH MAp
CURSH 将每个对象分配给一个 Placement Group (PG), 这是一个单独的散列桶. PG 是对象 (应用层)和osd(物理层)之间的抽象层.
CRUSH 使用伪随机放置算法将对象分布到 PG 中, 并使用规则确定 PG 到 OSD 的映射.
如果发生故障, Ceph 将 PG 重新映射到不同的物理设备 (OSD)， 并同步其内容以匹配配置的数据保护规则.
```

![ceph-data-placement](http://typora.hflxhn.com/documents/2024/1007/ceph-data-placement.png)

```bash
# Primary 和 Secondary OSDs
一个 OSD 对象 PG 的 Primary OSD， Ceph 客户端读写数据时总是联系代理集群中的主 OSD.
其他 osd 是 Secondary OSD, 这些 Secondary OSD 在确保集群发生故障时数据恢复能力方面发挥着重要的作用.

# Primary OSD 功能:
处理所有 I/O
请求复制和保护数据
检查数据的一致性
平衡数据
恢复数据

# Secondary OSD 功能:
总是在 Primary OSD 的控制下行动
可以成为 Primary OSD
```

![platform-technology-architecture-6](http://typora.hflxhn.com/documents/2024/1007/platform-technology-architecture-6.png)

```bash
注意: 运行 OSD 的主机不能使用基于内核的客户端挂载 Ceph RBD 映射或 cephfs 文件系统.
由于过期会话挂起的内存死锁或者阻塞 I/O, 挂载的资源可能失去响应.
```

#### 3.3 Managers (MGR)

```bash
Ceph Managers (MGR) 提供一组集群的统计信息
如果集群中没有 MGR, 客户端的IO操作不会受到影响, 但是尝试查询集群统计信息将会失败. 
为了避免这种情况, 红帽建议你将每个集群至少部署2个MGR, 每个MGR都在单独的故障域中运行.
MGR守护进程集中了对从集群收集的所有数据的访问, 并为存储管理员提供了一个简单的WEB仪表盘.
MGR守护进程还可以将状态信息导出到外部监控服务器, 例如 Zabbix.
```

#### 3.4 Metadata Server (MDS)

```bash
Ceph 元数据服务器 (MDS ceph-mds) 代表 Ceph 文件系统存储元数据 (即 Ceph 块设备和 Ceph 对象存储不使用 MDS).
Ceph 元数据服务器允许 POSIX 文件系统用户执行基本的命令 (如 ls, find等), 而不会给 Ceph 存储集群带来巨大的负担.
MDS 使用 cephfs 能够与 Ceph 对象存储进行交互,将一个 inode 映射到一个对象和Ceph在树中存储数据的位置.
访问 cephfs 文件系统的客户端首先向MDS发送一个请求, MDs 提供从正确的 OSD获取文件内容所需要的信息.
```

#### 3.5 Ceph Cluster Map (集群映射)

```bash
Ceph 客户端和 OSD 需要知道集群拓扑的消息. 5个 map (映射)表示了集群的拓扑,这个集群拓扑通常被叫做 Cluster Map (集群映射).
Ceph Monitor 守护进程维护 Cluster map. 如果一个 Ceph MONs 故障了, Ceph MONs 集群确保了高可用性.

1. Monitor Map 包含集群 fsid; 每个Ceph Monitor 的位置, 名字, 地址, 端口, 映射的时间戳.
fsid 是唯一自动生成的 UUID, 用来标识 Ceph 集群. 可以使用 ceph mon dump 命令查看 Monitor Map.

2. OSD Map 包含集群 fsid，pool 的列表, replica 大小，PG数量，OSD的列表和状态，map时间戳。
使用 ceph osd dump 命令查看OSD map。

3. PG Map 包含 PG 版本，完整的比率，每个PG ID，up set，Acting Set，PG状态，每个pool使用情况的详细信息，map的时间戳。
使用 ceph pg dump命令查看PG map。

4. CRUSH Map包含存储设备的列表，故障域层次结构（如设备，主机，机架，行，房间）的列表以及存储数据时遍历该层次结构的规则。
使用ceph osd crush dump命令查看CRUSH map。

5. Metadata Server （MDS）Map包含存储元数据的pool， MDS的列表状态和map的时间戳。
使用 ceph fs dump 查看MDS Map。
```

### 4. Ceph 的访问方式

```bash
Ceph 提供以下方式访问 Ceph 集群:
1. Ceph Native API (librados)
2. Ceph Block Device (RBD, librbd), RBD
3. Ceph Object Gateway (librgw)
4. Ceph File Systen (CephFSm libcephfs)
下图描述了Ceph集群的四种数据访问方法，支持访问方法的库，以及管理和存储数据的底层Ceph组件。
```

![5](http://typora.hflxhn.com/documents/2024/1007/5.png)

#### 4.1 Ceph本地API（librados）

```bash
如 RBD 和 Ceph Object Gateway 实现其它 Ceph 接口的基础库是 librados。
librados 库是一个原生的 C 库，它允许应用程序直接使用 RADOS 访问 Ceph 集群存储对象。
类似的库也可以用于 C++, Java, Python, Ruby, Erlang 和 PHP。

为了最大化性能，编写应用时要直接使用 librados。这种方法在 Ceph 环境中提高存储性能的效果最好。
为了更方便的Ceph存储访问，请使用提供的更高级别的访问方法，如RADOS块设备，Ceph对象网关(RADOSGW)和cephfs。
```

#### 4.2 RBD

```bash
Ceph Block Device (RADOS Block Device/RBD)通过Ceph集群中的RBD镜像提供块设备。Ceph 将组成的RBD镜像的单个对象分散在集群中的不同OSD中。
因为组成RBD的对象位于不同的OSD上，所以对块设备的访问会自动并行化。
RBD提供以下特性:
1. Ceph集群中虚拟磁盘的存储
2. 在Linux内核中挂载的支持
3. QEMU，KVM，Openstack Cinder的引导支持
```

#### 4.3 RADOS网关

```bash
Ceph Object Gateway(RADOS Gateway, RADOSGW或RGW)是一个用librados构建的对象存储接口。它使用这个库与Ceph集群通信，并直接写入OSD进程。
为应用提供基于RESTful API的网关，支持Amazon S3和Openstack Swift两种接口。

注意：你只需要更新endpoint来一直使用Amazon S3 API和Openstack Swift API 的现有应用程序。

Ceph Object Gateway通过不限制已部署网关的数量和提供对标准HTTP负载均衡器的支持，提供了可伸缩性支持。Ceph Object Gateway 包括以下用例:
1. 镜像存储
2. 备份服务
3. 文件存储和共享
```

#### 4.4 CephFS

```bash
Ceph File System (cephfs)是一个并行文件系统，它提供一个可伸缩的，单层次的共享磁盘。
Red Hat Ceph Storage为cephfs提供生产环境支持，包括对快照的支持。
Ceph Metadata Server (MDS)管理与存储在cephfs中的文件相关的元数据，包括文件访问，更改和修改时间戳。
```

#### 4.5 Ceph 客户端组件

```bash
云感知(Cloud-aware)应用程序需要一个具有异步通信能力的简单对象存储接口。
Red Hat Ceph存储集群提供了这样的接口。客户端可以直接、并行地访问对象，并可以访问整个集群，包括:
1.pool操作
2.快照
3.对象的读写，对象的创建和删除，
4.创建/设置/获得/删除XATTRs
5.创建/设置/获得/删除 key vaule对
6.复合操作和双确认规则

当客户端写入RBD镜像时，object map跟踪备份RADOS对象的存在。当发生写操作时，它将被转换为后端RADOS对象中的偏移量。
当启用object map特性时，将跟踪存在的RADOS对象，以表示对象的存在。Objectmap保存在librbd客户端的内存中，以避免在osd中查询不存在的对象。

object map对于某些操作是有益的，例如:
Resize，Export，Copy，Flatten，Delete，Read。

存储设备有吞吐量限制，影响性能和可扩展性。存储系统通常支持striping（分条化），分条化在多个存储设备存储有序的信息来增加吞吐量和性能。
当Ceph客户端向集群写入数据时，可以使用数据分条来增加性能。
```

### 5. Ceph 存储管理接口

#### 5.1 Ceph 接口介绍

```bash
之前的ceph版本使用ceph-ansible的playbook来部署ansible和管理ceph集群。
红帽ceph5使用cephadm代替之前的ceph-ansible作为管理整个集群生命周期的工具，包括部署，管理，监控。

当部署一个新集群的时候，cephadm使用MGR作为第一个守护进程。
ceph集群核心集成所有的管理任务并且当集群开始的时候cephadm已经准备好了。

cephadm软件包提供了cephadm。你应该在第一个集群节点安装这个包，这个节点作为bootstrap节点。
因为ceph5的部署是容器化的，唯一需要让集群up的软件包就是cepadm，podman，python3和chrony。
这个容器化的版本减少了ceph集群部署的复杂性和依赖性。
下图展示了cephadm如何和其它服务通信。
```

![6](http://typora.hflxhn.com/documents/2024/1007/6.png)

```bash
cephadm可以登录到容器仓库来拉取ceph镜像和使用对应镜像来在对应ceph节点进行部署。
ceph容器镜像对于部署ceph集群是必须的，因为被部署的ceph容器是基于那些镜像。
为了和ceph集群节点通信，cephadm使用ssh。通过使用ssh连接，cephadm可以向集群中添加主机，添加存储和监控那些主机。
```

#### 5.2 ceph命令行接口

```bash
Cephadm可以启动一个容器版本的shell，其中安装了所有所需的Ceph包。运行这个容器shell的命令是cephadm shell。
你应该只在bootstrap节点中运行此命令，因为在引导集群时，只有该节点可以访问/etc/ceph中的管理密匙环（key ring）。

# 命令行接口
ssh root@serverc.lab.example.com
cephadm shell
cephadm shell -- ceph status
```

#### 5.3 ceph图形接口

```bash
Red Hat Ceph Storage 5 Dashboard GUI通过这个接口得到了增强，可以支持许多集群任务。
Ceph Dashboard GUI是一个基于web的应用程序，用于监视和管理集群。它以比Ceph CLI更直观的方式提供集群信息。
像Ceph CLI一样，Ceph实现了作为Ceph-mgr守护进程模块的Dashboard GUI web服务器。
默认情况下，Ceph在创建集群时将Dashboard GUI部署在引导节点中，并使用TCP端口8443。

# 图形化
serverc.lab.example.cpm:8443
admin redhat
```

## 部署红帽 Ceph 集群

### 1. 集群部署准备工作

```bash
使用 cephadm 实用程序部署一个新的 Red Hat Ceph Storage 5 集群。cephadm 实用程序由两个主要组件组成:
1. cephadm shell
2. cephadm orchestrator

cephadm shell命令在 ceph 提供的管理容器中运行 bash shell。首先使用 cephadm shell 执行集群部署任务，然后在集群安装和运行后执行集群管理任务。

启动 cephadm shell 以交互方式运行多个命令，或者运行单个命令。要以交互方式运行它，使用 cephadm shell 命令打开 shell，然后运行Ceph命令。

cephadm orchestrator 为 orchestrator ceph-mgr 模块提供了一个命令行接口，该模块与外部编排服务连接。
orchestor 的目的是协调必须在存储集群中的多个节点和服务之间协作执行的配置更改。

[root@serverc ~]# cephadm shell
[root@serverc ~]# cephadm shell -- CEPH_COMMAND
```

#### 1.1 计划集群服务排列

```bash
所有集群服务现在都作为容器运行. 集装箱化的Ceph服务可以运行在同一个节点上；这被称为 colocation。
Ceph 服务的托管允许更好的资源利用，同时保持服务之间的安全隔离。
可与OSD配置的守护进程有: RADOSGW，MDS，RBD-mirror，MON，MGR，Grafana和NFS Ganesha。
```

#### 1.2 主机间的安全通信

```bash
cephadm 命令使用 ssh 协议与存储集群节点通信。集群 SSH 密钥在集群引导过程中创建。将集群公钥复制到将成为集群成员的每个主机。
将集群密钥复制到集群节点：
[root@serverc ~]# cephadm shell
[ceph: root@serverc /]# ceph cephadm get-pub-key > ~/ceph.pub
[ceph: root@serverc /]# ssh-copy-id -f -i ~/ceph.pub root@serverd.lab.example.com
```

### 2. 部署新集群

```bash
部署新的集群步骤如下：
1. 在你选择作为引导节点的主机上安装 cephadm-ansible 包，该主机是集群中的第一个节点。
2. 运行 cephadm 飞行前 playbook (preflight playbook).此剧本验证主机是否具备所需的先决条件。
3. 使用 cephadm 引导集群。bootstrap 过程完成以下任务：
3.1 在引导节点上安装并启动 Ceph Monitor 和 Ceph Manager 守护进程。
3.2 创建 /etc/ceph 目录
3.3 将集群公共SSH密钥的副本写入 /etc/ceph/ceph.pub 并将键添加到 /root/.ssh/authorized_keys 文件中。
3.4 将与新集群通信所需的最小配置文件写入 /etc/ceph/ceph.conf 文件。
3.5 在 /etc/ceph/ceph.client.admin.keyring 写入 client.admin 管理密钥的拷贝。
3.6 使用 prometheus 和 grafana 服务以及其他工具 (如节点导出器和警报管理器)部署基本监视堆栈。
```

#### 2.1 安装的先决条件

```bash
在 bootstrap 节点上安装 cephadm-ansible
yum install cephadm-ansible -y

运行cephadm-preflight.yaml的剧本。这本playbook配置Ceph存储库，并为引导准备存储集群。
它还安装像podman，lvm2，chrony和cephadm这样的先决条件包。

飞行前 playbook 使用 cephadm-ansible 清单文件来识别存储集群中的管理节点和客户机节点。

inventory文件的默认位置为/usr/share/cephadm-ansible/hosts。下面的例子展示了一个典型的
inventory文件的结构:
[admin]
node00

[clients]
cluster01
cluster02
cluster03

运行 pre-flight playbook
ansible-playbook -i INVENTORY-FILE cephadm-preflight.yml --extra-vars "ceph_origin=rhcs"
```

#### 2.2 bootstrapping 集群

```bash
cephadm 引导过程在单个节点上创建一个小型存储集群，包括一个Ceph Monitor和一个Ceph Manager，以及任何所需的依赖项。
通过使用ceph orchestrator命令或Dashboard GUI扩展存储集群，添加集群节点和服务。
注意：在引导之前，必须指定registry的用户名和密码。
cephadm bootstrap --mon-ip=MON_IP --registry-url=registry.redhat.io --registry-username=REGISTRY_USERNAME --registry-password=REGISTRY_PASSWORD --initial-dashboard-password=DASHBOARD_PASSWORD --dashboard-password-noupdate --allow-fqdn-hostname
```

#### 2.3 使用服务特定文件(IAC)

```bash
使用cephadm bootstrap命令和--apply-spec参数以及一个服务规范文件来引导存储集群并配置额外的主机和守护进程。
配置文件是一个YAML文件，其中包含服务类型、位置和要部署的服务的指定节点。

cat ceph/initial-config-primary-cluster.yaml
```

#### 2.4 部署红帽ceph存储练习

```bash
# 在宿主机配置
mkdir -p /content/rhel8.4/x86_64/rhel8-additional
cp -a /content/rhcs5.0/x86_64/dvd/ansible-2.9-for-rhel-8-x86_64-rpms/ /content/rhel8.4/x86_64/rhel8-additional/
cp -a /content/rhcs5.0/x86_64/dvd/rhceph-5-tools-for-rhel-8-x86_64-rpms/ /content/rhel8.4/x86_64/rhel8-additional/

# 加载教学环境
lab start deploy-deploy

# 登陆 serverc
ssh root@serverc.lab.example.com

# 安装 ceph-ansible
yum install -y cephadm-ansible

# 配置 ansible
cd /usr/share/cephadm-ansible/

cat << 'END' > hosts 
clienta.lab.example.com
serverc.lab.example.com
serverd.lab.example.com
servere.lab.example.com
END

# 运行起飞前脚本
ansible-playbook -i hosts cephadm-preflight.yml --extra-vars "ceph_origin="

cd /root/ceph
cat /root/ceph/initial-config-primary-cluster.yaml

# 部署
cephadm bootstrap --mon-ip=172.25.250.12 --apply-spec=initial-config-primary-cluster.yaml --initial-dashboard-password=redhat --dashboard-password-noupdate --allow-fqdn-hostname --registry-url=registry.lab.example.com --registry-username=registry --registry-password=redhat

[root@serverc ceph]# cephadm shell
[ceph: root@serverc /]# ceph health

# 将 clienta 加入 admin 中
[ceph: root@serverc /]# ceph orch host label add clienta.lab.example.com _admin
exit

# 上传 ceph 管理密钥和配置文件到 clienta
scp /etc/ceph/{ceph.client.admin.keyring,ceph.conf} root@clienta:/etc/ceph/

# 在 workstation 登陆 clienta
ssh root@clienta
cephadm shell
ceph status
ceph health

# 结束练习
lab finish deploy-deploy
```

### 3. 给集群节点分配标签

```bash
Ceph orchestrator支持为主机分配标签。标签可以用来对集群主机进行分组，这样你就可以同时将Ceph服务部署到多个主机。一个主机可以有多个标签。

通过帮助识别每个主机上运行的守护进程，标签简化了集群管理任务。你可以使用ceph orchestrator或YAML服务规范文件在特定标记的主机上部署或删除守护进程。

除了_admin标签之外，标签都是自由形式的，没有特定的含义。可以使用mon、monitor、mycluster_monitor等标签或其他文本字符串来标记和分组集群节点。
例如，将mon标签分配给部署mon守护进程的节点。为部署了mgr守护进程的节点分配mgr标签，为RADOS网关分配rgw标签。

例如，下面的命令将_admin标签应用到一个主机，以指定管理节点。
ceph orch host label add ADMIN_NODE _admin

通过标签方式将集群守护进程部署到指定主机。
ceph orch apply prometheus --placement="label:prometheus"

#设置管理节点
配置admin节点的操作步骤如下:
1.为节点分配admin标签。
2.将admin密钥复制到admin节点。
3.拷贝ceph.conf到管理节点。
scp /etc/ceph/ceph.client.admin.keyring ADMIN_NODE:/etc/ceph
scp /etc/ceph/ceph.conf ADMIN_NODE:/etc/ceph/
```

### 4. 扩容ceph集群

```bash
你可以扩展你的Red Hat Ceph存储集群的存储容量，而不中断活跃的存储活动。有两种方法可以扩展集群中的存储:
1.向集群中添加额外的OSD节点，即向外扩展。
2.为现有OSD节点增加额外的存储空间，即扩容。
在开始部署额外的osd之前，使用 cephadm shell -- ceph health 命令验证集群处于HEALTH_OK状态。
```

#### 4.1 配置额外的OSD服务器

```bash
作为存储管理员，你可以向Ceph存储集群添加更多主机，以保持集群的健康状态并提供足够的负载容量。
当当前存储空间即将满时，可通过增加一个或多个osd扩容存储集群。

使用root用户，将Ceph存储集群公共SSH密钥添加到新主机上root用户的authorized_keys文件中。
ssh-copy-id -f -i /etc/ceph/ceph.pub root@new-osd-1

使用root用户，向位于/usr/share/cephadm-ansible/hosts/的目录文件中添加新节点。
使用--limit参数运行preflight playbook，以限制playbook的任务只在指定的节点上运行。Ansible Playbook会验证需要添加的节点是否满足包的要求。
ansible-playbook -i /usr/share/cephadmin-ansible/hosts/ /usr/share/cephadm-ansible/cephadm-preflight.yml --limit new-osd-1

选择一种向Ceph存储集群添加新主机的方法:
1.在Cephadm shell下，以root用户使用ceph orch host add命令为存储集群添加新主机。在本例中，该命令还分配主机标签。
ceph orch host add new-osd-1 --labels=mon,osd,mgr
2.要添加多个主机，创建一个带有主机描述的YAML文件，在管理容器中创建YAML文件，
然后在其中运行ceph orch.. 创建YAML文件后，运行ceph orch apply命令添加osd:
ceph orch apply -i host.yaml
```

#### 4.2 列出主机

```bash
使用 ceph orch host ls 从cephadm shell中列出集群节点。STATUS列为空说明主机在线且运行正常。
[root@clienta ceph]# ceph orch host ls
```

#### 4.3 为OSD服务器配置额外的OSD存储

```bash
作为存储管理员，你可以通过向现有的OSD节点添加新的存储设备来扩展Ceph集群的容量，然后使用cephadm orchestrator将它们配置为OSD。
Ceph要求满足以下条件来考虑存储设备:
1.设备不能有任何分区。
2.设备不能有LVM状态。
3.设备不能挂载。
4.设备不能包含文件系统。
5.设备不能包含Ceph BlueStore OSD。
6.设备的容量必须大于5GB。

在cephadm shell中执行ceph orch device ls命令列出可用的设备。--wide参数提供更多的设备详细信息。

[root@clienta ceph]# ceph orch device ls

以root用户运行ceph orch daemon add osd命令，在指定主机的指定设备上创建osd。 
ceph orch daemon add osd osd-1:/dev/vdb

或者执行ceph orch apply osd --all-available-devices命令，在所有可用和未使用的设备上部署osd。
ceph orch apply osd --all-available-devices

通过包含特定的磁盘属性，你可以只使用特定主机上的特定设备来创建osd。
以下示例在每台主机的default_drive_group组中的/dev/vdc和/dev/vdd中创建两个osd。

ceph orch apply -i /var/lib/ceph/osd/osd_spec.yml
```

#### 4.4 扩容练习

```bash
# 加载教学环境
lab start deploy-expand

# 连接 clienta
ssh root@clienta

# 查看设备
ceph orch device ls

# 挂载
cephadm shell --mount /root/expand-osd/osd_spec.yml
ceph orch apply -i /root/expand-osd/osd_spec.yml

# 查看
ceph orch device ls --hostname=servere.lab.example.com

# 添加
ceph orch daemon add osd servere.lab.example.com:/dev/vde
ceph orch daemon add osd servere.lab.example.com:/dev/vdf

ceph osd tree
ceph osd df

lab finish deploy-expand
```
