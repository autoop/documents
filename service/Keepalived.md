# Keepalived Server

## 软件安装

```bash
yum install -y gcc make openssl-devel libnl3-devel
wget https://www.keepalived.org/software/keepalived-2.2.1.tar.gz
tar -xf keepalived-2.2.1.tar.gz
cd keepalived-2.2.1
./configure --prefix=/opt/keepalived-2.2.1 && make && make install
```

## 服务启动

```bash
ls /opt/keepalived-2.2.1/etc/keepalived/keepalived.conf.bak &>/dev/null || cp /opt/keepalived-2.2.1/etc/keepalived/keepalived.conf{,.bak}

# systemctl
cat << 'END' > /etc/systemd/system/keepalived-2.2.1.service
[Unit]
Description=Keepalived service
After=network.target

[Service]
Type=forking
ExecStart=/opt/keepalived-2.2.1/sbin/keepalived -D --use-file=/opt/keepalived-2.2.1/etc/keepalived/keepalived.conf
ExecReload=/bin/kill -HUP $MAINPID

[Install]
WantedBy=multi-user.target
END

systemctl enable keepalived-2.2.1.service --now
```

## nginx 高可用

```bash
# check nginx script
mkdir -p /opt/keepalived-2.2.1/script
cat << 'END' > /opt/keepalived-2.2.1/script/check-nginx.sh
#!/bin/bash
if ! systemctl is-active --quiet nginx-1.22.0.service; then
    systemctl start nginx-1.22.0.service
    if ! systemctl is-active --quiet nginx-1.22.0.service; then
        systemctl stop keepalived-2.2.1.service
    fi
fi
END
chmod +x /opt/keepalived-2.2.1/script/check-nginx.sh

# server 1 config
cat << 'END' > /opt/keepalived-2.2.1/etc/keepalived/keepalived.conf
vrrp_script check_nginx {
    script "/opt/keepalived-2.2.1/script/check-nginx.sh"
    interval 2
    fall 2
    rise 2
}

vrrp_instance VI_1 {
    state MASTER
    interface ens192
    virtual_router_id 66
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 654321
    }
    virtual_ipaddress {
        10.173.21.19
    }
    track_script {
        check_nginx
    }
}

END

# server 2 config
cat << 'END' > /opt/keepalived-2.2.1/etc/keepalived/keepalived.conf
vrrp_script check_nginx {
    script "/opt/keepalived-2.2.1/script/check-nginx.sh"
    interval 2
    fall 2
    rise 2
}

vrrp_instance VI_1 {
    state BACKUP
    interface ens192
    virtual_router_id 66
    priority 80
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 654321
    }
    virtual_ipaddress {
        10.173.21.19
    }
    track_script {
        check_nginx
    }
}
END
```
