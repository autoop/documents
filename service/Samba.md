# Samba Server

## 1. Samba 服务安装

```bash
# 安装 samba 服务
yum install samba -y

# 配置 samba 目录
groupadd samba
mkdir -p /samba-share
chgrp samba /samba-share
chmod 2775 /samba-share
ls -ld /samba-share
semanage fcontext -a -t samba_share_t '/samba-share(/.*)?'
restorecon -Rv /samba-share/
```

![image-20230611220728123](http://typora.hflxhn.com/documents/2023/0613/image-20230611220728123.png)

```bash
# samba 服务配置
tail -n7 /etc/samba/smb.conf

[samba-share]
    comment = samba share
    path = /samba-share
    valid users = win, @samba
    browseable = yes
    read only = yes
    write list = @samba
```

![image-20230611220835071](http://typora.hflxhn.com/documents/2023/0613/image-20230611220835071.png)

```bash
# 创建访问 samba 服务的用户
useradd -g samba -s /sbin/nologin win
smbpasswd -a win
pdbedit -L
```

![image-20230611220948969](http://typora.hflxhn.com/documents/2023/0613/image-20230611220948969.png)

```bash
# 防火墙放行
firewall-cmd --add-service=samba
firewall-cmd --add-service=samba --per
```

![image-20230611221006594](http://typora.hflxhn.com/documents/2023/0613/image-20230611221006594.png)

```shell
#  服务启动
systemctl enable smb.service --now
systemctl status smb.service
```

![image-20230611221541222](http://typora.hflxhn.com/documents/2023/0613/image-20230611221541222.png)

## 2. Linux 客户端安装

```bash
# 客户端软件安装
yum install cifs-utils -y
```

```bash
# 临时挂载 samba 分享的目录
mkdir -p ~/samba-share/
mount -t cifs -ousername=win,password=123 //127.0.0.1/samba-share ~/samba-share/
df -Th
```

![image-20230611221627616](http://typora.hflxhn.com/documents/2023/0613/image-20230611221627616.png)

```bash
# 永久挂载
cat >> /etc/fstab << END
//127.0.0.1/samba-share /root/samba-share/ cifs defaults,user=win,password=123 0 0
END
```

## 3. Win 挂载

### 1. 映射网络驱动器

![image-20230612092236505](http://typora.hflxhn.com/documents/2023/0613/image-20230612092236505.png)

### 2. 输入挂载地址

![image-20230612092414242](http://typora.hflxhn.com/documents/2023/0613/image-20230612092414242.png)

### 3. 输入用户凭证

![image-20230612092851159](http://typora.hflxhn.com/documents/2023/0613/image-20230612092851159.png)

### 4. 映射成功

![image-20230612092931800](http://typora.hflxhn.com/documents/2023/0613/image-20230612092931800.png)
