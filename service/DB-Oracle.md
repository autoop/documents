# Oracle 19c Server

## 部署 Oracle 19C

### 1.  前置准备

```bash
# 修改安全限制
cat > /etc/security/limits.d/30-oracle.conf << 'END'
oracle   soft   nofile    1024
oracle   hard   nofile    65536
oracle   soft   nproc     16384
oracle   hard   nproc     16384
oracle   soft   stack     10240
oracle   hard   stack     32768
oracle   hard   memlock   134217728
oracle   soft   memlock   134217728
END

# 修改 linux 内核
cat > /etc/sysctl.d/98-oracle.conf << 'END'
fs.file-max = 6815744
kernel.sem = 250 32000 100 128
kernel.shmmni = 4096
kernel.shmall = 1073741824
kernel.shmmax = 4398046511104
kernel.panic_on_oops = 1
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048576
net.ipv4.conf.all.rp_filter = 2
net.ipv4.conf.default.rp_filter = 2
fs.aio-max-nr = 1048576
net.ipv4.ip_local_port_range = 9000 65500
END

# 安装依赖软件
yum install xauth xorg-x11-utils binutils.x86_64 gcc.x86_64 gcc-c++.x86_64 glibc.x86_64 glibc-devel.x86_64 ksh libaio.x86_64 libaio-devel.x86_64 libgcc.x86_64 libnsl zip -y

# 设置主机名
hostnamectl set-hostname db-oracle

# 创建用户
groupadd -g 54321 oinstall
groupadd -g 54322 dba
groupadd -g 54323 oper
groupadd -g 54324 backupdba
groupadd -g 54325 dgdba
groupadd -g 54326 kmdba
groupadd -g 54330 racdba
useradd -u 54321 -g oinstall -G dba,oper,backupdba,dgdba,kmdba,racdba oracle
echo "oracle" | passwd --stdin oracle

# 创建 oracle 工作目录
mkdir /u01
mkdir -p /u01/app/oracle/product/19.3.0/dbhome_1
chown oracle:oinstall -R /u01/

# 创建 oraInst.loc
cat > /etc/oraInst.loc << 'END'
inventory_loc=/u01/app/oraInventory
inst_group=oinstall
END
chown oracle:oinstall /etc/oraInst.loc
```

### 2. 开始安装

```bash
# 使用 oracle 登陆
ssh oracle@localhost

# 添加环境变量
cat >> ~/.bashrc << 'END'
export ORACLE_HOSTNAME=db-oracle
export ORACLE_BASE=/u01/app/oracle
export ORACLE_HOME=$ORACLE_BASE/product/19.3.0/dbhome_1
export ORA_INVENTORY=/u01/app/oraInventory
export PATH=$ORACLE_HOME/bin:$PATH
export ORACLE_SID=test
export PDB_NAME=pdb1
export DATA_DIR=/u01/oradata
END
sed -i "s/db-oracle/$(hostname -f)/" ~/.bashrc
. ~/.bashrc

# 下载解压
wget http://172.24.0.248/web/soft/linux/oracle/LINUX.X64_193000_db_home.zip
unzip -q LINUX.X64_193000_db_home.zip  -d /u01/app/oracle/product/19.3.0/dbhome_1/
cd /u01/app/oracle/product/19.3.0/dbhome_1/

# 安装 oracle
export CV_ASSUME_DISTID=OEL7.6
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"
./runInstaller -ignorePrereq -waitforcompletion -silent \
  oracle.install.option=INSTALL_DB_SWONLY \
  ORACLE_HOSTNAME=${ORACLE_HOSTNAME} \
  UNIX_GROUP_NAME=oinstall \
  INVENTORY_LOCATION=${ORA_INVENTORY} \
  ORACLE_HOME=${ORACLE_HOME} \
  ORACLE_BASE=${ORACLE_BASE} \
  oracle.install.db.InstallEdition=EE \
  oracle.install.db.OSDBA_GROUP=dba \
  oracle.install.db.OSBACKUPDBA_GROUP=backupdba \
  oracle.install.db.OSDGDBA_GROUP=dgdba \
  oracle.install.db.OSKMDBA_GROUP=kmdba \
  oracle.install.db.OSRACDBA_GROUP=racdba \
  SECURITY_UPDATES_VIA_MYORACLESUPPORT=false \
  DECLINE_SECURITY_UPDATES=true

# root 执行脚本
ssh root@localhost /u01/app/oracle/product/19.3.0/dbhome_1/root.sh

# 初始化
dbca \
-silent -createDatabase \
-templateName General_Purpose.dbc \
-gdbname ${ORACLE_SID} \
-sid  ${ORACLE_SID}   \
-responseFile NO_VALUE   \
-characterSet AL32UTF8   \
-sysPassword Fhl3y_Jsdwj   \
-systemPassword Fhl3y_Jsdwj   \
-createAsContainerDatabase true   \
-numberOfPDBs 1   -pdbName ${PDB_NAME}   \
-pdbAdminPassword Fhl3y_Jsdwj   \
-databaseType MULTIPURPOSE   \
-automaticMemoryManagement false   \
-totalMemory 800   \
-storageType FS   \
-datafileDestination "${DATA_DIR}"   \
-redoLogFileSize 50   \
-emConfiguration NONE   \
-ignorePreReqs
```

### 3.  配置系统服务

```sql
# 使用 root 登陆
ssh root@localhost

# 创建启动脚本
mkdir /opt/script
cat > /opt/script/start_oracle.sh << 'END'
#!/bin/bash

export ORACLE_HOME=/u01/app/oracle/product/19.3.0/dbhome_1
export ORACLE_SID=TEST

$ORACLE_HOME/bin/dbstart $ORACLE_HOME
END
chown oracle:oinstall /opt/script/start_oracle.sh
chmod +x /opt/script/start_oracle.sh

# 创建系统服务
cat > /etc/systemd/system/oracle.service << END
[Unit]
Description=Oracle Database Service
After=network.target

[Service]
Type=forking
Restart=no
User=oracle
Group=oinstall
ExecStart=/opt/script/start_oracle.sh

[Install]
WantedBy=multi-user.target
END
systemctl enable oracle.service --now

# 修改 /etc/oratab
sed -i 's|^test:/u01/app/oracle/product/19.3.0/dbhome_1:N|test:/u01/app/oracle/product/19.3.0/dbhome_1:Y|' /etc/oratab
```

## Oracle 常用命令

### 1. 登陆数据库

```bash
ssh oracle@localhost "sqlplus / as sysdba"
startup
SELECT instance_name, status FROM v$instance;
```

### 2. 创建用户

```bash
# 1. 确认当前连接到 PDB
SELECT name, open_mode FROM v$pdbs;

# 2. 打开 PDB1
# 将 PDB1 状态从 MOUNTED 改为 READ WRITE 模式：
ALTER PLUGGABLE DATABASE PDB1 OPEN;
# 或者，如果希望直接打开 PDB 并设置为可读写模式：
ALTER PLUGGABLE DATABASE PDB1 OPEN READ WRITE;

# 3. 确认 PDB1 状态
SELECT name, open_mode FROM v$pdbs;

# 4. 切换到 PDB1 并创建用户
# -- 切换到 PDB1
ALTER SESSION SET CONTAINER = PDB1;

# -- 创建用户
CREATE USER my_user IDENTIFIED BY my_password;

# -- 授予权限
GRANT CREATE SESSION TO my_user;
GRANT CONNECT, RESOURCE TO my_user;

# -- 设置表空间配额
ALTER USER my_user QUOTA UNLIMITED ON USERS;
```

