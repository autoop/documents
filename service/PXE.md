# PXE Kickstart

## 搭建 tftp 服务器

```bash
# 安装 tftp-server
yum -y install tftp-server

# 启动服务
systemctl enable tftp --now

# 放行防火墙
firewall-cmd --add-service=tftp
firewall-cmd --add-service=tftp --per

# 检查端口
ss -tunlp | grep 69

# 安装 syslinux
yum install syslinux -y

# 拷贝 seslinux 文件到 tftp
cp -r /usr/share/syslinux/* /var/lib/tftpboot/

# 创建 pxelinux.cfg
mkdir /var/lib/tftpboot/pxelinux.cfg
cat > /var/lib/tftpboot/pxelinux.cfg/default << END
default menu.c32
timeout 6000
label CentOS 7 network install
    kernel vmlinuz
    append initrd=initrd.img method=http://10.173.28.219/web/cdrom/CentOS/7 ks=http://10.173.28.219/web/ks7.cfg

label AlmaLinux 8 network install
    menu default
    kernel vmlinuz
    append initrd=initrd.img method=http://10.173.28.219/web/cdrom/AlmaLinux/8 ks=http://10.173.28.219/web/ks8.cfg
END
```

## 搭建 dhcp 服务器

```bash
# 安装 dhcp
yum -y install dhcp-server

# 生成配置文件
cat > /etc/dhcp/dhcpd.conf << END
subnet 10.173.61.0 netmask 255.255.255.0 {
    range 10.173.61.10 10.173.61.200;
    option routers 10.173.61.1;
    next-server 10.173.61.254;
    filename "pxelinux.0";
}
END

# 启动服务
systemctl enable dhcpd --now

# 放行防火墙
firewall-cmd --add-service=dhcp
firewall-cmd --add-service=dhcp --per
```

## 搭建 nginx 服务

```bash
# 安装 nginx
yum -y install nginx

# 创建挂载点
mkdir -p /cdrom/AlmaLinux/8
mount /dev/sr0 /cdrom/AlmaLinux/8

# 关闭 selinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
setenforce 0

# 配置 nginx
cat > "END" << /etc/nginx/conf.d/default.conf
server {
    listen       80;
    server_name  localhost;

    location /cdrom {
        alias   /cdrom;
        autoindex on;
    }
}
END

# 拷贝 vmlinuz,initrd.img 到 /var/lib/tftpboot
cp /cdrom/AlmaLinux/8/isolinux/{vmlinuz,initrd.img} /var/lib/tftpboot/

# 放行防火墙
firewall-cmd --add-port=80/tcp
firewall-cmd --add-port=80/tcp --per

# 启动 nginx
systemctl --now enable nginx.service
```

## KS 配置

### KS 文件生成

[https://access.redhat.com/labs/kickstartconfig/](https://access.redhat.com/labs/kickstartconfig/)

#### 生成截图

![image-20230814171638492](http://typora.hflxhn.com/documents/2023/0814/image-20230814171638492.png)

![image-20230814171744072](http://typora.hflxhn.com/documents/2023/0814/image-20230814171744072.png)

![image-20230814171756654](http://typora.hflxhn.com/documents/2023/0814/image-20230814171756654.png)

![image-20230814171807917](http://typora.hflxhn.com/documents/2023/0814/image-20230814171807917.png)

![image-20230814171835834](http://typora.hflxhn.com/documents/2023/0814/image-20230814171835834.png)

![image-20230814171851161](http://typora.hflxhn.com/documents/2023/0814/image-20230814171851161.png)

![image-20230814171911934](http://typora.hflxhn.com/documents/2023/0814/image-20230814171911934.png)

![image-20230814171925709](http://typora.hflxhn.com/documents/2023/0814/image-20230814171925709.png)

#### 生成结果

```bash
lang en_US
keyboard --xlayouts='us'
timezone Asia/Shanghai --utc
rootpw $2b$10$ksT7KdGzjQYeBhpK5SyJHOOPClO4XTOa1tydIbjYm/sdIXVBrsKSO --iscrypted
reboot
url --url=http://172.24.0.211/web/cdrom/AlmaLinux/8/
bootloader --append="rhgb quiet crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M"
zerombr
clearpart --all --initlabel
autopart
network --bootproto=dhcp
firstboot --disable
selinux --enforcing
firewall --enabled
%packages
@^minimal-environment
kexec-tools
%end
```

### ks8 cfg

```bash
lang en_US
keyboard --xlayouts='us'
timezone Asia/Shanghai
rootpw $2b$10$nBDA5hypKxx8AhvuTSRcFu768opLe6Aav0rmSqdA5dmeOPovo1Wki --iscrypted
reboot
url --url=http://172.24.0.248/web/cdrom/AlmaLinux/8/
bootloader --append="rhgb quiet crashkernel=1G-4G:192M,4G-64G:256M,64G-:512M"
zerombr
clearpart --all --initlabel
autopart
network --bootproto=dhcp
firstboot --disable
selinux --enforcing
firewall --enabled
%post
mkdir -p /root/.ssh
echo "" >> /root/.ssh/authorized_keys
chmod 700 /root/.ssh && chmod 600 /root/.ssh/authorized_keys
mkdir /etc/yum.repos.d/bak && mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak
cat > /etc/yum.repos.d/iso.repo << 'END'
[BaseOS]
baseurl = http://172.24.0.248/web/cdrom/AlmaLinux/8/BaseOS/
enabled = 1
gpgcheck = 0
name = BaseOS

[AppStream]
baseurl = http://172.24.0.248/web/cdrom/AlmaLinux/8/AppStream/
enabled = 1
gpgcheck = 0
name = AppStream
END
yum install bash-completion wget -y
%end
%packages
@^minimal-environment
kexec-tools
%end
```

### ks7 cfg

```bash
lang en_US
keyboard us
timezone Asia/Shanghai
rootpw $6$A1xmlRDU$BCaFmJtvBDZUD8C0qnaSVAGOMChcKNAdvmzRsjFHGOteIDKaWDTBt7hZiOlkfAy6hLv5el2k2nXEbRA8zTk1L1 --iscrypted
reboot
url --url=http://172.24.0.248/web/cdrom/CentOS/7/
bootloader --append="rhgb crashkernel=auto console=tty0 console=ttyS0,115200n8"
zerombr
clearpart --all --initlabel
autopart
auth --passalgo=sha512 --useshadow
network --bootproto=dhcp
firstboot --disable
selinux --enforcing
firewall --enabled
%post
mkdir -p /root/.ssh
echo "" >> /root/.ssh/authorized_keys
chmod 700 /root/.ssh && chmod 600 /root/.ssh/authorized_keys
sed -i 's/#UseDNS yes/UseDNS no/' /etc/ssh/sshd_config && systemctl restart sshd.service
mkdir /etc/yum.repos.d/bak && mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak
cat > /etc/yum.repos.d/iso.repo << 'END'
[base]
name=base
gpgcheck=0
baseurl=http://172.24.0.248/web/cdrom/CentOS/7/
END
yum install bash-completion wget -y
%end
%packages
@^minimal
kexec-tools
%end
```
