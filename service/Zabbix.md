# zabbix 6.x 企业级开源解决方案

## 部署 zabbix 6.x

### 1. 官网手册

```bash
https://www.zabbix.com/cn/download?zabbix=6.4&os_distribution=alma_linux&os_version=8&components=server_frontend_agent&db=mysql&ws=nginx
```

### 2. 修改主机名

```bash
hostnamectl set-hostname zabbix-server
```

### 3. 安装 zabbix server

```bash
# Install Zabbix repository
rpm -Uvh https://repo.zabbix.com/zabbix/6.4/rhel/8/x86_64/zabbix-release-6.4-1.el8.noarch.rpm
yum clean all
yum makecache

# Switch DNF module version for PHP
yum module -y switch-to php:7.4

# Install Zabbix server, frontend, agent
yum install -y zabbix-server-mysql zabbix-web-mysql zabbix-nginx-conf zabbix-sql-scripts zabbix-selinux-policy zabbix-agent
```

### 4. MySQL 服务安装

```bash
yum -y install mysql-server
systemctl enable mysqld.service --now
grep "password" /var/log/mysql/mysqld.log
mysql -uroot -e "alter user 'root'@'localhost' identified by 'qwer4321'; flush privileges;"
```

### 5. 初始化数据库

```mysql
# Run the following on your database host.
mysql -uroot -p'qwer4321' -e "create database zabbix character set utf8mb4 collate utf8mb4_bin;"
mysql -uroot -p'qwer4321' -e "create user zabbix@localhost identified by 'password';"
mysql -uroot -p'qwer4321' -e "grant all privileges on zabbix.* to zabbix@localhost;"
mysql -uroot -p'qwer4321' -e "set global log_bin_trust_function_creators = 1;"
mysql -uroot -p'qwer4321' -e "show databases;"

# On Zabbix server host import initial schema and data. You will be prompted to enter your newly created password.
zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql --default-character-set=utf8mb4 -uzabbix -p'password' zabbix

# Disable log_bin_trust_function_creators option after importing database schema.
mysql -uroot -p'qwer4321' -e "set global log_bin_trust_function_creators = 0;"
```

### 6. 配置 zabbix

```bash
cp -r /etc/zabbix/zabbix_server.conf{,.bak}
egrep -v '^#|^$' /etc/zabbix/zabbix_server.conf

cat > /etc/zabbix/zabbix_server.conf << END
LogFile=/var/log/zabbix/zabbix_server.log
LogFileSize=0
PidFile=/run/zabbix/zabbix_server.pid
SocketDir=/run/zabbix
DBHost=localhost
DBName=zabbix
DBUser=zabbix
DBPassword=password
SNMPTrapperFile=/var/log/snmptrap/snmptrap.log
Timeout=4
LogSlowQueries=3000
StatsAllowedIP=127.0.0.1
END

echo 'php_value[date.timezone] = Asia/Shanghai' >> /etc/php-fpm.d/zabbix.conf
sed -i 's/^#.*listen.*/        listen          80;/' /etc/nginx/conf.d/zabbix.conf
sed -i 's/^.*server_name.*/        server_name     localhost;/' /etc/nginx/conf.d/zabbix.conf
```

### 7. 启动并设置自启

```bash
systemctl restart zabbix-server zabbix-agent nginx php-fpm
systemctl enable zabbix-server zabbix-agent nginx php-fpm
```

### 8. 防火墙放行

```bash
firewall-cmd --add-port=80/tcp --per
firewall-cmd --add-port=80/tcp
```

### 9. 测试

```bash
yum install zabbix-get -y
zabbix_get -s '127.0.0.1' -p 10050 -k 'system.hostname'
```

### 10. 浏览器初始化

#### 10.1 打开网页

```bash
http://localhost/setup.php
```

![image-20230927162351412](http://typora.hflxhn.com/documents/2023/0927/image-20230927162351412.png)

#### 10.2 安装汉语包

```bash
# 安装语言包
yum install langpacks-zh_CN -y
```

![image-20230927162509615](http://typora.hflxhn.com/documents/2023/0927/image-20230927162509615.png)

#### 10.3 配置数据库

![image-20230927162538951](http://typora.hflxhn.com/documents/2023/0927/image-20230927162538951.png)

#### 10.4 配置主机名

![image-20230927162609499](http://typora.hflxhn.com/documents/2023/0927/image-20230927162609499.png)

![image-20230927162623815](http://typora.hflxhn.com/documents/2023/0927/image-20230927162623815.png)

#### 10.5 完成初始化

![image-20230927162635861](http://typora.hflxhn.com/documents/2023/0927/image-20230927162635861.png)

#### 10.6 登陆系统

```bash
# 默认用户名和密码
Admin/zabbix
```

![image-20230927162653855](http://typora.hflxhn.com/documents/2023/0927/image-20230927162653855.png)

#### 10.7 zabbix 首页

```bash
# 安装完成
```

![image-20230927162815108](http://typora.hflxhn.com/documents/2023/0927/image-20230927162815108.png)

## 部署 zabbix agent

### 1. 官网手册

```bash
https://www.zabbix.com/cn/download?zabbix=6.4&os_distribution=alma_linux&os_version=8&components=agent_2&db=&ws=
```

### 2. 安装 zabbix agent 2

```bash
# rhel 8 配置 yum 源
rpm -Uvh https://repo.zabbix.com/zabbix/6.4/rhel/8/x86_64/zabbix-release-6.4-1.el8.noarch.rpm

# rhel 7 配置 yum 源
rpm -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/7/x86_64/zabbix-release-6.0-4.el7.noarch.rpm

yum clean all

# 安装 zabbix agent2
yum install zabbix-agent2 zabbix-agent2-plugin-* -y
```

### 3. 配置 zabbix agent 2

```bash
# default_net_card=$(ip route show default | awk 'NR==1' | awk '/default/ {print $5}')
# local_address=$(ip a show ${default_net_card} | grep -m 1 inet | awk '{print $2}' | awk -F"/" '{print $1}')

fqdb=$(hostname -f)
zabbix_server=10.173.28.220
sed -i "s/^Server=.*/Server=${zabbix_server}/" /etc/zabbix/zabbix_agent2.conf && grep "^Server=" /etc/zabbix/zabbix_agent2.conf
sed -i "s/^ServerActive=.*/ServerActive=${zabbix_server}/" /etc/zabbix/zabbix_agent2.conf && grep "^ServerActive=" /etc/zabbix/zabbix_agent2.conf
sed -i "s/^Hostname=.*/Hostname=${fqdb}/" /etc/zabbix/zabbix_agent2.conf && grep "^Hostname=" /etc/zabbix/zabbix_agent2.conf
```

### 4. 防火墙放行

```bash
firewall-cmd --add-port=10050/tcp
firewall-cmd --add-port=10050/tcp --per
```

### 5. 服务启动

```bash
systemctl enable zabbix-agent2.service --now
```

## zabbix 模板配置

### 1. 主机群组

```bash
# 配置 > 主机群组 > 创建主机群组
```

![image-20231024100031325](http://typora.hflxhn.com/documents/2023/1024/image-20231024100031325.png)

```bash
# 添加物理机群组
# PhysicalMachine
```

![image-20231024102924191](http://typora.hflxhn.com/documents/2023/1024/image-20231024102924191.png)

### 2. 模板

#### 2.1 创建模板

```bash
# 配置 > 模板 > 创建模板
```

![image-20231024101234624](http://typora.hflxhn.com/documents/2023/1024/image-20231024101234624.png)

```bash
# 模板名称: PhysicalMachine
# 群组: PhysicalMachine
# 描述: 安装了 zabbix-agent 的物理主机
```

![image-20231024103000149](http://typora.hflxhn.com/documents/2023/1024/image-20231024103000149.png)

#### 2.2 模板 > 检测项

```bash
# 配置 > 模板 > 检测项 > 创建检测项
```

![image-20231024104850365](http://typora.hflxhn.com/documents/2023/1024/image-20231024104850365.png)

![image-20231024105011031](http://typora.hflxhn.com/documents/2023/1024/image-20231024105011031.png)

```bash
# 名称: 节点过去1分钟cpu的平均负载 (名称尽可能的见字而知其意)
# 键值: system.cpu.load[all,avg1]
# 信息类型: 浮点类型
# 更新间隔: 1m (单位有s,m,h,d,表示秒,分钟,小时,天)
# 其他保持默认
```

![image-20231024105754076](http://typora.hflxhn.com/documents/2023/1024/image-20231024105754076.png)

#### 2.3 模板 > 图形

```bash
# 配置 > 模板 > 图形 > 创建图形
```

![image-20231024105938395](http://typora.hflxhn.com/documents/2023/1024/image-20231024105938395.png)

```bash
# 配置 -> 模板 -> 图形 -> 创建图形
# 名称: 节点过去1分钟cpu的平均负载展示图 (名称尽可能的见字而知其意)
# 纵轴Y最小值MIN, 纵轴最大值: 
#     一般在生产环境都会设置固定的
#     固定值都在能容忍的最大值之上一点点
#     比如cpu负载最大容忍为2,那么纵轴的固定值应该比2高一点点
#     然后我们会设置两个触发器
#     比如值超过1了，我们会通过邮件报警
#     比如值超过1.5了，我们会设置微信或在钉钉报警
#     比如值超过2了，我们会设置短信或者电话报警
```

![image-20231024110212265](http://typora.hflxhn.com/documents/2023/1024/image-20231024110212265.png)

```bash
# 图形乱码问题解决
# 在 windowns 找一个中文字体
mv /usr/share/fonts/dejavu/DejaVuSans.ttf{,.bak}
wget http://typora.hflxhn.com/software/fonts/simhei.ttf
mv simhei.ttf /usr/share/fonts/dejavu/DejaVuSans.ttf
```

#### 2.4 模板 > 触发器

```bash
# 配置 -> 模板 -> 触发器 -> 创建触发器
# 名称: 主机节点cpu过去一分钟的平均负载超过0.8 (名称尽可能的见字而知其意)
# 表达式: {zabbix-server:system.cpu.load[all,avg1].last(#1,20)}>0.8
#        zabbix-server 表示主机
#        system.cpu.load[all,avg1]表示键值
#        last(#1,20)表示last函数,最后的1个值,持续20s
#        >0.8表示last函数的取值如果大于0.8,那么触发器就生效.
```

### 3. 主机

```bash
# 配置 > 主机 > 创建主机
```

![image-20231024103344914](http://typora.hflxhn.com/documents/2023/1024/image-20231024103344914.png)

## zabbix 监控 snmp

### 1. 交换机或者系统配置 snmp 服务

```bash
# 安装软件
yum install net-snmp net-snmp-utils -y

# 备份配置文件
ls /etc/snmp/snmpd.conf.bak &>/dev/null || cp -r /etc/snmp/snmpd.conf{,.bak}

# 启动服务
systemctl enable snmpd.service --now

#  测试 snmp 服务
snmpwalk -v2c -c public localhost system

# 防火墙放行
firewall-cmd --add-port=161/udp
firewall-cmd --add-port=161/udp --per
```

### 2. zabbix 添加 snmp 设备

```bash
# 配置 > 主机 > 创建主机
# 主机名称: switch-10.173.26.1
# 模板: Cisco IOS by SNMP
# 群组: switch
# 接口: 10.173.26.1
#       SNMP v3
#       allbest
```

![image-20231026095338623](http://typora.hflxhn.com/documents/2023/1026/image-20231026095338623.png)

## zabbix 监控 ipmi

### 1. 开启华为服务器的 ipmi

```bash
https://172.24.99.254/index.php
```

![image-20231025173626167](http://typora.hflxhn.com/documents/2023/1025/image-20231025173626167.png)

### 2. zabbix server 开启 ipmi

```bash
grep '^StartIPMIPollers' /etc/zabbix/zabbix_server.conf || sed -i '$a StartIPMIPollers=5' /etc/zabbix/zabbix_server.conf
systemctl restart zabbix-server.service
```

### 3. zabbix web 添加 ipmi

```bash
# 主机 > 创建主机 > 模板 > 接口
```

![image-20231025174318843](http://typora.hflxhn.com/documents/2023/1025/image-20231025174318843.png)

```bash
# 主机 > 创建主机 > IPMI
```

![image-20231025174506834](http://typora.hflxhn.com/documents/2023/1025/image-20231025174506834.png)

## zabbix 监控 esxi

### 1. esxi 配置

```bash
# 选择 ESXi 主机 > 配置 > 高级系统配置 > Config.HostAgent.plugins.solo.enableMob
# 将 false 改为 true
```

![image-20231024142134993](http://typora.hflxhn.com/documents/2023/1024/image-20231024142134993.png)

### 2. 修改 zabbix server 配置文件

```bash
StartVMwareCollectors=6     # 监控esxi的实例，一般写5就好
VMwareCacheSize=50M         # 从单个VMware服务收集数据之间的延迟(秒)
VMwareFrequency=10          # 用于存储VMware数据的共享内存大小
VMwarePerfFrequency=60      # 从单个VMware服务检索性能计数器统计信息之间的延迟(秒)
VMwareTimeout=300           # 响应超时时间

grep '^StartVMwareCollectors' /etc/zabbix/zabbix_server.conf || sed -i '$a StartVMwareCollectors=6' /etc/zabbix/zabbix_server.conf
grep '^VMwareCacheSize' /etc/zabbix/zabbix_server.conf || sed -i '$a VMwareCacheSize=50M' /etc/zabbix/zabbix_server.conf
grep '^VMwareFrequency' /etc/zabbix/zabbix_server.conf || sed -i '$a VMwareFrequency=10' /etc/zabbix/zabbix_server.conf
grep '^VMwarePerfFrequency' /etc/zabbix/zabbix_server.conf || sed -i '$a VMwarePerfFrequency=60' /etc/zabbix/zabbix_server.conf
grep '^VMwareTimeout' /etc/zabbix/zabbix_server.conf || sed -i '$a VMwareTimeout=300' /etc/zabbix/zabbix_server.conf
```

### 3. 重启 zabbix 服务并查看日志

```bash
systemctl restart zabbix-server.service
tail -f -n500 /var/log/zabbix/zabbix_server.log | grep 'VMware monitoring'
```

![image-20231024144134829](http://typora.hflxhn.com/documents/2023/1024/image-20231024144134829.png)

### 4. zabbix web 添加 esxi 主机

```bash
# 配置 > 主机 > 创建主机
https://10.173.28.254/mob/?moid=ha-host&doPath=hardware.systemInfo

# vSphere 查看 UUID
https://10.172.23.52/mob/?moid=ServiceInstance&doPath=content.about
```

![image-20231024144935083](http://typora.hflxhn.com/documents/2023/1024/image-20231024144935083.png)

![image-20231024151626832](http://typora.hflxhn.com/documents/2023/1024/image-20231024151626832.png)

```bash
# 设置宏
{$VMWARE.HV.UUID}
{$VMWARE.URL}
{$VMWARE.USERNAME}
{$VMWARE.PASSWORD}
```

![image-20231024151439903](http://typora.hflxhn.com/documents/2023/1024/image-20231024151439903.png)

## zabbix api 接口

### 1. shell 前置操作

```bash
# 配置 hosts 解析
grep zabbix.local.com /etc/hosts || echo "10.173.28.224 zabbix.local.com" >> /etc/hosts

# 安装 jq
yum install jq -y

# 生成 post 函数
function curlPost() {
    local url=http://zabbix.local.com/zabbix/api_jsonrpc.php
    local data=${1}

    resturl=$(curl -sS -X POST "${url}" -H "Content-Type: application/json-rpc" -d "${data}")
    echo ${resturl} | jq '.'
}
```

### 2. 版本

```json
curlPost '{
  "jsonrpc": "2.0",
  "method": "apiinfo.version",
  "id": 1,
  "auth": null,
  "params": {}
}'
```

### 3. 用户

#### 3.1 单用户认证

```json
curlPost '{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "Admin",
        "password": "zabbix"
    },
    "id": 1,
    "auth": null
}'
```

### 4. 主机组

#### 4.1 获取

```bash
curlPost '{
    "jsonrpc": "2.0",
    "method": "hostgroup.get",
    "params": {
        "output": "extend",
        "filter": {
            "name": [
                "虚拟主机群组"
            ]
        }
    },
    "auth": "e022f9c13969f93a50b0909531765f9b",
    "id": 1
}'
```

### 5. 主机

#### 5.1 创建

```bash
curlPost '{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "dhcp-server",
        "name": "virtual-ops-dhcp-server",
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "10.173.28.229",
                "dns": "",
                "port": "10050"
            }
        ],
        "groups": [
            {
                "groupid": "29"
            }
        ]
    },
    "auth": "e022f9c13969f93a50b0909531765f9b",
    "id": 1
}'
```

#### 5.2 删除

```bash
curlPost '{
    "jsonrpc": "2.0",
    "method": "host.delete",
    "params": [
        "10684"
    ],
    "auth": "e022f9c13969f93a50b0909531765f9b",
    "id": 1
}'
```

