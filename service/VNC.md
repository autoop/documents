# VNC (Virtual Network Computing)

## rhel 部署 VNC

### 1. vnc server

```bash
# 安装 tigervnc-server
yum install tigervnc-server -y

# 配置 vnc 用户
## :[display_number]=[user]
cat >> /etc/tigervnc/vncserver.users << END
:20=vnc
END

# 设置普通用户的 vnc 密码
useradd vnc
echo 123 | passwd --stdin vnc
su - vnc
echo session=gnome >> .vnc/config
vncpasswd

# 启动 vnc 服务
# systemctl start vncserver@:[display_number]
systemctl enable vncserver@:20 --now

# 防火墙放行
firewall-cmd --add-port=5920/tcp
firewall-cmd --add-port=5920/tcp --per

# vnc 日志
tail -f /home/vnc/.vnc/localhost.localdomain\:20.log
```

### 2. vnc client

#### 2.1 ssh 连接

```bash
# ssh [user]@[server] -L 59[display_number]:localhost:59[display_number]
ssh vnc@[server] -L 5920:localhost:5920

# vncviewer localhost:[display_number]
vncviewer localhost:20
```

#### 2.2 putty 连接

![image-20230922114419380](http://typora.hflxhn.com/documents/2023/0922/image-20230922114419380.png)

#### 2.3 xshell 连接

![image-20230922113600347](http://typora.hflxhn.com/documents/2023/0922/image-20230922113600347.png)

## ubuntu 部署 VNC

### 1. vnc server

```bash
# 安装 tigervnc-server
sudo apt install tigervnc-standalone-server tigervnc-common -y

# 设置普通用户的 vnc 密码
useradd vnc
echo 123 | passwd --stdin vnc
su - vnc

cat > ~/.vnc/xstartup << 'END'
#!/bin/sh
# Start up the standard system desktop
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS

/usr/bin/gnome-session
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
x-window-manager &
END

chmod +x ~/.vnc/xstartup

vncpasswd

# 启动 vnc
vncserver :30 -localhost no

# 配置 systemctl 系统服务
cat > /etc/systemd/system/vncserver@.service << 'END'
[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target
[Service]
Type=simple
User=vnc
PAMName=login
PIDFile=/home/%u/.vnc/%H%i.pid
ExecStartPre=/bin/sh -c '/usr/bin/vncserver -kill :%i > /dev/null 2>&1 || :'
ExecStart=/usr/bin/vncserver :%i -alwaysshared -fg -localhost no
ExecStop=/usr/bin/vncserver -kill :%i
[Install]
WantedBy=multi-user.target
END

sudo systemctl enable vncserver@30.service --now
```