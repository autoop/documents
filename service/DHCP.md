# DHCP 服务安装及配置

```bash
# 安装
yum install dhcp-server -y

# 配置
cp /usr/share/doc/dhcp-server/dhcpd.conf.example /etc/dhcp/dhcpd.conf
cat > /etc/dhcp/dhcpd.conf << 'END'
default-lease-time 600;
max-lease-time 7200;

subnet 10.173.28.0 netmask 255.255.255.0 {
    range 10.173.28.10 10.173.28.15;
    option routers 10.173.28.1;
    option domain-name-servers 114.114.114.114;
}
END

# 启动服务并设置开机自启
systemctl enable dhcpd.service --now
```

