# frp 内网穿透

## frp 服务端配置

```bash
# 获取软件包
https://github.com/fatedier/frp/releases

# 解压软件
tar xf frp_0.51.0_linux_amd64.tar.gz -C /opt
mv /opt/frp_0.51.0_linux_amd64/ /opt/frp

# 配置服务端
cat > /opt/frp/frps.ini << END
[common]
bind_port = 65500
authentication_method = token
token = $(cat /proc/sys/kernel/random/uuid)
log_file = /opt/frp/frps.log
END

# 加入系统服务
cat > /etc/systemd/system/frps.service << 'END'
[Unit]
Description = frps server
After = network.target syslog.target
Wants = network.target

[Service]
Type = simple
ExecStart = /opt/frp/frps -c /opt/frp/frps.ini

[Install]
WantedBy = multi-user.target
END
systemctl enable frps.service --now

# 查看服务是否启动
ss -tunlp | grep 65500
systemctl enable frps.service --now
tail -f /opt/frp/frps.log
```

## frp 客户端

```bash
# 解压软件
tar xf frp_0.51.0_linux_amd64.tar.gz -C /opt
mv /opt/frp_0.51.0_linux_amd64/ /opt/frp

# 配置客户端
cat > /opt/frp/frpc.ini << END
[common]
server_addr = frp.dmxy.com
bind_port = 65500
authentication_method = token
token = db8fd2c4-3058-41ab-9711-8d1e32eca101
log_file = /opt/frp/frpc.log
END

# 加入系统服务
cat > /etc/systemd/system/frpc.service << 'END'
[Unit]
Description = frpc server
After = network.target syslog.target
Wants = network.target

[Service]
Type = simple
ExecStart = /opt/frp/frpc -c /opt/frp/frpc.ini

[Install]
WantedBy = multi-user.target
END
systemctl enable frpc.service --now

# 查看服务是否启动
ss -tunlp
```

