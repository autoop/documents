# Proxy Server 

## 1. Serer 部署

```bash
# 下载并解压
wget https://fanyang-1300296837.cos.ap-chengdu.myqcloud.com/software/v2ray/v2ray.tar.gz
tar xf v2ray.tar.gz -C /opt

# 生成配置文件
cat > /opt/v2ray/conf/config.json << 'END'
{
  "log": {
    "access": "/opt/v2ray/logs/access.log",
    "error": "/opt/v2ray/logs/error.log",
    "loglevel": "debug"
  },
  "inbound": {
    "port": 11234,
    "listen":"127.0.0.1",
    "protocol": "vmess",
    "settings": {
      "clients": [
        {
          "id": "v2uuid",
          "level": 1,
          "alterId": 64,
          "email": "v2email"
        }
      ]
    },
     "streamSettings": {
      "network": "ws",
      "wsSettings": {
         "path": "/newpath"
        }
     }
  },
  "outbounds": [
    {
      "protocol": "freedom",
      "settings": {}
    }
  ]
}
END

uuid=$(cat /proc/sys/kernel/random/uuid)
newpath=$(cat /dev/urandom | head -1 | md5sum | head -c 4)
v2email=dmxy@163.com

mkdir -p /opt/nginx-1.22.0/html/
cat > /opt/nginx-1.22.0/html/v2ray.html << END
<meta charset="UTF-8">
<title>config</title>
<ul>
    <li>地址 : agent.dmxy.com</li>
    <li>端口 : 443</li>
    <li>uuid : ${uuid}</li>
    <li>额外id : 64</li>
    <li>加密方式 : aes-128-gcm</li>
    <li>传输协议 : ws</li>
    <li>别名 : myws</li>
    <li>路径 : /${newpath}</li>
    <li>底层传输 : tls</li>
</ul>
END

sed -i "s/v2uuid/${uuid}/" /opt/v2ray/conf/config.json
sed -i "s/newpath/${newpath}/" /opt/v2ray/conf/config.json
sed -i "s/v2email/${v2email}/" /opt/v2ray/conf/config.json

cat /opt/v2ray/conf/config.json

# 修改目录权限
chown nobody:nobody -R /opt/v2ray

# 创建 server 文件
cat > /usr/lib/systemd/system/v2ray.service << 'END'
[Unit]
Description=V2Ray Service
Documentation=https://www.v2fly.org/
After=network.target nss-lookup.target

[Service]
Environment="V2RAY_VMESS_AEAD_FORCED=false"
User=nobody
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
NoNewPrivileges=true
ExecStart=/opt/v2ray/bin/v2ray run -config /opt/v2ray/conf/config.json
Restart=on-failure
RestartPreventExitStatus=23

[Install]
WantedBy=multi-user.target
END

# 启动并设置自启
systemctl enable v2ray.service --now
```

## 2. 配置 v2ray 客户端

### win 客户端

```json
{
    ===========配置参数=============
    地址：agent.dmxy.com
    端口：443
    uuid：0612d1ce-160a-4c3c-95aa-375f8558de21
    额外id：64
    加密方式：aes-128-gcm
    传输协议：ws
    别名：myws
    路径：3ac3
    底层传输：tls
}
```

![image.png](http://typora.hflxhn.com/documents/2023/0714/1682319258660-dcc93fd6-8732-4d89-a4b7-00db118ce2ad.png)

### linux 客户端

```bash
{
  "policy": null,
  "log": {
    "access": "/opt/v2ray/logs/access.log",
    "error": "/opt/v2ray/logs/error.log",
    "loglevel": "error"
  },
  "inbounds": [
    {
      "tag": "proxy",
      "port": 10808,
      "listen": "127.0.0.1",
      "protocol": "socks",
      "sniffing": {
        "enabled": true,
        "destOverride": [
          "http",
          "tls"
        ]
      },
      "settings": {
        "auth": "noauth",
        "udp": true,
        "ip": null,
        "address": null,
        "clients": null
      },
      "streamSettings": null
    }
  ],
  "outbounds": [
    {
      "tag": "proxy",
      "protocol": "vmess",
      "settings": {
        "vnext": [
          {
            "address": "agent.dmxy.com",
            "port": 443,
            "users": [
              {
                "id": "111be39c-480a-4b00-a518-9b07b57c6ccc",
                "alterId": 64,
                "email": "t@t.tt",
                "security": "aes-128-gcm"
              }
            ]
          }
        ],
        "servers": null,
        "response": null
      },
      "streamSettings": {
        "network": "ws",
        "security": "tls",
        "tlsSettings": {
          "allowInsecure": true,
          "serverName": null
        },
        "tcpSettings": null,
        "kcpSettings": null,
        "wsSettings": {
          "connectionReuse": true,
          "path": "/d177",
          "headers": null
        },
        "httpSettings": null,
        "quicSettings": null
      },
      "mux": {
        "enabled": true,
        "concurrency": 8
      }
    },
    {
      "tag": "direct",
      "protocol": "freedom",
      "settings": {
        "vnext": null,
        "servers": null,
        "response": null
      },
      "streamSettings": null,
      "mux": null
    },
    {
      "tag": "block",
      "protocol": "blackhole",
      "settings": {
        "vnext": null,
        "servers": null,
        "response": {
          "type": "http"
        }
      },
      "streamSettings": null,
      "mux": null
    }
  ],
  "stats": null,
  "api": null,
  "dns": null,
  "routing": {
    "domainStrategy": "IPIfNonMatch",
    "rules": [
      {
        "type": "field",
        "port": null,
        "inboundTag": [
          "api"
        ],
        "outboundTag": "api",
        "ip": null,
        "domain": null
      }
    ]
  }
}
```

### privoxy

```bash
# 将 socks5 代理转换为 http 代理
yum install epel-release
yum install privoxy -y

sed -i 's/^listen-address.*/listen-address 0.0.0.0:8118/' /etc/privoxy/config
sed -i '/^listen-address.*/a forward-socks5t / 192.168.0.100:10808 .' /etc/privoxy/config
egrep -A1 '^listen-address' /etc/privoxy/config

systemctl enable privoxy.service --now
```

```bash
cat >> /etc/profile << 'END'
PROXY_HOST=127.0.0.1
export all_proxy=http://$PROXY_HOST:8118
export ftp_proxy=http://$PROXY_HOST:8118
export http_proxy=http://$PROXY_HOST:8118
export https_proxy=http://$PROXY_HOST:8118
END
```

### 绕过中国大陆的 ip

```bash
curl -4sSkLO https://raw.github.com/zfl9/gfwlist2privoxy/master/gfwlist2privoxy
bash gfwlist2privoxy 127.0.0.1:10808
mv -f gfwlist.action /etc/privoxy/
echo 'actionsfile gfwlist.action' >>/etc/privoxy/config
restorecon -RvF /etc/privoxy/
systemctl restart privoxy.service
```

## 3. nginx 转发配置

```bash
# 安装 nginx
wget https://www.openssl.org/source/old/1.1.1/openssl-1.1.1a.tar.gz
wget https://nginx.org/download/nginx-1.22.0.tar.gz
yum install -y pcre-devel zlib-devel openssl-devel
useradd -M -s /sbin/nologin nginx
tar xf openssl-1.1.1a.tar.gz
tar xf nginx-1.22.0.tar.gz && cd nginx-1.22.0

./configure --prefix=/opt/nginx-1.22.0 \
--user=nginx --group=nginx \
--with-http_stub_status_module \
--with-http_ssl_module \
--with-http_random_index_module \
--with-http_sub_module \
--with-http_gzip_static_module \
--with-pcre \
--with-openssl=../openssl-1.1.1a \
--with-openssl-opt='enable-tls1_3' \
--with-http_v2_module \
--with-stream \
--with-stream_ssl_module

make -j12 && make install

chown nginx:nginx -R /opt/nginx-1.22.0

# 防火墙放行
firewall-cmd --permanent --add-port=443/tcp
firewall-cmd --reload

# 生成 server 文件
cat > /lib/systemd/system/nginx.service << END
[Unit]
Description=nginx service
After=network.target network-online.target syslog.target
Wants=network.target network-online.target

[Service]
Type=forking
ExecStart=/opt/nginx-1.22.0/sbin/nginx
ExecReload=/opt/nginx-1.22.0/sbin/nginx -s reload
ExecStop=/opt/nginx-1.22.0/sbin/nginx -s stop

[Install]
WantedBy=multi-user.target
END

systemctl enable nginx.service --now

# 配置 nginx 服务
cat > /opt/nginx-1.22.0/conf/nginx.conf << 'END'
user nginx nginx;
worker_processes 1;
error_log logs/error.log;
pid logs/nginx.pid;

events {
    worker_connections 1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile      on;
    keepalive_timeout  65;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    include conf.d/*.conf;
}
END

mkdir -p /opt/nginx-1.22.0/conf/ssl
mkdir -p /opt/nginx-1.22.0/conf/conf.d
cat > /opt/nginx-1.22.0/conf/conf.d/v2ray.conf << 'END'
server {
    listen      443 ssl http2;
    server_name agent.dmxy.com;
    root        html;
    index       index.php index.html;
    ssl_certificate ssl/agent.dmxy.com.pem;
    ssl_certificate_key ssl/agent.dmxy.com.key;

    ssl_protocols   TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_ciphers     'TLS13-AES-256-GCM-SHA384:TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-GCM-SHA256:TLS13-AES-128-CCM-8-SHA256:TLS13-AES-128-CCM-SHA256:EECDH+CHACHA20:EECDH+CHACHA20-draft:EECDH+ECDSA+AES128:EECDH+aRSA+AES128:RSA+AES128:EECDH+ECDSA+AES256:EECDH+aRSA+AES256:RSA+AES256:EECDH+ECDSA+3DES:EECDH+aRSA+3DES:RSA+3DES:!MD5';
    ssl_prefer_server_ciphers   on;
    ssl_early_data  on;
    ssl_stapling on;
    ssl_stapling_verify on;
    location /${newpath} {
        proxy_redirect off;
        proxy_pass http://127.0.0.1:11234; 
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $http_host;
    }
}
END

# 配置 ssl 证书
scp * 45.77.133.24:/opt/nginx-1.22.0/conf/ssl/

# 重启服务
chown nginx:nginx -R /opt/nginx-1.22.0
systemctl restart nginx.service
```

## 4. 开启 bbr 加速

```bash
# BBR是 Google 提出的一种新型拥塞控制算法，可以使 Linux 服务器显著地提高吞吐量和减少 TCP 连接的延迟。
# TCP BBR是由来自Google的 Neal Cardwell 和 Yuchung Cheng 发表的新的TCP拥塞控制算法，其目的就是要尽量跑满带宽，并且尽量不要有排队的情况。
# 目前已经在Google内部大范围使用，并且随着Linux 4.9版本正式发布。

# 检查内核版本
# Linux内核4.9版本已经内置BBR，因此首先检查内核版本。
uname -r

# 检查是否启用了BBR？
sysctl net.ipv4.tcp_available_congestion_control
sysctl net.core.default_qdisc

# 返回结果中没有BBR，说明没有启用。

# 开启BBR
echo "net.ipv4.tcp_congestion_control= bbr" >> /etc/sysctl.conf
echo "net.core.default_qdisc = fq" >> /etc/sysctl.conf
sysctl -p

# 现在内核中已经BBR功能，检测一下是否正在运行。
lsmod | grep bbr

# 看到 tcp_bbr 说明 BBR 已经成功在运行了。
```

