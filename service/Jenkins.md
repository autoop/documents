# Jenkins 持续集成工具

## 部署 Jenkins

### 1. 创建目录结构

```bash
mkdir -p /podman/podman-jenkins/var/jenkins_home
mkdir -p /podman/podman-jenkins/opt
```

### 2. 下载 maven

```bash
wget -c https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.6.3/apache-maven-3.6.3-bin.tar.gz
tar xf apache-maven-3.6.3-bin.tar.gz -C /podman/podman-jenkins/opt
```

### 3. 下载jdk

```bash
wget -c https://mirrors.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz
tar xf jdk-8u202-linux-x64.tar.gz -C /podman/podman-jenkins/opt
```

### 4. 下载 nodejs

```bash
wget -c https://nodejs.org/dist/v16.20.2/node-v16.20.2-linux-x64.tar.xz
tar xf node-v16.20.2-linux-x64.tar.xz -C /podman/podman-jenkins/opt
```

### 5. 拉起容器

```bash
podman run --privileged -d \
--name jenkins \
-p 8080:8080 -p 50000:50000 \
--tz Asia/Shanghai \
-v /podman/podman-jenkins/var/jenkins_home:/var/jenkins_home:Z \
-v /podman/podman-jenkins/opt/node-v16.20.2-linux-x64:/opt/node-v16.20.2-linux-x64:Z \
-v /podman/podman-jenkins/opt/apache-maven-3.6.3:/opt/apache-maven-3.6.3:Z \
-v /podman/podman-jenkins/opt/jdk1.8.0_202:/opt/jdk1.8.0_202:Z \
jenkins/jenkins:2.438
```

### 6. 生成 service 文件

```bash
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name jenkins
mv container-jenkins.service ~/.config/systemd/user
podman stop jenkins && podman rm jenkins
```

### 7. 设置开启启动容器

```bash
loginctl enable-linger
systemctl --user daemon-reload
systemctl --user enable container-jenkins.service --now
```

### 8. 放行防火墙

```bash
firewall-cmd --add-port=8080/tcp --add-port=50000/tcp
firewall-cmd --add-port=8080/tcp --add-port=50000/tcp --per
```

## 初始化 Jenkins

### 1. 浏览器访问 jenkins 服务

```bash
http://192.168.0.23:8080/login?from=%2F
```

![image-20231220115954098](http://typora.hflxhn.com/documents/2023/1220/image-20231220115954098.png)

### 2. 查看 jenkins 初始化密码

```bash
cat /var/jenkins_home/secrets/initialAdminPassword

# podman
cat /podman/podman-jenkins/var/jenkins_home/secrets/initialAdminPassword
```

![image-20231220120032241](http://typora.hflxhn.com/documents/2023/1220/image-20231220120032241.png)

![image-20231220122919010](http://typora.hflxhn.com/documents/2023/1220/image-20231220122919010.png)

### 3. 安装建议插件

![image-20231220123029552](http://typora.hflxhn.com/documents/2023/1220/image-20231220123029552.png)

![image-20231220123118182](http://typora.hflxhn.com/documents/2023/1220/image-20231220123118182.png)

### 4. 创建管理员用户

![image-20231220123540770](http://typora.hflxhn.com/documents/2023/1220/image-20231220123540770.png)

### 5. 实例配置

![image-20231220115824731](http://typora.hflxhn.com/documents/2023/1220/image-20231220115824731.png)

### 6. 开始使用 jenkins

![image-20231220123656964](http://typora.hflxhn.com/documents/2023/1220/image-20231220123656964.png)

## 构建 maven 项目

### 1. 配置 jdk

```bash
# Dashboard > Manage Jenkins￼> Tools
jdk1.8.0_202
/opt/jdk1.8.0_202
```

![image-20231222175026546](http://typora.hflxhn.com/documents/2023/1222/image-20231222175026546.png)

### 2. 配置指定版本的 maven

```bash
# Dashboard￼> Manage Jenkins￼> Tools
# 1. Default settings provider
/opt/apache-maven-3.6.3/conf/settings.xml

# 2. Default global settings provider
/opt/apache-maven-3.6.3/conf/settings.xml
```

![image-20231221101952385](http://typora.hflxhn.com/documents/2023/1222/image-20231221101952385.png)

```bash
# 3. Maven installations
apache-maven-3.6.3
/opt/apache-maven-3.6.3
```

![image-20231221102542794](http://typora.hflxhn.com/documents/2023/1222/image-20231221102542794.png)

### 3. 安装 maven 插件

```bash
# Dashboard > Manage Jenkins > Plugins
1. Maven Integration
```

![image-20231221103023447](http://typora.hflxhn.com/documents/2023/1222/image-20231221103023447.png)

```bash
2. Deploy to container
```

![image-20231221103121971](http://typora.hflxhn.com/documents/2023/1222/image-20231221103121971.png)

### 4. 修改 maven 为阿里源

```bash
cp /podman/podman-jenkins/opt/apache-maven-3.6.3/conf/settings.xml{,.bak}

# vi /podman/podman-jenkins/opt/apache-maven-3.6.3/conf/settings.xml
    <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <mirrorOf>central</mirrorOf>        
    </mirror>
```

![image-20231221103444065](http://typora.hflxhn.com/documents/2023/1222/image-20231221103444065.png)

### 5. 构建项目

````bash
1. new item
````

![image-20231221103702289](http://typora.hflxhn.com/documents/2023/1222/image-20231221103702289.png)

```bash
2. newJob
```

![image-20231221103958300](http://typora.hflxhn.com/documents/2023/1222/image-20231221103958300.png)

```bash
3. General
```

![image-20231222163921113](http://typora.hflxhn.com/documents/2023/1222/image-20231222163921113.png)

```bash
4. Source Code Management
```

![image-20231222164027815](http://typora.hflxhn.com/documents/2023/1222/image-20231222164027815.png)

```bash
6. Build
# pom.xml git项目路径的相对位置
clean install -D maven.test.skip=true
```

![image-20231222164205333](http://typora.hflxhn.com/documents/2023/1222/image-20231222164205333.png)
