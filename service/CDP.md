# CDP-7.11.3 服务部署

## 前置准备

### 1. 虚拟机准备

| hostname | os       | cpu  | memory | sys disk | data disk | IP 1          | IP 2          |
| -------- | -------- | ---- | ------ | -------- | --------- | ------------- | ------------- |
| cdp01    | rhel-8.4 | 8    | 32G    | 40G      | 500G      | 10.172.24.151 | 10.172.98.251 |
| cdp02    | rhel-8.4 | 8    | 32G    | 40G      | 500G      | 10.172.24.152 | 10.172.98.252 |
| cdp03    | rhel-8.4 | 8    | 32G    | 40G      | 500G      | 10.172.24.153 | 10.172.98.253 |

### 2. 免密登录

```bash
# 生成密钥对
ssh-keygen -t rsa -P "" -f ~/.ssh/id_rsa

# 安装 sshpass
ssh-copy-id -o StrictHostKeyChecking=no root@10.172.24.151
ssh-copy-id -o StrictHostKeyChecking=no root@10.172.24.152
ssh-copy-id -o StrictHostKeyChecking=no root@10.172.24.153
```

### 3. 公共函数

```bash
mkdir ~/cdp-install

cat > ~/cdp-install/cdp-func.sh << 'END'
function line() {
    echo -e "\033[33m#################### ${1} ##################\033[0m"
}

function check() {
    if [[ $? == 0 ]]; then
        echo -e "\033[32m${1} --------> success\033[0m";
    else
        echo -e "\033[31m${1} --------> fail\033[0m";
    fi
}
END
source ~/cdp-install/cdp-func.sh

cat > ~/cdp-install/cdp-vars.sh << 'END'
username=root

all_address=(
10.172.24.151
10.172.24.152
10.172.24.153
);

all_fqdn=(
cdh01.vdi.sh.moonpac.com
cdh02.vdi.sh.moonpac.com
cdh03.vdi.sh.moonpac.com
);

all_hostname=(
cdh01
cdh02
cdh03
);
END

cat > ~/cdp-install/all-node.sh << 'END'
#!/bin/bash
source ~/cdp-install/cdp-func.sh
source ~/cdp-install/cdp-vars.sh

for (( i = 0; i < ${#all_address[*]}; i++ )); do
    line "${all_address[i]}"
    ssh $username@${all_address[i]} ${1}
    check "${1}"
done
END

cat > ~/cdp-install/scp-node.sh << 'END'
#!/bin/bash
source ~/cdp-install/cdp-func.sh
source ~/cdp-install/cdp-vars.sh

for (( i = 0; i < ${#all_address[*]}; i++ )); do
    line "${all_address[i]}"
    scp -r ${1} ${username}@${all_address[i]}:${2} &>/dev/null
    check "scp -r ${1} $username@${all_address[i]}:${2}"
done
END
```

### 3. 修改主机名

```bash
for i in 1 2 3; do 
	ssh root@10.172.24.15${i} "hostnamectl set-hostname cdp0${i}"; hostname; 
done
```

### 4. NTP 同步

```bash
./cdp-install/all-node.sh 'yum install chrony -y &>/dev/null'
./cdp-install/all-node.sh 'ls /etc/chrony.conf.bak &>/dev/null || cp -r /etc/chrony.conf /etc/chrony.conf.bak'
./cdp-install/all-node.sh 'grep -vE "^#|^$" /etc/chrony.conf.bak > /etc/chrony.conf'
./cdp-install/all-node.sh 'sed -i "/^pool/d" /etc/chrony.conf'
./cdp-install/all-node.sh 'grep "pool 10.172.24.151 iburst" /etc/chrony.conf || echo "pool 10.172.24.151 iburst" >> /etc/chrony.conf'
./cdp-install/all-node.sh 'cat /etc/chrony.conf'

ssh 10.172.24.151 'grep "0.0.0.0/0" /etc/chrony.conf || echo "allow 0.0.0.0/0" >> /etc/chrony.conf'
ssh 10.172.24.151 'sed -i "s/^pool.*/pool 10.173.28.219 iburst/" /etc/chrony.conf'
ssh 10.172.24.151 'firewall-cmd --add-service=ntp; firewall-cmd --add-service=ntp --per'

./cdp-install/all-node.sh 'systemctl restart chronyd.service'
./cdp-install/all-node.sh 'chronyc sources'
```

### 5. 系统初始化

```bash
# 关闭防火墙
./cdp-install/all-node.sh 'systemctl disable --now firewalld.service'
./cdp-install/all-node.sh 'sed -i "s/^SELINUX=.*/SELINUX=disabled/" /etc/selinux/config && setenforce 0'

./cdp-install/all-node.sh 'firewall-cmd --add-source=10.172.24.0/24 --zone=trusted'
./cdp-install/all-node.sh 'firewall-cmd --add-source=10.172.24.0/24 --zone=trusted --per'

# 关闭透明内存
echo 'GRUB_CMDLINE_LINUX="transparent_hugepage=never"' >>  /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg
grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg

# 设置内存使用权重
echo vm.swappiness=1 >> /etc/sysctl.conf
echo vm.max_map_count=2000000 >> /etc/sysctl.conf
sysctl -p

# 设置 limits
cat > /etc/security/limits.conf << END
*  soft nofile 128000
*  hard nofile 128000
*  soft nproc 128000
*  hard nproc 128000
END
```

### 6. 安装随机数发生器

```bash
./cdp-install/all-node.sh 'yum install rng-tools -y'
./cdp-install/all-node.sh 'systemctl enable --now rngd.service'
./cdp-install/all-node.sh 'cat /proc/sys/kernel/random/entropy_avail'
```

### 7. 设置 hosts

```bash
cat > /etc/hosts << END
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.172.24.151 cdp01
10.172.24.152 cdp02
10.172.24.153 cdp03
END

~/cdp-install/scp-node.sh /etc/hosts /etc/hosts
```

### 8. 配置 yum 源

```bash
cat > /etc/yum.repos.d/cloudera-manager.repo << END
[cloudera-manager]
name = Cloudera Manager, Version cm7.11.3.9
baseurl = http://10.173.28.219/web/mirror/CDP/cm7.11.3.16/
gpgcheck = 0
END
~/cdp-install/scp-node.sh /etc/yum.repos.d/cloudera-manager.repo /etc/yum.repos.d/cloudera-manager.repo
```

### 9. 安装 JDK

```bash
./cdp-install/all-node.sh 'yum install openjdk8 -y'
```

### 10. 安装 python3.8

```bash
./cdp-install/all-node.sh 'yum install python3.8 -y'
./cdp-install/all-node.sh 'pip3 install psycopg2-binary'
```

## 安装 MySQL

### 1. 安装及配置

```bash
# 安装
yum -y install mariadb-server

# 创建数据目录
mkdir -p /data/mysql/{data,log,run}
chown mysql:mysql -R /data/mysql
yum install policycoreutils-python-utils -y
semanage fcontext -a -t mysqld_db_t '/data/mysql/data(/.*)?'
semanage fcontext -a -t mysqld_log_t '/data/mysql/log(/.*)?'
semanage fcontext -a -t mysqld_var_run_t '/data/mysql/run(/.*)?'
restorecon -RvF /data

# 配置
cat >> /etc/my.cnf << END

[mysqld]
datadir=/data/mysql/data
log-error=/data/mysql/log/mysqld.log
transaction-isolation = READ-COMMITTED
symbolic-links = 0
key_buffer_size = 32M
max_allowed_packet = 32M
thread_stack = 256K
thread_cache_size = 64
max_connections = 550
log_bin=/var/lib/mysql/mysql_binary_log
server_id=1
binlog_format = mixed
read_buffer_size = 2M
read_rnd_buffer_size = 16M
sort_buffer_size = 8M
join_buffer_size = 8M
innodb_file_per_table = 1
innodb_flush_log_at_trx_commit  = 2
innodb_log_buffer_size = 64M
innodb_buffer_pool_size = 4G
innodb_thread_concurrency = 8
innodb_flush_method = O_DIRECT
innodb_log_file_size = 512M
log_bin_trust_function_creators=true

[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
sql_mode=STRICT_ALL_TABLES
END

# 启动
systemctl enable mariadb --now
```

### 2. 创建数据库和用户

```mysql
create database if not exists scm default character set utf8 default collate utf8_general_ci;
create database if not exists rman default character set utf8 default collate utf8_general_ci;
create database if not exists hive default character set utf8 default collate utf8_general_ci;
create database if not exists oozie default character set utf8 default collate utf8_general_ci;
create database if not exists hue default character set utf8 default collate utf8_general_ci;
create database if not exists ranger default character set utf8 default collate utf8_general_ci;
create database if not exists queryprocessor default character set utf8 default collate utf8_general_ci;

grant all on *.* to 'mproot'@'%' identified by 'Fhl3y_Jsdwj';
grant all on scm.* to 'scm'@'%' identified by 'Fhl3y_Jsdwj';
grant all on rman.* to 'rman'@'%' identified by 'Fhl3y_Jsdwj';
grant all on hive.* to 'hive'@'%' identified by 'Fhl3y_Jsdwj';
grant all on oozie.* to 'oozie'@'%' identified by 'Fhl3y_Jsdwj';
grant all on hue.* to 'hue'@'%' identified by 'Fhl3y_Jsdwj';
grant all on ranger.* to 'ranger'@'%' identified by 'Fhl3y_Jsdwj';
grant all on queryprocessor.* to 'queryprocessor'@'%' identified by 'Fhl3y_Jsdwj';

alter user 'root'@'localhost' identified by 'Fhl3y_Jsdwj';

flush privileges;
```

### 3. 配置驱动

```bash
~/cdp-install/all-node.sh "mkdir -p /usr/share/java && cd /usr/share/java && wget -c http://10.173.28.219/web/soft/linux/jdk/mysql-connector-java-5.1.47.jar &>/dev/null && mv mysql-connector-java-5.1.47.jar mysql-connector-java.jar"
```

## 部署 Cloudera-Manage

### 1. 安装 CM

```bash
yum install -y cloudera-manager-daemons cloudera-manager-server
```

### 2. 数据库初始化

```bash
/opt/cloudera/cm/schema/scm_prepare_database.sh mysql scm scm Fhl3y_Jsdwj
```

### 3. 启动

```bash
systemctl enable --now cloudera-scm-server

# 查看 log
tail -f /var/log/cloudera-scm-server/cloudera-scm-server.log
```

### 4. 防火墙放行

```bash
cat > /usr/lib/firewalld/services/cloudera-manager.xml << END
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>Cloudera Manager</short>
  <description>Firewall service for Cloudera Manager</description>
  <port protocol="tcp" port="7180"/>
  <port protocol="tcp" port="7182"/>
  <port protocol="tcp" port="7183"/>
</service>
END

firewall-cmd --reload
firewall-cmd --add-service=cloudera-manager
firewall-cmd --add-service=cloudera-manager --per
```

### 5. WEB 访问

```bash
http://10.172.24.151:7180
username: admin
password: admin
```

## WEB 初始化

### 1. 初始化集群

#### 1.1 登录



![image-20241016113042482](http://typora.hflxhn.com/documents/2024/1016/image-20241016113042482.png)

#### 1.2 Upload License File

![image-20241015163936826](http://typora.hflxhn.com/documents/2024/1016/image-20241015163936826.png)

#### 1.3 Add Private Cloud Base Cluster

![image-20241015164146875](http://typora.hflxhn.com/documents/2024/1016/image-20241015164146875.png)

#### 1.4 Cluster Basics

![image-20241015164237453](http://typora.hflxhn.com/documents/2024/1016/image-20241015164237453.png)

#### 1.5 Specify Host

![image-20241016113342143](http://typora.hflxhn.com/documents/2024/1016/image-20241016113342143.png)

#### 1.6 Select Repository

![image-20241016113655662](http://typora.hflxhn.com/documents/2024/1016/image-20241016113655662.png)

![image-20241015165531804](http://typora.hflxhn.com/documents/2024/1016/image-20241015165531804.png)

#### 1.7 Select JDK

![image-20241016113758343](http://typora.hflxhn.com/documents/2024/1016/image-20241016113758343.png)

#### 1.8 Enter Login Credentials

![image-20241016113831645](http://typora.hflxhn.com/documents/2024/1016/image-20241016113831645.png)

#### 1.9 Install Agents

![image-20241016113855907](http://typora.hflxhn.com/documents/2024/1016/image-20241016113855907.png)

![image-20241015175139072](http://typora.hflxhn.com/documents/2024/1016/image-20241015175139072.png)

#### 1.10 Install Parcels

![image-20241015175204530](http://typora.hflxhn.com/documents/2024/1016/image-20241015175204530.png)

#### 1.11 Inspect Cluster

![image-20241016125140718](http://typora.hflxhn.com/documents/2024/1016/image-20241016125140718.png)

### 2. Add Cluster

#### 2.1 Select Services

![image-20241016134524046](http://typora.hflxhn.com/documents/2024/1016/image-20241016134524046.png)

#### 2.2 Assign Roles

HDFS, Kafka, YARN, ZooKeeper

![image-20241016150816502](http://typora.hflxhn.com/documents/2024/1016/image-20241016150816502.png)

#### 2.3 Setup Database

![image-20241016142012075](http://typora.hflxhn.com/documents/2024/1016/image-20241016142012075.png)

#### 2.4 Enter Required Parameters 

![image-20241016150921822](http://typora.hflxhn.com/documents/2024/1016/image-20241016150921822.png)

#### 2.5 Review Changes

![image-20241016151127054](http://typora.hflxhn.com/documents/2024/1016/image-20241016151127054.png)

#### 2.6 Command Details

![image-20241016151655790](http://typora.hflxhn.com/documents/2024/1016/image-20241016151655790.png)

#### 2.7 Summary

![image-20241016151706522](http://typora.hflxhn.com/documents/2024/1016/image-20241016151706522.png)

![image-20241016155042122](http://typora.hflxhn.com/documents/2024/1016/image-20241016155042122.png)
