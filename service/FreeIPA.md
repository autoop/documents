# rhel 8.8 部署 FreeIPA

## 架构图

![image-20230704162126703](http://typora.hflxhn.com/documents/2023/0704/image-20230704162126703.png)

```bash
https://www.freeipa.org/page/About
```

## 服务端安装

```bash
yum -y install @idm:DL1
yum -y install freeipa-server

ipa-server-install
```

![image-20230704162626797](http://typora.hflxhn.com/documents/2023/0704/image-20230704162626797.png)

![image-20230704162808999](http://typora.hflxhn.com/documents/2023/0704/image-20230704162808999.png)

```bash
# 防火墙放行
firewall-cmd --add-port=80/tcp
firewall-cmd --add-port=443/tcp
firewall-cmd --add-port=389/tcp
firewall-cmd --add-port=636/tcp
firewall-cmd --add-port=88/tcp
firewall-cmd --add-port=464/tcp
firewall-cmd --add-port=88/udp
firewall-cmd --add-port=464/udp
firewall-cmd --add-port=123/udp
firewall-cmd --runtime-to-permanent
```

## 客户端安装

```bash
yum -y install ipa-client
echo "10.173.28.205 ipa.local.com ipa" >> /etc/hosts
ipa-client-install --hostname=`hostname -f` \
--mkhomedir \
--server=ipa.local.com \
--domain local.com \
--realm LOCAL.COM
```

