# MySQL Server

## 1. 部署 MySQL

```bash
# 安装容器 Podman
yum install podman -y

# 拉去镜像
podman pull library/mysql:8.0.33

# 创建工作目录
mkdir -p /podman/podman-mysql/{data,etc/conf.d,log,mysql-files}

# 生成 mysql 配置文件
cat > /podman/podman-mysql/etc/conf.d/my.cnf << 'END'
[mysqld]
port = 3306
bind-address = 0.0.0.0
character-set-server = utf8mb4
max_connections = 1024
default-time_zone = '+8:00'
slow-query-log = on
long_query_time = 2
skip-log-bin
# skip-grant-tables

[client]
default-character-set = utf8mb4
END

# 拉起容器
podman run -d \
--name mysql-8.0.33 \
-p 3306:3306 \
--tz Asia/Shanghai \
-v /podman/podman-mysql/etc/conf.d:/etc/mysql/conf.d:Z \
-v /podman/podman-mysql/data:/var/lib/mysql:Z \
-v /podman/podman-mysql/mysql-files:/var/lib/mysql-files:Z \
-v /podman/podman-mysql/log:/var/log/mysql:Z \
-e MYSQL_ROOT_PASSWORD=123 \
mysql:8.0.33

# 生成 service 文件
loginctl enable-linger
mkdir -p ~/.config/systemd/user && cd ~/.config/systemd/user
podman generate systemd --files --new --name mysql-8.0.33
podman stop mysql-8.0.33 && podman rm mysql-8.0.33

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-mysql-8.0.33.service --now

# 防火墙放行
firewall-cmd --add-port=3306/tcp
firewall-cmd --add-port=3306/tcp --per

# 进入容器
podman exec -it mysql-8.0.33 bash
podman exec -it mysql-8.0.33 mysql -uroot -p123

# 查看 logs
podman logs mysql-8.0.33
```

## 2. 配置 MySQL 主从

### 2.1 主服务器操作

```bash
# 修改配置文件
# vi /podman/podman-mysql/etc/conf.d/my.cnf
server_id = 1
log-bin = mysql-bin
expire_logs_days = 7
# 屏蔽系统库
binlog-ignore-db=mysql
binlog-ignore-db=information_schema
binlog-ignore-db=performance_schema

# 重启 MySQL
systemctl --user restart container-mysql-8.0.33.service

# 创建主从用户
podman exec -it mysql-8.0.33 mysql -uroot -p123
drop user 'sync'@'10.%.%.%';
create user 'sync'@'10.%.%.%' identified with mysql_native_password by 'Fhl3y_Jsdwj'; 
grant all privileges on *.* to sync@'10.%.%.%';

# 查看 binlog 位置点
mysql> show master status;
+------------------+----------+--------------+---------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                            | Executed_Gtid_Set |
+------------------+----------+--------------+---------------------------------------------+-------------------+
| mysql-bin.000003 |      671 |              | mysql,information_schema,performance_schema |                   |
+------------------+----------+--------------+---------------------------------------------+-------------------+
1 row in set (0.00 sec)
```

### 2.2 从服务器操作

```bash
# 修改配置文件
# vi /podman/podman-mysql/etc/conf.d/my.cnf
server_id = 2
log-bin = mysql-bin
slave-net-timeout = 60
replicate_wild_ignore_table=mysql.%
replicate_wild_ignore_table=information_schema.%
replicate_wild_ignore_table=performance_schema.%

# 重启 MySQL
systemctl --user restart container-mysql-8.0.33.service

# 登录 mysql
podman exec -it mysql-8.0.33 mysql -uroot -p123
stop slave;
change master to master_host='10.173.20.70',master_user='sync',master_password='Fhl3y_Jsdwj',master_log_file='mysql-bin.000003',master_log_pos=671;
start slave;

# 查看状态
show slave status\G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for source to send event
                  Master_Host: 10.173.20.70
                  Master_User: sync
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000003
          Read_Master_Log_Pos: 671
               Relay_Log_File: f6facce8088d-relay-bin.000002
                Relay_Log_Pos: 326
        Relay_Master_Log_File: mysql-bin.000003
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
```

## 3. MySQL 常用命令

```mysql
# 修改 root 密码
alter user 'root'@'%' identified by '1';

# 创建用户及设置密码
create user 'test'@'%' identified by '123';
grant all privileges on `test_%`.* to 'test'@'%';


# DDL 语句
## 创建数据库
create database db1;

## 删除数据库
drop database db1;

## 创建表
create table t1(
    id int unsigned not null auto_increment,
    username varchar(20) not null comment '用户名',
    passwd char(32) not null comment '密码',
    code char(32) not null comment '加盐字符串',
    status tinyint unsigned not null default 0 comment '状态 0正常 1停用',
    create_time int unsigned not null comment '新增时间',
    update_time int unsigned not null comment '更新时间',
    primary key(id),
    unique key username(username)
)engine=InnoDB auto_increment=1 default charset=utf8 comment='xxx';

## 删除表
drop table if exists t1;


# DML语句
## 插入数据
insert into t1(username, passwd, code, create_time, update_time) values('user1', '123', '', 0, 0);
insert into t1 values('user2', '123', '', 0, 0);

## 更新数据
update t1 set username="user100" where id = 1;

## 删除数据
delete from t1 where name="user2";
```

## 4. MySQL 备份还原

```bash
# 备份脚本
cat > ~/bak-mysql << 'END'
#!/bin/bash

mysql_address=127.0.0.1
mysql_username=root
mysql_passwrod='xxxxxx'

# vars
time=`date +%Y%m%d`
bak_path=/backup/mysql

# create bak directory
mkdir -p ${bak_path}/${time}

# db name
db_name=$(mysql -u${mysql_username} -p${mysql_passwrod} -e "show databases;" | egrep -v "Database|mysql|information_schema|performance_schema|sys")

for db in ${db_name}; do
    mysqldump -u${mysql_username} -p${mysql_passwrod} --databases $db -h${mysql_address} > ${bak_path}/${time}/${db}.sql
done
END


# 还原脚本
cat > source-mysql << 'END'
#!/bin/bash

mysql_address=10.xxx.xx.xx
mysql_username=xxxxx
mysql_passwrod='xxxxxx'

# vars
time=`date +%Y%m%d`
bak_path=/backup/mysql/${time}

# funs
function check() {
    if [[ $? == 0 ]]; then
        echo -e "\033[32m${1} --------> success\033[0m";
    else
        echo -e "\033[31m${1} --------> fail\033[0m"; exit 1;
    fi
}

for bak_sql_file in ${bak_path}/*.sql; do
    db_name=$(basename ${bak_sql_file} | cut -d'.' -f1)
    mysql -u${mysql_username} -p${mysql_passwrod} -h${mysql_address} -e "drop database if exists ${db_name}; create database ${db_name}" &>/dev/null
    mysql -u${mysql_username} -p${mysql_passwrod} -h${mysql_address} ${db_name} < ${bak_sql_file} &>/dev/null
    check "source database: ${db_name}"
done
END
```

## 5. MySQL, MariaDB 版本对应关系

```mysql
Mariadb是MySQL的作者在MySQL被收购之后，由于担心所属公司把MySQL闭源而fork出来的一个分支。很大程度上，我们可以使用Mariadb来代替MySQL，但是在一些细节的方面，我们需要注意，尤其是在版本对应的关系上
```

| MySQL       | MariaDB                                 |
| ----------- | --------------------------------------- |
| 5.1         | 5.1, 5.2, 5.3                           |
| 5.5         | 5.5                                     |
| 5.6         | 10.0, 10.1                              |
| 5.7         | 10.2, 10.3, 10.4                        |
| 8, 有限代替 | 10.4                                    |
| 8, 有限代替 | 10.5                                    |
| 8           | 10.6, 10.7, 10.8， 10.9, 10.10          |
| 8           | 可以认为 10.5 开始和 mysql 8 对应起来了 |

```
我们可以认为Mariadb的10.2，10.3，10.4版本与MySQL 5.7对应；
Mariadb的10.4有限代替MySQL 8；
Mariadb的10.5有限代替MySQL 8
Mariadb的10.6, 10.7, 10.8, 10.9, 10.10对应MySQL 8

如果你想使用Mariadb是MySQL，如果是选择MySQL 5.7版本，可以使用Mariadb 10.4， 
如果是选择MySQL 8，可以选择Mariadb 10.6及其以后的版本，这样更为稳妥。
```

## 6. 查看 glibc 的版本

```bash
ldd --version | awk '/ldd/ {print $NF}'
```

## 7. MySQL 启动失败恢复

```bash
# [ERROR] InnoDB: Cannot find the dir slot for this record on that page

# 服务器意外掉电，数据库无法启动，启动日志报错，大概提示是上一次没有正常关机，可能有文件损坏，日志还提示需要你自己查查资料，看看怎么恢复数据库。
# https://dev.mysql.com/doc/refman/8.0/en/forcing-innodb-recovery.html
# 大概意思是mysql的自动恢复失败了，你需要在mysql的配置里面启用强制恢复模式。
[mysqld]
innodb_force_recovery = 1

# 官方文档写的很清楚，默认这个值是0，不会强制恢复。你可以改成 1-6中的数值，开启强制恢复。
# 数值越大，恢复越强硬，越可能恢复成功（启动成功，但是数据找回困难），所以建议你从1开始往大了试。不要上来就试6。从1开始试验。
# 文档也说明了每个恢复级别分别干啥了。
1. (SRV_FORCE_IGNORE_CORRUPT)Lets the server run even if it detects a corrupt page. Tries to make SELECT * FROM tbl_name jump over corrupt index records and pages, which helps in dumping tables.
2. (SRV_FORCE_NO_BACKGROUND)Prevents the master thread and any purge threads from running. If an unexpected exit would occur during the purge operation, this recovery value prevents it.
3. (SRV_FORCE_NO_TRX_UNDO)Does not run transaction rollbacks after crash recovery.
4. (SRV_FORCE_NO_IBUF_MERGE)Prevents insert buffer merge operations. If they would cause a crash, does not do them. Does not calculate table statistics. This value can permanently corrupt data files. After using this value, be prepared to drop and recreate all secondary indexes. Sets InnoDB to read-only.
5. (SRV_FORCE_NO_UNDO_LOG_SCAN)Does not look at undo logs when starting the database: InnoDB treats even incomplete transactions as committed. This value can permanently corrupt data files. Sets InnoDB to read-only.
6. (SRV_FORCE_NO_LOG_REDO)Does not do the redo log roll-forward in connection with recovery. This value can permanently corrupt data files. Leaves database pages in an obsolete state, which in turn may introduce more corruption into B-trees and other database structures. Sets InnoDB to read-only.
```
