# shell 常用工具脚本

## 测试内网存活的主机

```bash
read -p "please enter lan address (eg 172.16.0): " lan
for (( i = 1; i <= 255; i++ )); do
    address=${lan}.${i}

    ping ${address} -c1 -W0.01 &>/dev/null
    if [[ $? == 0 ]]; then
        echo -e "\033[32m${address} is up"
    fi
done
```