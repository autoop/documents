# dd 的使用

```bash
# 写 1G 的数据，测试使用了多长时间
dd if=/dev/zero of=testfile bs=1M count=1024 conv=fsync oflag=direct

# 用 dd 命令测磁盘的写速度
dd if=/dev/zero of=/dev/null

# 创建 1G大小的文件
dd if=/dev/zero of=/root/file count=1000 bs=1M

# 用 dd 命令创建厚置备的虚拟机磁盘文件 (kvm虚拟机)
qemu-img create test 100G
ls -lh test
dd if=/dev/zero of=/root/test count=102400 bs=1M
```