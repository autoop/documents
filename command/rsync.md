# Rsync

## rsync

```bash
rsync -av --delete /path/source/ /path/destination/
# -av 选项表示以递归方式同步，并保留文件属性
# -P 显示同步的过程及传输时的进度等信息
# --delete 表示删除目标目录中不存在于源目录中的文件
```

## 备份 NAS 盘

```bash
files=( eve-ng iso mirror podman software study )
target_file="root@172.24.0.246:/srv/dev-disk-by-uuid-0842f256-3fb1-439d-8834-357722d87ac2/Disk1"

for (( i = 0; i < ${#files[*]}; i++ )); do
    echo -e "\033[33m#################### rsync ${files[i]} to omv start ##################\033[0m" >> /tmp/rsync.log
    sudo rsync -avP --delete /qnap/${files[i]} ${target_file} >> /tmp/rsync.log
    echo -e "\033[33m#################### rsync ${files[i]} to omv end ##################\033[0m" >> /tmp/rsync.log
done
```

