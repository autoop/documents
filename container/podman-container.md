# 使用 podman 搭建常用容器

## 拉起 nginx 容器

```bash
# 非根用户使用 80 端口需要修改 sysctl.conf
grep "^net.ipv4.ip_unprivileged_port_start" /etc/sysctl.conf || echo "net.ipv4.ip_unprivileged_port_start = 80" >> /etc/sysctl.conf
sysctl -p

# 创建目录结构
mkdir -p /podman/podman-nginx/conf/conf.d
mkdir -p /podman/podman-nginx/{logs,html}
mkdir -p /qnap/{iso,cdrom,mirror,soft}

# 创建软连接
ls /podman/podman-nginx/html/iso    &>/dev/null || ln -s /qnap/iso /podman/podman-nginx/html/
ls /podman/podman-nginx/html/cdrom  &>/dev/null || ln -s /qnap/cdrom /podman/podman-nginx/html/
ls /podman/podman-nginx/html/mirror &>/dev/null || ln -s /qnap/mirror /podman/podman-nginx/html/
ls /podman/podman-nginx/html/soft   &>/dev/null || ln -s /qnap/soft /podman/podman-nginx/html/

# 生成 nginx 配置文件
cat > /podman/podman-nginx/conf/conf.d/web.conf << 'END'
server {
    listen       80;
    server_name  localhost;
    charset utf-8;
    autoindex_exact_size off;
    autoindex_localtime on;
    location / {}

    location /web {
        alias html;
        autoindex on;
    }
    location /video {
        alias /video;
        autoindex on;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {}
}
END

# 停止已经拉起的容器
loginctl enable-linger
systemctl --user stop container-nginx.service
podman stop nginx &>/dev/null && podman rm nginx &>/dev/null

# 拉起容器
podman run --privileged -dt \
--name nginx \
-p 80:80 \
--tz Asia/Shanghai \
-v /podman/podman-nginx/conf/conf.d:/etc/nginx/conf.d:Z \
-v /podman/podman-nginx/logs:/var/log/nginx:Z \
-v /podman/podman-nginx/html:/etc/nginx/html:Z \
-v /qnap/iso:/etc/nginx/html/iso \
-v /qnap/mirror:/etc/nginx/html/mirror \
-v /qnap/software:/etc/nginx/html/software \
-v /cdrom:/etc/nginx/html/cdrom:Z \
-v /video:/video:Z \
nginx:1.22.0

# 生成 service 文件
mkdir -p ~/.config/systemd/user && cd ~/.config/systemd/user
podman generate systemd --files --new --name nginx
podman stop nginx && podman rm nginx

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-nginx.service --now
```

## 搭建容器仓库

```bash
# 创建目录结构
mkdir -p /podman/podman-registry/certs && cd /podman/podman-registry/certs/

# 创建自签名证书
# 创建证书私钥
openssl genrsa 2048 > server.key
# 创建证书签名请求
cat > csr.cnf <<END
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
[dn]
C = CN
ST = shanxi
L = xi'an
O = fanyang
OU = fanyang
CN = registry.hflxhn.com
END
openssl req -new -key server.key -config csr.cnf -out server.csr

# 自签名证书
openssl x509 -req -days 3650 -in server.csr -signkey server.key -out server.crt

# 查看证书
openssl x509 -in /podman/podman-registry/certs/server.crt -text -noout

# 创建htpasswd文件支持认证
mkdir -p /podman/podman-registry/auth
sudo yum -y install httpd-tools
htpasswd -Bbn fanyang Fhl3y_Jsdwj >> /podman/podman-registry/auth/htpasswd

# 运行容器
loginctl enable-linger
systemctl --user stop container-registry.service
podman stop registry &>/dev/null && podman rm registry &>/dev/null
podman run --privileged -d \
--name registry \
-p 50000:5000 \
--tz Asia/Shanghai \
-v /podman/podman-registry:/var/lib/registry:Z \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/var/lib/registry/certs/server.crt \
-e REGISTRY_HTTP_TLS_KEY=/var/lib/registry/certs/server.key \
-e REGISTRY_AUTH=htpasswd \
-e REGISTRY_AUTH_HTPASSWD_REALM="Registry Realm" \
-e REGISTRY_AUTH_HTPASSWD_PATH=/var/lib/registry/auth/htpasswd \
registry:2.8.3

# 生成 service 文件
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name registry
mv container-registry.service ~/.config/systemd/user
podman stop registry && podman rm registry

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-registry.service --now

# 测试
curl -u fanyang:Fhl3y_Jsdwj -X GET https://localhost:50000/v2/_catalog -k
```

## 拉起 mysql

```bash
# 创建目录结构
mkdir -p /podman/podman-mysql/{data,etc/conf.d,log,mysql-files}

# 生成 mysql 配置文件
cat > /podman/podman-mysql/etc/conf.d/my.cnf << 'END'
[mysqld]
port = 3306
bind-address = 0.0.0.0
character-set-server = utf8mb4
max_connections = 1024
default-time_zone = '+8:00'
slow-query-log = on
long_query_time = 2
skip-log-bin
# skip-grant-tables

[client]
default-character-set = utf8mb4
END

# 停止已经拉起的容器
loginctl enable-linger
systemctl --user stop container-mysql-8.0.33.service &>/dev/null
podman stop mysql-8.0.33 &>/dev/null && podman rm mysql-8.0.33 &>/dev/null

# 拉起容器
podman rm -f mysql-8.0.33
podman run -d \
--name mysql-8.0.33 \
-p 3306:3306 \
--tz Asia/Shanghai \
-v /podman/podman-mysql/etc/conf.d:/etc/mysql/conf.d:Z \
-v /podman/podman-mysql/data:/var/lib/mysql:Z \
-v /podman/podman-mysql/mysql-files:/var/lib/mysql-files:Z \
-v /podman/podman-mysql/log:/var/log/mysql:Z \
-e MYSQL_ROOT_PASSWORD=123 \
mysql:8.0.33

# 生成 service 文件
mkdir -p ~/.config/systemd/user && cd ~/.config/systemd/user
podman generate systemd --files --new --name mysql-8.0.33
podman stop mysql-8.0.33 && podman rm mysql-8.0.33

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-mysql-8.0.33.service --now

# 进入容器
podman exec -it mysql-8.0.33 bash
podman exec -it mysql-8.0.33 mysql -uroot -p123

# 查看 logs
podman logs mysql-8.0.33
```

## 拉起 NextCloud

```bash
# 创建目录结构
mkdir -p /podman/podman-nextcloud/html

# 拉起容器
loginctl enable-linger
systemctl --user stop container-nextcloud.service
podman stop nextcloud && podman rm nextcloud
podman run -d \
--name nextcloud \
-p 8080:80 \
--tz Asia/Shanghai \
-v /podman/podman-nextcloud/html:/var/www/html:Z \
nextcloud:28.0.1-apache

# 生成 service 文件
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name nextcloud
mv container-nextcloud.service ~/.config/systemd/user
podman stop nextcloud && podman rm nextcloud

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-nextcloud.service --now

# 配置 https 访问 nextcloud
# 生成自签名证书
mkdir /podman/podman-nginx/conf/conf.d/ssl && cd /podman/podman-nginx/conf/conf.d/ssl
openssl genrsa 2048 > nextcloud.hflxhn.com.key
cat > nextcloud.hflxhn.com.cnf <<END
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
[dn]
C = CN
ST = shanxi
L = xi'an
O = fanyang
OU = fanyang
CN = nextcloud.hflxhn.com
END
openssl req -new -key nextcloud.hflxhn.com.key -config nextcloud.hflxhn.com.cnf -out nextcloud.hflxhn.com.csr
openssl x509 -req -days 3650 -in nextcloud.hflxhn.com.csr -signkey nextcloud.hflxhn.com.key -out nextcloud.hflxhn.com.crt

# nginx 配置
cat > /podman/podman-nginx/conf/conf.d/nextcloud.conf << 'END'
server {
    listen 443 ssl;
    server_name localhost;

    ssl_certificate conf.d/ssl/nextcloud.hflxhn.com.crt;
    ssl_certificate_key conf.d/ssl/nextcloud.hflxhn.com.key;

    location / {
        proxy_pass http://nextcloud.hflxhn.com:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
END

# 若使用httpd proxy 的方式需要加入以下设定, 以免登入转圈圈无法进入页面, 修改 config.php 档案
# /podman/podman-nextcloud/html/config/config.php
  'overwriteprotocol' => 'https',
  'overwritehost' => 'nextcloud.hflxhn.com',
```

## 拉起 GitLab

```bash
# 修改 sshd 服务默认端口
sed -i 's/^#Port.*/Port 65522/' /etc/ssh/sshd_config
semanage port -a -t ssh_port_t -p tcp 65522
systemctl restart sshd.service

# 创建 gitlab 所需目录
mkdir -p /podman/podman-gitlab/{config,logs,data}

# 拉起容器
loginctl enable-linger
podman run -d \
--name gitlab-ce \
--hostname gitlab.local.com \
-p 443:443 -p 80:80 -p 22:22 \
--tz Asia/Shanghai \
-v /podman/podman-gitlab/config:/etc/gitlab:Z \
-v /podman/podman-gitlab/logs:/var/log/gitlab:Z \
-v /podman/podman-gitlab/data:/var/opt/gitlab:Z \
--shm-size 256m \
gitlab/gitlab-ce:16.7.0-ce.0

# 生成 service 文件
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name gitlab-ce
mv container-gitlab-ce.service ~/.config/systemd/user
podman stop gitlab-ce && podman rm gitlab-ce

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-gitlab-ce.service --now

# 放行防火墙
firewall-cmd --add-port=80/tcp --add-port=443/tcp --add-port=65522/tcp
firewall-cmd --add-port=80/tcp --add-port=443/tcp --add-port=65522/tcp --per

# 查看默认用户默认的密码
grep 'Password:' /podman/podman-gitlab/config/initial_root_password
```

## 拉起 jenkins

```bash
# 创建目录结构
mkdir -p /podman/podman-jenkins/var/jenkins_home
mkdir -p /podman/podman-jenkins/opt

# 下载 maven
wget -c https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.6.3/apache-maven-3.6.3-bin.tar.gz
tar xf apache-maven-3.6.3-bin.tar.gz -C /podman/podman-jenkins/opt

# 下载jdk
wget -c https://mirrors.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz
tar xf jdk-8u202-linux-x64.tar.gz -C /podman/podman-jenkins/opt

# 下载 nodejs
wget -c https://nodejs.org/dist/v16.20.2/node-v16.20.2-linux-x64.tar.xz
tar xf node-v16.20.2-linux-x64.tar.xz -C /podman/podman-jenkins/opt

# 拉起容器
loginctl enable-linger
podman run --privileged -d \
--name jenkins \
-p 8080:8080 -p 50000:50000 \
--tz Asia/Shanghai \
-v /podman/podman-jenkins/var/jenkins_home:/var/jenkins_home:Z \
-v /podman/podman-jenkins/opt/node-v16.20.2-linux-x64:/opt/node-v16.20.2-linux-x64:Z \
-v /podman/podman-jenkins/opt/apache-maven-3.6.3:/opt/apache-maven-3.6.3:Z \
-v /podman/podman-jenkins/opt/jdk1.8.0_202:/opt/jdk1.8.0_202:Z \
jenkins/jenkins:2.438

# 生成 service 文件
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name jenkins
mv container-jenkins.service ~/.config/systemd/user
podman stop jenkins && podman rm jenkins

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-jenkins.service --now

# 放行防火墙
firewall-cmd --add-port=8080/tcp --add-port=50000/tcp
firewall-cmd --add-port=8080/tcp --add-port=50000/tcp --per
```

## 拉起 postgres

```bash
# 创建目录结构
mkdir -p /podman/podman-sonar/postgres/{postgresql,data}

# 拉起容器
podman run --privileged -d \
--name postgres \
-p 5432:5432 \
--tz Asia/Shanghai \
-v /podman/podman-sonar/postgres/postgresql:/var/lib/postgresql:Z \
-v /podman/podman-sonar/postgres/data:/var/lib/postgresql/data:Z \
-e POSTGRES_USER=sonar \
-e POSTGRES_PASSWORD=sonar \
-e POSTGRES_DB=sonar \
postgres:16.2

# 生成 service 文件
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name postgres
mv container-postgres.service ~/.config/systemd/user
podman stop postgres && podman rm postgres

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-postgres.service --now
```

## 拉起 sonarqube

```bash
# 创建目录结构
mkdir -p /podman/podman-sonar/sonarqube/{logs,conf,data,extensions}
chmod -R 777 /podman/podman-sonar/sonarqube/

# 修改系统参数
echo "vm.max_map_count=262144" > /etc/sysctl.conf
sysctl -p

# 拉起容器
podman run -d \
--name sonarqube \
-p 9000:9000 \
--tz Asia/Shanghai \
-e SONAR_JDBC_URL=jdbc:postgresql://10.173.28.206:5432/sonar \
-e SONAR_JDBC_USERNAME=sonar \
-e SONAR_JDBC_PASSWORD=sonar \
-v /podman/podman-sonar/sonarqube/logs:/opt/sonarqube/logs:Z \
-v /podman/podman-sonar/sonarqube/conf:/opt/sonarqube/conf:Z \
-v /podman/podman-sonar/sonarqube/data:/opt/sonarqube/data:Z \
-v /podman/podman-sonar/sonarqube/extensions:/opt/sonarqube/extensions:Z \
sonarqube:9.9.4-community

# 生成 service 文件
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name sonarqube
mv container-sonarqube.service ~/.config/systemd/user
podman stop sonarqube && podman rm sonarqube

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-sonarqube.service --now
```

## 拉起 Netdata

```bash
mkdir -p /podman/podman-netdata/{cache,etc,lib}

# 拉起容器
podman run --privileged -d \
--name=netdata \
-p 19999:19999 \
--tz Asia/Shanghai \
--hostname=$(hostname) \
-v /podman/podman-netdata/etc:/etc/netdata:Z \
-v /podman/podman-netdata/lib:/var/lib/netdata:Z \
-v /podman/podman-netdata/cache:/var/cache/netdata:Z \
-v /etc/os-release:/host/etc/os-release:ro \
-v /etc/passwd:/host/etc/passwd:ro \
-v /etc/group:/host/etc/group:ro \
-v /proc:/host/proc:ro \
-v /sys:/host/sys:ro \
--cap-add SYS_PTRACE \
--security-opt apparmor=unconfined \
netdata/netdata

# 生成 service 文件
mkdir -p ~/.config/systemd/user
podman generate systemd --files --new --name netdata
mv container-netdata.service ~/.config/systemd/user
podman stop netdata && podman rm netdata

# 设置开启启动容器
systemctl --user daemon-reload
systemctl --user enable container-netdata.service --now
```

