# Podman 的使用

## podman 的介绍

### 1. podman 的由来

```bash
很多人可能遇到过开机重启时，由于Docker守护程序在占用多核CPU使用100%使用的情况，导致所有容器都无法启动，服务都不能用的情况。
而且Docker守护进程是以root特权权限启动的，是一个安全问题，那么有什么方法解决呢？（使用podman）
官网：https://podman.io/
```

### 2. podman介绍

```bash
Podman是一个开源项目，可在大多数Linux平台上使用并开源在GitHub上。
Podman是一个无守护进程的容器引擎，用于在Linux系统上开发，管理和运行Open Container Initiative（OCI）容器和容器镜像。
Podman提供了一个与Docker兼容的命令行前端，它可以简单地作为Docker cli，简单地说你可以直接添加别名：alias docker =podman来使用podman。
Podman控制下的容器可以由root用户运行，也可以由非特权用户运行。Podman管理整个容器的生态系统，其包括pod，容器，容器镜像，和使用libpod library的容器卷。
Podman专注于帮助您维护和修改OCI容器镜像的所有命令和功能，例如拉取和标记。它允许您在生产环境中创建，运行和维护从这些映像创建的容器。
```

### 3. podman和docker对比

```bash
1.docker 需要在我们的系统上运行一个守护进程(docker daemon)，而podman不需要
2.启动容器的方式不同：
docker cli 命令通过API跟 Docker Engine(引擎)交互告诉它我想创建一个container，然后docker Engine才会调用OCI container runtime(runc)来启动一个container。
这代表container的process(进程)不会是Docker CLI的child process(子进程)，而是Docker Engine的child process。
Podman是直接给OCI containner runtime(runc)进行交互来创建container的，所以container process直接是podman的child process。
3.因为docker有docker daemon，所以docker启动的容器支持--restart策略，但是podman不支持，
如果在k8s中就不存在这个问题，我们可以设置pod的重启策略，在系统中我们可以采用编写systemd服务来完成自启动
4.docker需要使用root用户来创建容器，但是podman不需要
```

### 4. podman架构

```bash
https://github.com/containers
Podman，Skopeo和Buildah这三个工具都是符合OCI计划下的工具（github/containers）。
主要是由RedHat推动的，他们配合可以完成Docker所有的功能，而且不需要守护程序或访问有root权限的组，更加安全可靠，是下一代容器容器工具。
Podman可以替换Docker中了大多数子命令（RUN，PUSH，PULL等）。
Podman不需要守护进程，而是使用用户命名空间来模拟容器中的root，无需连接到具有root权限的socket保证容器的体系安全。
Podman专注于维护和修改OCI镜像的所有命令和功能，例如拉取和标记。它还允许我们创建，运行和维护从这些镜像创建的容器。
Podman 只是 OCI 容器生态系统计划中的一部分，主要专注于帮助用户维护和修改符合 OCI 规范的容器镜像。
其它的组件还有 Buildah、Skopeo 等。
```

### 5. buildah

```bash
Buildah用来构建OCI镜像。虽然Podman也可以用户构建Docker镜像，但是构建速度超慢，并且默认情况下使用vfs存储驱动程序会耗尽大量磁盘空间。 
buildah bud（使用Dockerfile构建）则会非常快，并使用overlay存储驱动程序。
Buildah专注于构建OCI镜像。 
Buildah的命令复制了Dockerfile中的所有命令。
可以使用Dockerfiles构建镜像，并且不需要任何root权限。 
Buildah的最终目标是提供更低级别的coreutils界面来构建图像。
Buildah也支持非Dockerfiles构建镜像，可以允许将其他脚本语言集成到构建过程中。 
Buildah遵循一个简单的fork-exec模型，不以守护进程运行，但它基于golang中的综合API，可以存储到其他工具中。
虽然 Podman 也可以支持用户构建 Docker 镜像，但是构建速度比较慢。并且默认情况下使用 VFS 存储驱动程序会消耗大量磁盘空间。
Buildah 是一个专注于构建 OCI 容器镜像的工具，Buildah 构建速度非常快并使用覆盖存储驱动程序，可以节约大量的空间。
Buildah 和 Podman 之间的一个主要区别是：Podman 用于运行和管理容器， 允许我们使用熟悉的容器 CLI 命令在生产环境中管理和维护这些镜像和容器，而 Buildah 主用于构建容器。
```

### 6. skopeo

```bash
Skopeo是一个工具，允许我们通过推，拉和复制镜像来处理Docker和OC镜像。
Skopeo 是一个镜像管理工具，允许我们通过 Push、Pull和复制镜像来处理 Docker 和符合 OCI 规范的镜像。
```

## podman 安装

```bash
如果你想在rhel系统中玩podman，必须是rhel8.2版本以上。podman版本是1.9.3。从centos8.2开始默认情况下，除了最小化安装之外，系统都会默认安装podman。
如果你使用rhel8.2以上的版本，那么就直接安装podman就可以了。
在rhel8以上的系统中，默认的appstream中已经集成了podman的软件。

yum -y install podman
```

## podman 配置镜像加速

```bash
cp /etc/containers/registries.conf{,.bak}
cat > /etc/containers/registries.conf << 'END'
unqualified-search-registries = ["docker.io"]
[[registry]]
prefix = "docker.io"
location = "fiyc0dbc.mirror.aliyuncs.com"
END

mv /etc/containers/registries.conf.d/000-shortnames.conf /etc/containers/registries.conf.d/000-shortnames.conf.bak
mv /etc/containers/registries.conf.d/001-rhel-shortnames.conf /etc/containers/registries.conf.d/001-rhel-shortnames.conf.bak
```

## podman 镜像管理

```bash
# 镜像拉取
podman pull httpd

# 镜像查看
podman images

# 镜像删除
podman image rm docker.io/library/ubuntu

# 镜像备份
podman save > centos-latest.tar docker.io/library/centos:latest
ls -lh centos-latest.tar

# 镜像导入
podman load -i centos-latest.tar

# podman镜像搜索
podman search nginx
```

## podman 容器管理

```bash
# 运行容器
podman run -dt --name web1 httpd
podman exec -it web1 bash

# 停止容器
podman container stop web1
podman ps -a

# 启动容器
podman start web1

# 重启容器
podman restart web1

# 删除容器
podman stop web1
podman rm web1

# 强行删除容器
podman rm -f container

# 查看容器的详细信息
podman run -dt --name web1 httpd
podman inspect web1
```

## podman 网络管理

```bash
# 容器网络的创建
nmcli device status
nmcli con show

# 创建容器网络
podman network create
podman network ls

# 查看网络
nmcli con show
ip a show

# 创建容器指定网络
podman run -dt --name web2 --network podman1 httpd
bridge link

# 创建ipv4网络指定网段
podman network create --subnet 192.5.0.0/16 newnet
cat /etc/containers/networks/newnet.json

# 创建ipv6网络指定网段
podman network create --subnet 2001:db8::/64 --ipv6 newnetv6
podman run -dt --name web4 --network newnetv6 httpd
podman inspect web4 | grep GlobalIPv6Address

# 容器网络的删除 (删除网络之前必须先停止容器)
podman network ls
podman network rm newnet
podman network rm newnetv6
podman network rm podman1
podman network ls

# 容器的端口映射
podman run -dt --name web2 -p 12345:80 httpd
```

## podman 持久化存储

### 数据卷

```bash
# 创建数据卷
podman volume ls
podman volume create volume1
find / -name volume1

podman volume create web
podman run -dt --name centos1 -v web:/web centos
podman exec -i centos1 df -Th

# 查看数据卷
podman volume inspect web
podman inspect centos1 | grep web

# 使用数据卷指定容器内部文件
podman run -dt --name web1 -v web:/usr/local/apache2/htdocs httpd
podman exec -i web1 df -Th
```

### bind mounts

```bash
# 创建目录
mkdir /web2
echo "dmxy" >> /web2/index.html
```

```bash
# 挂载web2目录到容器目录
podman run -dt --name web2 -v /web2:/usr/local/apache2/htdocs httpd

# 查看容器网络
podman inspect web2 | grep 10.88

# 访问 web 页面
podman exec -i centos1 curl 10.88.0.12
```

![image-20230704101042716](http://typora.hflxhn.com/documents/2023/0704/image-20230704101042716.png)

```bash
# 关闭 selinux 再次访问
setenforce 0
podman exec -i centos1 curl 10.88.0.12
```

![image-20230704101330598](http://typora.hflxhn.com/documents/2023/0704/image-20230704101330598.png)

```bash
# 对比文件的selinux标记
ls -ldZ /web2/
ls -ldZ /var/lib/containers/storage/volumes/web/
setenforce 1
```

![image-20230704101419931](http://typora.hflxhn.com/documents/2023/0704/image-20230704101419931.png)

```bash
# 挂载目录时，加上 Z 参数解决 selinux 问题
podman run -dt --name web3 -p 22222:80 -v /web2:/usr/local/apache2/htdocs:Z httpd
curl localhost:22222
```

![image-20230704101531496](http://typora.hflxhn.com/documents/2023/0704/image-20230704101531496.png)

## podman 容器自启动

```bash
# --name表示使用容器的名字来代替容器的ID
# --files表示生成systemd的服务文件
# web1表示使用哪个容器生成systemd的服务文件

podman run -dt --name web1 httpd

podman generate systemd --name web1
podman generate systemd --name web1 --files

mv container-web1.service /etc/systemd/system/
restorecon -RvF /etc/systemd/system/container-web1.service
systemctl restart container-web1.service

# 高级玩法
podman run -dt --name web2 -p 88:80 -v /web2:/usr/local/apache2/htdocs:Z httpd
podman ps -a
curl localhost:88

podman generate systemd --files --new --name web2
cat container-web2.service

mv container-web2.service /etc/systemd/system/
restorecon -RvF /etc/systemd/system/container-web2.service
systemctl enable container-web2.service --now

podman ps -a
```

## 非根用户使用 podman 容器

```bash
# podman如果要使用普通用户来管理容器，那么这个普通用户必须是ssh登陆或者通过终端登陆才行。否则会有问题。
useradd greg
echo 123 | passwd --stdin greg
ssh greg@localhost

# root用户的images，普通用户是看不到的。所以普通用户需要自己拉image
podman pull httpd
podman images

# 拉起容器
mkdir web1
echo web1 > web1/index.html
podman run -dt --name web1 -p 54321:80 -v /home/greg/web1:/usr/local/apache2/htdocs:Z httpd
podman ps
curl localhost:54321

# 非根用户使用systemd接管podman容器
# 创建~/.config/systemd/user目录来存放普通用户的systemd文件
mkdir -p ~/.config/systemd/user

# 生成systemd服务文件
podman generate systemd --new --files --name web1

# 将服务文件移动到普通用户的systemd的目录文件
mv container-web1.service ~/.config/systemd/user/

# 恢复SELinux文件的安全上下文
restorecon -RvF ~/.config/systemd/user/container-web1.service

# 赋予普通用户的systemd管理权限
loginctl enable-linger
systemctl --user daemon-reload
systemctl --user enable container-web1.service --now

# 重启测试
reboot
```