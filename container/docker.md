# docker 的使用

## docker 安装

[docker 官网](https://docs.docker.com/)

```bash
https://docs.docker.com/

# 安装 yum-utils
yum install -y yum-utils

# 配置仓库
yum-config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
yum makecache

# 安装 docker
yum install docker-ce docker-ce-cli containerd.io

# 服务启动并设置开机自启
systemctl enable docker.service --now

# docker hello world 测试
docker run hello-world
```

## docker 镜像仓库

```bash
# 搜索容器镜像服务
# https://cr.console.aliyun.com/cn-chengdu/instances/mirrors
```

![image-20230628151320127](http://typora.hflxhn.com/documents/2023/0628/image-20230628151320127.png)

```bash
# 配置阿里云镜像仓库
mkdir -p /etc/docker
tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://fiyc0dbc.mirror.aliyuncs.com"]
}
EOF
systemctl daemon-reload
systemctl restart docker

# 镜像拉取测试
docker pull centos

# 镜像查看
docker images
bash
```

## 配置 docker 代理

```bash
# 将 socks5 代理转换为 http 代理
yum install epel-release
yum install privoxy -y

sed -i 's/^listen-address.*/listen-address 0.0.0.0:8118/' /etc/privoxy/config
sed -i '/^listen-address.*/a forward-socks5t / 192.168.0.100:10808 ./' /etc/privoxy/config
ergep -A1 '^listen-address' /etc/privoxy/config

# 配置 docker 使用 http 代理
mkdir -p /etc/systemd/system/docker.service.d
cat > /etc/systemd/system/docker.service.d/proxy.conf << 'END'
[Service]
Environment="HTTP_PROXY=http://127.0.0.1:8118"
Environment="HTTPS_PROXY=http://127.0.0.1:8118"
END
systemctl daemon-reload
systemctl restart docker.service
```

## docker 镜像管理

```bash
# 1. Docker 镜像拉取
# docker pull 命令就是默认拉取镜像的命令，默认情况下，如果不指定docker的仓库，默认的仓库就是dockerhub上拉取，如果指定了对应的docker仓库就从指定的仓库上拉取.
docker pull ubuntu:xenial-20210114

# 2. docker 镜像查看
docker images

# 3. docker 镜像删除
docker image rm ubuntu:xenial-20210114

# 如果已经有一个容器使用了你想要删除的镜像，那么你需要先删除这个容器，才能删除镜像
docker ps -a
docker container rm great_hoover
docker image rm hello-world:latest

# 4. docker 镜像导出
docker save ubuntu:latest -o ubuntu-latest.tar

# 5. docker 镜像导入
docker load -i ubuntu-latest.tar
```

## 在 dockerhub 上创将仓库

```bash
# 创建test仓库
```

![image-20230628171032225](http://typora.hflxhn.com/documents/2023/0628/image-20230628171032225.png)

![image-20230628171149248](http://typora.hflxhn.com/documents/2023/0628/image-20230628171149248.png)

```bash
# 修改镜像 tag
docker tag ubuntu:latest hflxhn/test:ubuntu-latest

# docker 登陆
docker login
```

![image-20230628173122904](http://typora.hflxhn.com/documents/2023/0628/image-20230628173122904.png)

```bash
# 镜像上传
docker push hflxhn/test:ubuntu-latest
```

## docker 容器

### docker 容器介绍

```bash
# 镜像是静态的，将镜像运行起来就是容器，所以说容器是动态的。镜像只会占用磁盘，但是容器会占用cpu，内存和网络。
# 镜像的英文:image
# 容器的英文:container
# 注意：你不能将容器理解成一个操作系统，因为容器本身并不包含内核，没有内核的东西自然不能称为操作系统。
```

### 运行容器

```bash
# 1. 一次性运行容器
# docker run
# docker container run
# 早期就是用 docker run 来运行一个容器，但是在 Docker 后期版本对命令行进行更加明显的使用方法，
# docker image ... 就表示操作 docker 镜像，docker container ... 就表示操作容器
# 需要注意的是如果 docker run 后面指定的镜像没有加 tag，那么默认就会运行 tag 为 latest 的镜像。
# 我们通过 docker ps 命令并没有查看到我们之前运行的基于 centos:latest 的容器。原因是什么呢？原因其实非常简单，容器已经运行完毕了，运行完就自动退出了。
# 我们可以使用 docker ps -a 查看所有的容器（包括运行结束退出的容器也可以查看到）
docker container run centos
docker ps -a

# 2. 交互式运行容器
# -i 就表示交互的意思
# -t 就表示tty，表示为容器开启一个终端
docker run -i -t centos
# 我们通过pwd和ls命令查看到了，这个容器并没有/boot目录，原因也很好理解，因为容器不是操作系统，所以不需要引导，也自然就没有内核存放的位置。
# 容器内部的第一个进程/bin/bash
ps aux

# 如果你使用-i -t参数以交互式的形式运行了容器，那么你可以使用ctrl+p+q（按住ctrl，再按p，p松手，再按q）临时的退出容器，这个容器就并没有真正的被关闭。
# 当临时的退出容器，并不是exit退出的时候，可以使用docker attach命令，再回到容器
docker attach 4e0b9159fc29

# 3. 以后台的形式运行容器(使用频率最高的)
# -d参数表示daemon的意思，表示将容器以守护进程的形式运行，说白了就是在后台运行。
docker run -d centos
# 那为什么在后台运行之后也是直接退出了呢？
# 运行一次在前台退出和在后台退出是一样的。就好比运行一个ls命令一样，不管你是在前台运行还是在后台运行，都是运行完之后就退出。

# 如何让一个容器在运行之后不退出呢？
# 我们需要增加一个 -t 参数，-t参数的意思是开启一个终端。docker run "image" "command" 后面的 command 会替代 image 构建的最后一个命令。
docker run -dt centos /bin/bash

# 强烈不建议使用 docker attach 命令进入在后台运行的容器，建议使用 docker exec 加上对应参数进入后台运行的容器。
# docker exec 表示容器的执行 -i -t 和 docker run 的参数是一样的 478137a63565 表示的是容器的 ID，bash 表示以 bash 命令运行这个容器。
# 因为你看上面的进程PID为1的进程为/bin/bash就是我在用docker run运行起来容器的那个进程，还有一个PID为15的bash进程，是我用docker exec运行的进程，
# 所以我使用exit并不是退出/bin/bash，而是退出bash
# 所以由于容器里面的PID为1的进程/bin/bash还在，
# 所以我exit之后没有退出容器，只是退出了容器里面的一个bash而已。
docker exec -it 478137a63565 bash
```

### 容器的生命周期管理

```bash
# 1. 容器的停止
docker stop 478137a63565

# 2. 容器的启动
# 没有加-d参数的容器，即使我们将它手工启动，也不会让他持续性运行
# 只有以后台的形式运行的容器才有生命周期管理的意义
# Exited后面有一个数字，数字代表着容器结束时候的返回值，返回值是0表示容器是正常结束的，返回值非0表示容器是非正常结束的。
# docker kill是强行干掉一个容器，docker stop是正常关闭一个容器
docker start 478137a63565

# 3. 容器的重启
docker restart 478137a63565

# 4. 容器的删除
# 默认情况下只能删除停止的容器，运行的容器删除会报错，但是也可以使用-f参数强行删除运行的容器（强烈不建议）
docker container rm 478137a63565

# 强行删除所有容器（仅限于lab环境操作）
docker rm -f `docker ps -qa`
```

## 查看容器信息

```bash
# 1. 查看容器log
docker run -dt --name web nginx
docker logs web

# 2. 查看容器进程信息
# 一般情况你可以进入到容器内部使用ps命令查看容器的进程信息，但并不是所有的容器都支持这么操作
docker exec -it centos-test bash

docker top web

# 3. 查看容器内部细节
docker inspect web

# 4. 容器和主机间的文件拷贝
docker cp ./ubuntu-latest.tar adoring_sutherland:/
# 查看容器文件
docker exec -it adoring_sutherland ls
rm -rf ubuntu-latest.tar
docker cp adoring_sutherland:/ubuntu-latest.tar .
```

## 容器的网络

```bash
# 当你使用容器时（以docker为例），容器的通信就变得至关重要。要解决容器的通信问题，必须具备一定的网络基础。
# Docker有4种网络模式，只有两种是常用的。这4种网络模式分别为bridge，host，none，container。
# 常用的是host模式和bridge模式。
```

### 1. bridge网络模式

```bash
# bridge网络模式会为每个容器分配IP地址。分配的IP地址从何而来呢？
ip a show docker0

nmcli device status
nmcli con show
nmcli connection show docker0 | grep 'connection.type'

# 在该模式中，Docker进程创建了一个以太网桥docker0，这个以太网桥你可以理解成一个交换机，新建的容器默认会自动的桥接到这个网桥上。

# 从RHEL8或者CentOS8开始已经默认不使用brctl命令查看网桥和网卡的关系了，代替的是bridge命令
bridge link

docker exec -it centos-test ip a

# 可以看到在 host 上看到的两个桥接到 docker 网桥的网卡和容器内部看到的网卡并不是一个，因为 mac 地址不同。
# 在 host 上看到的网卡和容器内部的网卡其实叫做 veth pair
# veth pair其实是一对虚拟网卡，这一对虚拟网卡就像"网线"一样。具体内容如下图所示
```

![image-20230629150007864](http://typora.hflxhn.com/documents/2023/0629/image-20230629150007864.png)

```bash
iptables -L -t nat -n
```

![image-20230629150230559](http://typora.hflxhn.com/documents/2023/0629/image-20230629150230559.png)

```bash
# 查看docker的网络
docker network ls

# 连接到 bridge 的容器都在 bridge 的网桥中被记录了
docker network inspect bridge

# 总结：Docker 的 bridge 网络模式的底层采用的是 Linux bridge，veth pair，iptables
```

![image-20230629150443395](http://typora.hflxhn.com/documents/2023/0629/image-20230629150443395.png)

### 2. Host网络模式

```bash
# Docker的Host网络模式其实就是容器和host共享一个网络。说白了就是容器完全的使用容器所在主机上的网络，并不对容器网络做任何隔离。
# 使用--network指定容器运行时的网络模式
docker run -dt --name centos1 --network host centos

docker exec -it centos1 ip a

# 采用host网络模式的Docker容器，可以直接使用宿主机的IP地址与外界通信，若是宿主机的eth0是一个公有IP，那么容器也会共享这个公有IP，
# 同时容器服务的端口也可以使用宿主机的端口，无需进行SNAT的转换。
# 带来的好处就是性能好，带来的劣势就是容器的网络缺少了隔离性，增加了风险，并且由于所有容器和宿主机共享同一个网络，网络资源也受到了限制。
docker network inspect host
```

![image-20230629150753592](http://typora.hflxhn.com/documents/2023/0629/image-20230629150753592.png)

### 3. none网络模式

```bash
# none的网络模式是Docker容器中最容易理解的一种网络模式，因为如果使用了none作为Docker容器的网络模式，
# 那么就意味着容器会禁用网络功能，只会留一个环回接口。我们同样可以使用--network参数在容器运行的时候指定none类型的网络。
docker run -dt --name centos2 --network none centos

docker exec -it centos2 ip a show

# none模式的好处就是Docker容器几乎不参与网络的配置，那么如果你想针对这个容器配置网络，就需要第三方的driver。
# none模式的优点就是让你容器的网络设置更加的开放，不再局限于Docker自带的网络模式。
docker network inspect none
```

### 4. container网络模式

```bash
docker run -dt --network container:centos1 --name centos3 centos
# container类型的网络表明指定一个容器，和那个容器共享一个网络
```

![image-20230629151040809](http://typora.hflxhn.com/documents/2023/0629/image-20230629151040809.png)

### 5. 自定义网络和网络连接

```bash
# 通过我们之前的学习，我们知道了，Docker容器网络的几种常见的模式。默认情况下docker容器会使用bridge类型的网络。
docker network create test-network
ip a show

docker run -dt --network test-network --name test-centos1 centos
docker run -dt --network test-network --name test-centos2 centos

docker exec -it test-centos1 ip a
docker exec -it test-centos2 ip a

docker network disconnect test-network test-centos1
docker network disconnect test-network test-centos2

docker exec -it test-centos1 ip a
docker exec -it test-centos2 ip a

docker network connect test-network test-centos1
docker network connect test-network test-centos2

docker exec -it test-centos1 ip a
docker exec -it test-centos2 ip a

docker network connect bridge test-centos1
docker exec -it test-centos1 ip a

# 如果你想删除一个网络，那么必须要先将这个网络中的容器断连，全部断连之后，你才能删除这个网络
docker network disconnect test-network test-centos1
docker network disconnect test-network test-centos2
docker network rm test-network
docker network ls
```

### 6. Docker容器间网络通信

```bash
# Docker网络设置的目的无非就是两个，第一个要保证容器间的通信，第二要保证外部能够访问到我们的容器承载的业务。
# 比如说我们有一些复杂的应用，要部署在不同的容器中，那么我们为了让着个复杂的应用能够完美运行，我们一般都要求不同容器的端口实现通信。
docker rm -f `docker ps -qa`
docker ps -a

# 想实现容器之间的通信，有两种方法，第一种就是将容器放到同一个bridge中，第二种就是将容器都设置为host的网络模式，让容器和宿主机共享一个网络。
docker run -dt --name b-centos1 centos
docker run -dt --name b-centos2 centos
docker exec -it b-centos1 ip a
docker exec -it b-centos2 ip a

docker exec -it b-centos2 ping -c 1 172.17.0.2

```

### 7. 容器被外部访问

```bash
# Docker容器被外部访问一般基于两种网络模式，第一种是bridge，第二种是Host。
# 如果是bridge模式则需要通过DNAT的方法让我们的Docker容器被访问到
# 如果是Host模式，则我们的Docker容器可以直接被访问到

docker run -dt --name web1 nginx
docker ps

docker inspect web1 | grep -i ip

curl 172.17.0.4:80
# 上面的情况默认外部是访问不到nginx容器的，因为nginx容器所获得的地址是docker网桥内部的地址。

docker run -dt --name web2 -p 8080:80 nginx
docker ps
# -p 参数表示端口映射，-p 8080:80的意思是将宿主机的8080端口映射到容器的80端口
ss -tunlp
iptables -L -t nat -n

curl localhost:8080

docker inspect web2 | grep -i NetworkSettings -A 10


# 如果容器的网络模式是host，那就简单了，由于容器会和host共享一个网络，那么容器曝露的端口将会被直接访问到
docker run -dt --network host --name web3 nginx
docker ps
ss -tunlp
```

## 容器的持久化存储

### 1. 镜像的分层

```bash
# Docker容器和Docker镜像的关系。
# Docker镜像运行之后就是Docker容器。（通俗的解释）
# 基于Docker镜像挂载一个可读可写的文件系统，这个文件系统就是Docker容器的根分区。有了根分区，我们才能认为Docker容器像一个操作系统，可以运行。
# Docker镜像本身是没有写权限的。基于Docker镜像之上挂载一个可读可写的文件系统就可以当这个文件系统是一个Docker容器的文件系统。
# 也就是说我们针对Docker容器的操作，其实是我们在可读可写的文件系统上操作，而不是操作Docker容器底层的镜像。
# 因为Docker镜像是不具备写权限的。所以无论你在Docker容器内做任何操作都不会影响到Docker镜像。
# 为何Docker容器的存储是非持久化的？
# 因为Docker容器的文件系统是即用即删的。也就意味着，当你的Docker容器被删除之后，你挂载的Docker文件系统也会被删除，
# 那么你在Docker容器中做的任何操作，都会随着Docker容器的文件系统删除而删除。
```

![image-20230630160911263](http://typora.hflxhn.com/documents/2023/0630/image-20230630160911263.png)

### 2. 容器使数据卷Volume

```bash
# 如果想让容器实现持久化存储，那么可以使用data volume来实现。
# data volume叫做数据卷，我们可以将数据库挂载到容器当中，进而让存储在数据卷中的数据不会随着容器的删除而删除。

docker volume ls

# 默认情况下，没有任何的volume
ls /var/lib/docker/volumes/

# /var/lib/docker/volume是存储本地volume的默认路径，所以说我们创建的volume默认都存放在这个位置

docker volume create test1
ls /var/lib/docker/volumes/ /var/lib/docker/volumes/test1/ /var/lib/docker/volumes/test1/_data/

docker run -dt --name test1 -v test1:/test1 centos
docker exec -it test1 bash
df -Th

# 之前的步骤是先创建volume，再挂载（local形式的数据卷）
# 现在换一种方法使用数据卷
docker run -dt --name test2 -v test2 centos
docker volume ls

# 如果创建容器的时候不使用-v参数的标准格式来使用数据卷，那么docker会自动帮你创建一个数据卷，
# 并且如果以上面的例子来看，那么test2并不是任何的数据卷，而是挂载点，而且是/test2

docker run -dt --name test3 -v web:/web nginx
docker volume ls
```

### 3. 容器使用bind mounts

```bash
mkdir /mnt/test5
docker run -dt --name test5 -v /mnt/test5:/test5 centos
docker exec -it test5 bash df -Th
```

### 4. 容器持久化存储实践

```bash
docker pull httpd
docker images

docker run -dt --name web1 httpd
docker exec -it web1 bash
# 查看 docker httpd 容器的 web 文件存放目录
docker rm -f web1

# 当我们进入到容器之后，我们可以看到我们当前的web页面实际的目录是/usr/local/apache2/htdocs
docker run -dt --name web2 -v /web:/usr/local/apache2/htdocs --network host httpd
curl localhost
```

### 5. 容器的自启动策略

```bash
# docker容器默认情况下，如果宿主机关机再开机之后，docker容器将不会自动启动。
# 我们必须要确保容器的服务是开机自启动的
systemctl enable docker.service --now

# 我们必须要确保哪些容器需要开机自启动
docker run -dt --name web1 -v /web:/usr/local/apache2/htdocs -p 80:80 --restart=always httpd

# --restart参数有三个
# 默认是no，表示容器退出时，不重启容器
# on-failure表示只有容器在非0状态退出时才重新启动容器
# always表示无论任何状态退付出，都重启容器
```

