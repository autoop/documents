# podman 搭建内网容器仓库

## 服务端搭建

```bash
# 拉取镜像
podman pull registry

# 启动容器
mkdir -p /opt/podman-registry
podman run --privileged -d -v /opt/podman-registry:/var/lib/registry:Z -p 50000:5000 --name registry registry:latest

# 测试仓库是否可用
curl http://localhost:50000/v2/

# 创建自签名证书
mkdir -p /opt/podman-registry/certs/
cd /opt/podman-registry/certs/
# 创建证书私钥
openssl genrsa 2048 > server.key
# 创建证书签名请求
cat > csr.cnf <<END
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
[dn]
C = CN
ST = shanxi
L = xi'an
O = fanyang
OU = fanyang
CN = registry.hflxhn.com
END
openssl req -new -key server.key -config csr.cnf -out server.csr
# 自签名证书
openssl x509 -req -days 3650 -in server.csr -signkey server.key -out server.crt

# 查看证书
openssl x509 -in /opt/podman-registry/certs/server.crt -text -noout

# 运行容器
podman rm -f registry
podman run --privileged -d -v /opt/podman-registry:/var/lib/registry:Z \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/var/lib/registry/certs/server.crt \
-e REGISTRY_HTTP_TLS_KEY=/var/lib/registry/certs/server.key \
-p 50000:5000 \
--name registry registry:latest

# 创建htpasswd文件支持认证
mkdir -p /opt/podman-registry/auth
yum -y install httpd-tools
htpasswd -Bbn mengpa Fhl3y_Jsdwj >> /opt/podman-registry/auth/htpasswd

# 运行容器
podman rm -f registry
podman run --privileged -d -p 50000:5000 \
-v /opt/podman-registry:/var/lib/registry:Z \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/var/lib/registry/certs/server.crt \
-e REGISTRY_HTTP_TLS_KEY=/var/lib/registry/certs/server.key \
-e REGISTRY_AUTH=htpasswd \
-e REGISTRY_AUTH_HTPASSWD_REALM="Registry Realm" \
-e REGISTRY_AUTH_HTPASSWD_PATH=/var/lib/registry/auth/htpasswd \
--name registry registry:latest

# 生成 service 文件
podman generate systemd --files --new --name registry
mv container-registry.service /etc/systemd/system/
restorecon -RvF /etc/systemd/system/container-registry.service

# 设置开启启动容器
systemctl enable container-registry.service --now
```

## 客户端

```bash
# 配置镜像仓库
ls /etc/containers/registries.conf.bak &>/dev/null || cp -r /etc/containers/registries.conf{,.bak}
cat > /etc/containers/registries.conf << 'END'
unqualified-search-registries = ["registry.hflxhn.com:50000", "docker.io"]

[[registry]]
prefix = "docker.io"
location = "fiyc0dbc.mirror.aliyuncs.com"

[[registry]]
prefix = "registry.hflxhn.com:50000"
location = "registry.hflxhn.com:50000"
insecure = true
END

# 修改 hosts 解析
cat > /etc/hosts << END
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
172.24.0.248 registry.hflxhn.com
END

# 修改tag
podman tag docker.io/library/nginx registry.hflxhn.com:50000/nginx:latest
podman tag docker.io/library/registry registry.hflxhn.com:50000/registry:latest

# 登陆仓库
podman login --username fanyang --password Fhl3y_Jsdwj registry.hflxhn.com:50000

# push镜像
podman push registry.hflxhn.com:50000/nginx:latest
podman push registry.hflxhn.com:50000/registry:latest

# 查看仓库中有哪些镜像
curl -u fanyang:Fhl3y_Jsdwj -X GET https://registry.hflxhn.com:50000/v2/_catalog -k

# 删除 tag
curl -u fanyang:Fhl3y_Jsdwj -k -X GET https://registry.hflxhn.com:50000/v2/php/tags/list
curl -I -XGET -k --header "Accept:application/vnd.docker.distribution.manifest.v2+json" -u fanyang:Fhl3y_Jsdwj https://registry.hflxhn.com:50000/v2/php/manifests/fpm
curl -k -I -XDELETE -u fanyang:Fhl3y_Jsdwj https://registry.hflxhn.com:50000/v2/php/manifests/sha256:4dc69f394c63c8f8acbe3d67c0cb5d2bd3bba9a6e62641e1fc53eda2447164be

# Registry GC回收空间
podman exec -it registry registry garbage-collect /etc/docker/registry/config.yml
```

